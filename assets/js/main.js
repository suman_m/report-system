/**
 * Created by suman on 7/15/14.
 */
jQuery(document).ready(function($) {
    var poel, upel, rgel, srel;
    $("#addUserForm #region, #addUserForm #accesslevel").change(function() {
        var siteurl = $("#siteURL").val();
        var reg = $("#addUserForm #region").val();
        var acc = $("#addUserForm #accesslevel").val();
        if (acc == "Head Officer") {
            if (!rgel)
                rgel = $(".regionWrap").detach();

            if (srel) {
                srel.insertAfter(".acclvlWrap");
                srel = null;
            }
        } else {
            if (rgel) {
                rgel.insertAfter(".acclvlWrap");
                rgel = null;
            }

            if (!srel) {
                srel = $(".sublvlWrap").detach();
            }
        }
        if (acc == "Head Officer" || acc == "Program Manager") {
            if (!poel)
                poel = $(".parentOfficer").detach();
            if (!upel)
                upel = $(".userProject").detach();
        } else {
            if (acc == "Program Officer") {
                if (poel) {
                    poel.insertBefore("#addUserForm button.btn");
                    poel = null;
                }
                if (!upel) {
                    upel = $(".userProject").detach();
                }
            }
            if (acc == "Project Holder") {
                if (poel) {
                    poel.insertBefore("#addUserForm button.btn");
                    poel = null;
                }
                if (upel) {
                    upel.insertBefore("#addUserForm button.btn");
                    upel = null;
                }
                //ajax for project filtering
            }
            $.ajax({
                type: "post",
                url: siteurl + "head-officer/users/filterusers",
                data: "reg=" + reg + "&acc=" + acc,
                success: function(server_response) {
                    $("#addUserForm #parentuser").html(server_response);
                }
            });
        }
    });

    $(".addRegionBtn").click(function() {
        $(".addFormWrap, .overlay").show();
    });
    $(".addFormWrap .close").click(function() {
        $(".addFormWrap h2").html("Add Region");
        $(".addFormWrap, .overlay").hide();
    });
    $(".editRegion").click(function(element) {
        element.preventDefault();
        var editid = $(this).attr("data");
        var href = $(this).attr("href");
        var name = $(".regionName_" + editid).text();
        $("#addRegionForm").attr("action", href);
        $(".addFormWrap h2").html("Edit Region");
        $("#addRegionForm #editid").val(editid);
        $("#addRegionForm #regionname").val(name);
        $(".addFormWrap, .overlay").show();
    });

    $(".editCategory").click(function(element) {
        element.preventDefault();
        var editid = $(this).attr("data");
        var href = $(this).attr("href");
        var name = $(".regionName_" + editid).text();
        $("#addCategoryForm").attr("action", href);
        $(".addFormWrap h2").html("Edit Category");
        $("#addCategoryForm #editid").val(editid);
        $("#addCategoryForm #categoryname").val(name);
        $(".addFormWrap, .overlay").show();
    });

    $(".addCategoryBtn").click(function() {
        $(".addFormWrap, .overlay").show();
    });

    setTimeout(function() {
        $(".alert-success").fadeOut(2000);
    }, 3000);
});