<?php
$districts = Array
    (
    0 => "Achham",
    1 => "Arghakhanchi",
    2 => "Baglung",
    3 => "Baitadi",
    4 => "Bajhang",
    5 => "Bajura",
    6 => "Banke",
    7 => "Bara",
    8 => "Bardiya",
    9 => "Bhaktapur",
    10 => "Bhojpur",
    11 => "Chitwan",
    12 => "Dadeldhura",
    13 => "Dailekh",
    14 => "Dang Deukhuri",
    15 => "Darchula",
    16 => "Dhading",
    17 => "Dhankuta",
    18 => "Dhanusa",
    19 => "Dholkha",
    20 => "Dolpa",
    21 => "Doti",
    22 => "Gorkha",
    23 => "Gulmi",
    24 => "Humla",
    25 => "Ilam",
    26 => "Jajarkot",
    27 => "Jhapa",
    28 => "Jumla",
    29 => "Kailali",
    30 => "Kalikot",
    31 => "Kanchanpur",
    32 => "Kapilvastu",
    33 => "Kaski",
    34 => "Kathmandu",
    35 => "Kavrepalanchok",
    36 => "Khotang",
    37 => "Lalitpur",
    38 => "Lamjung",
    39 => "Mahottari",
    40 => "Makwanpur",
    41 => "Manang",
    42 => "Morang",
    43 => "Mugu",
    44 => "Mustang",
    45 => "Myagdi",
    46 => "Nawalparasi",
    47 => "Nuwakot",
    48 => "Okhaldhunga",
    49 => "Palpa",
    50 => "Panchthar",
    51 => "Parbat",
    52 => "Parsa",
    53 => "Pyuthan",
    54 => "Ramechhap",
    55 => "Rasuwa",
    56 => "Rautahat",
    57 => "Rolpa",
    58 => "Rukum",
    59 => "Rupandehi",
    60 => "Salyan",
    61 => "Sankhuwasabha",
    62 => "Saptari",
    63 => "Sarlahi",
    64 => "Sindhuli",
    65 => "Sindhupalchok",
    66 => "Siraha",
    67 => "Solukhumbu",
    68 => "Sunsari",
    69 => "Surkhet",
    70 => "Syangja",
    71 => "Tanahu",
    72 => "Taplejung",
    73 => "Terhathum",
    74 => "Udayapur"
);
$this->load->view("head-officer/header");
var_dump($projectInfo);
?>
<script src="<?php echo site_url('assets/js/jquery.validate.min.js'); ?>"></script>
<script>
    $(document).ready(function() {
        $('#addProjectForm').validate();
    })
</script>
<div class="container">
    <div class="row">

        <?php $this->load->view("head-officer/leftNav"); ?>

        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main marginLeft0 listWrap">
            <h3 class="page-header"><?php echo (isset($projectInfo)) ? "Edit":'Add' ?> Project</h3>

            <form id="addProjectForm" class="small-forms" action="<?php echo (isset($projectInfo)) ? site_url().'head-officer/projects/update' : site_url().'head-officer/projects/save'; ?>" method="post">
                <input type="hidden" name="editid" value="<?php echo (isset($projectInfo)) ? $projectInfo->id : ""; ?>"/>
                <p><label for="projectNumber">Project No.:</label> <input type="text" name="projectNumber" id="projectName" class="form-control required" value="<?php echo (isset($projectInfo))? $projectInfo->number : ""; ?>"/></p>
                <p><label for="projectName">Project Name:</label> <input type="text" name="projectName" id="projectName" class="form-control required" value="<?php echo (isset($projectInfo))? $projectInfo->name : ""; ?>"/></p>
                <p><label for="description">Project Description:</label> <textarea name="description" id="description" class="form-control required" rows="5"><?php echo (isset($projectInfo))? $projectInfo->description : ""; ?></textarea></p>
                <p><label for="program">Program:</label> <input type="text" name="projectProgram" id="projectName" class="form-control required" value="<?php echo (isset($projectInfo))? $projectInfo->program : ""; ?>"/></p>
                <p>
                    <label for="projectHolder">Project holder</label>
                    <select name="projectHolder" class="required form-control">
                        <option value="">Choose Project Holder</option>
                        <?php foreach ($projectHolder as $ph): ?>
                            <option value="<?php echo $ph->ID; ?>" <?php echo (isset($projectInfo) && $projectInfo->assign_to_project_holder == $ph->ID) ? 'selected="selected"' : ''; ?>><?php echo $ph->name; ?></option>
                        <?php endforeach; ?>
                    </select>
                </p>
                <p>
                    <label for="projectManager">Project Manager</label>
                    <select name="projectManager" class="required form-control">
                        <option value="">Choose Project Manager</option>
                        <?php foreach ($projectManager as $ph): ?>
                            <option value="<?php echo $ph->ID; ?>" <?php echo (isset($projectInfo) && $projectInfo->project_manager == $ph->ID) ? 'selected="selected"' : ''; ?>><?php echo $ph->name; ?></option>
                        <?php endforeach; ?>
                    </select>
                </p>
                <p>
                    <label for="projectOfficer">Project Officer</label>
                    <select name="projectOfficer" class="required form-control">
                        <option value="">Choose Project Officer</option>
                        <?php foreach ($projectOfficer as $ph): ?>
                            <option value="<?php echo $ph->ID; ?>" <?php echo (isset($projectInfo) && $projectInfo->project_officer == $ph->ID) ? 'selected="selected"' : ''; ?>><?php echo $ph->name; ?></option>
                        <?php endforeach; ?>
                    </select>
                </p>
                <p>
                    <label for="district">Districts</label>
                    <select name="district" class="required form-control">
                        <option value="">Choose District</option>
                        <?php foreach ($districts as $dis): ?>
                            <option value="<?php echo $dis; ?>" <?php echo (isset($projectInfo) && $projectInfo->district == $dis) ? 'selected="selected"' : ''; ?>><?php echo $dis; ?></option>
                        <?php endforeach; ?>
                    </select>
                </p>
                <p class="regionWrap">
                    <label for="region">Region:</label>
                    <select name="region" id="region" class="form-control required">
                        <option value="">Select a Region</option>
                        <?php foreach($regions as $region): ?>
                            <option value="<?php echo $region->id; ?>" <?php echo (isset($projectInfo) && $projectInfo->region == $region->id) ? 'selected="selected"' : ''; ?>><?php echo $region->name; ?></option>
                        <?php endforeach; ?>
                    </select>
                </p>
                <button class="btn btn-primary btn-block" type="submit">Save</button>
            </form>
        </div>
    </div>
</div>
<?php $this->load->view("footer"); ?>