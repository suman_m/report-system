<?php $this->load->view("header"); ?>
<script>
    $(document).ready(function() {
        $('#myTab a').click(function(e) {
            e.preventDefault();
            $(this).tab('show');
        });
        $('#add-report-type').click(function() {
            var total = parseInt($('#total').val());
            var text = '<div class="form-group" id="reportType-' + total + '"><label>Report Type</label><input type="text" name="reportType[]" class="form-control required" /><span><a href="javascript:;" id="reportData-' + total + '" onclick="removeReportType(this.id)">Remove</a></span></div>';
            total = total + 1;
            $('#report-type').append(text);
            $('#total').val(total);
        });
    });

    function removeReportType(id)
    {
        var value = id.split('-');
        $('#reportType-' + value['1']).remove();
    }
</script>
<div class="page-wrapper">
    <div class="container">
        <h3 class="form-signin-heading"><?php echo $title; ?></h3>
        <form action="" method="post">
            <div id="report-type">

                <?php
                $i = 1;
                if (!isset($value)) {
                    $id = '';
                    ?>
                    <div class="form-group"><label>Report Type</label><input type="text" name="reportType[]" class="form-control required"  /> </div>
                    <?php
                } else {
                    //$reportType = json_decode($value->report_category);
                    var_dump($reportType);
                    $id = '';
                    foreach ($reportType as $r_t):
                        $id .= $r_t->id . ',';
                        ?>
                        <div class="form-group" <?php echo ($i != 1) ? 'id="reportType-' . $i . '"' : ''; ?>><label>Report Type</label><input type="text" name="reportCategory-<?php echo $r_t->id; ?>" class="form-control required" value="<?php echo $r_t; ?>" /><?php if ($i != 1): ?><span><a href="javascript:;" id="reportData-<?php echo $i; ?>" onclick="removeReportType(this.id)">Remove</a></span><?php endif; ?></div>
                        <?php
                    endforeach;
                    $id = trim(',', $id);
                    ?>
                    <?php
                }
                ?>
                <input type="id" name="id" value="<?php echo $id; ?>" />
            </div>
            <input type="hidden" name="total" value="1" id="total" />
            <div><a href="javascript:;" id="add-report-type">Add New</a></div>
            <div class="form-group"><label>Report Submission Time</label>
                <select name="reportSubmissionTime" class="form-control required">
                    <option value="">Choose Report Submission Time</option>
                    <option value="weekly" <?php echo (isset($value) && $value->report_submission_time == "weekly") ? 'selected="selected"' : ''; ?>>Weekly</option>
                    <option value="fortnignt" <?php echo (isset($value) && $value->report_submission_time == "fortnight") ? 'selected="selected"' : ''; ?>>Fortnight</option>
                    <option value="monthly" <?php echo (isset($value) && $value->report_submission_time == "monthly") ? 'selected="selected"' : ''; ?>>Monthly</option>
                </select>
            </div>
            <div class="form-group">
                <label>Status </label><input type="checkbox" name="status" value="1" class="checkbox" <?php echo (isset($value) && $value->status == "1") ? 'checked="checked"' : ''; ?> />
            </div>
            <button class="btn btn-primary btn-block" name="submit" value="submit" type="submit">Save</button>
        </form>
    </div>
</div>
<?php $this->load->view("footer"); ?>