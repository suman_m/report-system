<?php $this->load->view("head-officer/header"); ?>
<script src="<?php echo site_url('assets/js/jquery.validate.min.js'); ?>"></script>
<script>
    $(document).ready(function() {
        $('#addUserForm').validate();
    })
</script>
<?php
$username = $useremail = $accessLvl = $subsLvl = $regn = $parentuser = $project = '';
$addEdit = $this->uri->segment(3);
if ($addEdit == "add") {
$action = "save";
} elseif ($addEdit == "edit") {
$action = "update";
$userID = $edituser[0]->ID;
$username = $edituser[0]->name;
$useremail = $edituser[0]->email;
$accessLvl = $edituser[0]->accessLevel;
$subsLvl = $edituser[0]->sub_level;
$regn = $edituser[0]->region;
(!empty($userparent)) ? $parentuser = $userparent[0]->parent_user_id : 0;
}
?>
<div class="container">
    <div class="row">

        <?php $this->load->view("head-officer/leftNav"); ?>

        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main marginLeft0">
            <h3 class="page-header"><?php echo ucfirst($addEdit); ?> User</h3>
            <form id="addUserForm" class="small-forms" action="<?php echo site_url(); ?>head-officer/users/<?php echo $action; ?>" method="post">
                <?php if ($action == "update") { ?>
                <input type="hidden" name="editid" value="<?php echo $this->input->get("id"); ?>">
                <input type="hidden" name="oldPass" value="<?php echo $edituser[0]->password; ?>">
                <?php } ?>
                <p><label for="username">Name:</label> <input type="text" class="form-control required" required="" autofocus="" name="username" id="username" value="<?php echo $username; ?>"></p>
                <p><label for="useremail">Email:</label> <input type="email" class="form-control required" required="" autofocus="" name="useremail" id="useremail" value="<?php echo $useremail; ?>"></p>
                <p><label for="userpass">Password:</label> <input type="password" class="form-control" autofocus="" name="userpass" id="userpass"></p>
                <p><label for="confirmPass">Confirm Password:</label> <input type="password" class="form-control" autofocus="" name="confirmPass" id="confirmPass"></p>
                <p class="acclvlWrap"><label for="accesslevel">Access Level:</label>
                    <select name="accesslevel" id="accesslevel" class="form-control required" <?php
                    if ($addEdit == "edit") {
                    echo "disabled";
                    }
                    ?>>
                        <option value="">Select an Access Level</option>
                        <?php foreach ($accesslvl as $aclvl): ?>
                        <option value="<?php echo $aclvl->name; ?>" <?php
                        if ($accessLvl == $aclvl->name) {
                        echo "selected";
                        }
                        ?>><?php echo $aclvl->name; ?></option>
                                <?php endforeach; ?>
                    </select>
                </p>
                <?php if ($addEdit == "edit" && $accessLvl != "Head Officer"): ?>

                <?php else: ?>
                <p class="sublvlWrap"><label for="sublevel">Sub Level:</label>
                    <select name="sublevel" id="sublevel" class="form-control">
                        <option value="">Select an Sub Level</option>
                        <?php foreach ($sublvl as $aclvl): ?>
                        <option value="<?php echo $aclvl; ?>" <?php
                        if ($subsLvl == $aclvl) {
                        echo "selected";
                        }
                        ?>><?php echo $aclvl; ?>
                        </option>
                        <?php endforeach; ?>
                    </select>
                </p>
                <?php endif; ?>
                <p class="regionWrap"><label for="region">Region:</label>
                    <select name="region" id="region" class="form-control required" <?php
                    if ($addEdit == "edit") {
                    echo "disabled";
                    }
                    ?>>
                        <option value="">Select a Region</option>
                        <?php foreach ($regions as $region): ?>
                        <option value="<?php echo $region->id; ?>" <?php
                        if ($regn == $region->id) {
                        echo "selected";
                        }
                        ?>><?php echo $region->name; ?></option>
                                <?php endforeach; ?>
                    </select>
                </p>
                <p class="parentOfficer"><label for="parentuser">Direct Officer above this user:</label>
                    <select name="parentuser" id="parentuser" class="form-control required" <?php
                    if ($addEdit == "edit") {
                    echo "disabled";
                    }
                    ?>>
                        <option value="">Select an Officer</option>
                        <?php foreach ($userList as $user): ?>
                        <option value="<?php echo $user->ID; ?>" <?php
                        if ($parentuser == $user->ID) {
                        echo "selected";
                        }
                        ?>><?php echo $user->name; ?></option>
                                <?php endforeach; ?>
                    </select>
                </p>
                <?php if ($addEdit == "edit" && $accessLvl == "Project Holder"): ?>
                <p class="userProject"><label for="userProject">Assign Project for this user:</label>
                    <select name="userProject" id="userProject" class="form-control required">
                        <option value="">Select a Project</option>
                        <?php foreach ($projects as $project): ?>
                        <?php if ($project->assign_to_project_holder == 0 || $project->assign_to_project_holder == $userID) { ?><option value="<?php echo $project->id; ?>" <?php
                        if ($project->assign_to_project_holder == $userID) {
                        echo "selected";
                        }
                        ?>><?php echo $project->name; ?></option><?php } ?>
                                <?php endforeach; ?>
                    </select>
                </p>
                <?php elseif ($addEdit == "add"): ?>
                <p class="userProject"><label for="userProject">Assign Project for this user:</label>
                    <select name="userProject" id="userProject" class="form-control required">
                        <option value="">Select a Project</option>
                        <?php foreach ($projects as $project): ?>
                        <?php if ($project->assign_to_project_holder == 0) { ?><option value="<?php echo $project->id; ?>"><?php echo $project->name; ?></option><?php } ?>
                        <?php endforeach; ?>
                    </select>
                </p>
                <?php endif; ?>
                <button class="btn btn-primary btn-block" type="submit">Save</button>
            </form>
        </div>
    </div>
</div>
<?php $this->load->view("footer"); ?>