<?php $this->load->view("head-officer/header"); ?>
<div class="page-wrapper">
    <div class="container">
        <div class="col-sm-12">
            <a class="btn btn-primary addBtn" href="<?php echo site_url('head-officer/others/add'); ?>">Upload Other Document</a>
            <h3 class="page-header"><?php echo $title; ?></h3>
            <p></p>
            <p></p>
            <?php if (!empty($othersData)): ?>
                <table class="table table-striped">
                    <tr>
                        <th>SN</th>
                        <th>Title</th>
                        <th>Description</th>
                        <th>Uploaded Date</th>
                        <th>Option</th>
                    </tr>
                    <tbody>
                        <?php
                        $i = 1;
                        foreach ($othersData as $od):
                            ?>
                            <tr>
                                <td><?php echo $i; ?></td>
                                <td><?php echo $od->others_title; ?></td>
                                <td><?php echo $od->others_description; ?></td>
                                <td><?php echo $od->date; ?></td>
                                <td><a class="btn btn-primary" href="<?php echo site_url('head-officer/others/downloadDocuments/' . $od->id); ?>">Download</a></td>
                            </tr>
                            <?php
                            $i++;
                        endforeach;
                        ?>
                    </tbody>
                </table>
                <?php
            else:
                echo "No data Available";
            endif;
            ?>
            <div>
                <?php
                if (!empty($othersLog)):
                    ?>
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>SN</th>
                                <th>Log Message</th>
                                <th>Date</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $i = 1;
                            foreach ($othersLog as $ol):
                                ?>
                                <tr>
                                    <td><?php echo $i; ?></td>
                                    <?php $userData = $this->misc_lib->getUserData($ol->user_id); ?>
                                    <td><?php echo $ol->logs; ?> by <?php echo $userData->name . ' ' . $ol->access_level; ?> </td>
                                    <td><?php echo $ol->date; ?></td>
                                </tr>
                                <?php
                                $i ++;
                            endforeach;
                            ?>
                        </tbody>
                    </table>
                    <?php
                endif;
                ?>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view("footer"); ?>