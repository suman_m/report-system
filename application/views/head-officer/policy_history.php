<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 7/22/14
 * Time: 10:25 AM
 */
$this->load->view("head-officer/header");
?>
    <script type="application/javascript" src="<?php echo site_url('assets/js/jquery-ui.js'); ?>"></script>
    <link rel="stylesheet" href="<?php echo site_url('assets/css/jquery-ui.css'); ?>" />
    <script>
        $(document).ready(function(){
            $('#dateStart').datepicker();
            $('#dateEnd').datepicker();
        });
    </script>
<div class="page-wrapper">
    <div class="container">
        <h3 class="page-header">Policies Download History</h3>
        <fieldset>
            <form method="post" action="">
                <label>Region</label>
                <select name="region">
                    <option value="">Choose Regions</option>
                    <?php foreach ($region as $row): ?>
                        <option value="<?php echo $row->id; ?>"><?php echo $row->name; ?></option>
                    <?php endforeach; ?>
                </select>
                <label>Downloaded Date From</label>
                <input type="text" id="dateStart" name="from" value="" placeholder="Filter By Date">
                <label>Downloaded Date To</label>
                <input type="text" id="dateEnd" name="to" value="" >
                <input type="submit" name="submitBtn" value="Search" class="btn btn-primary">
            </form>
        </fieldset>
        <table class="table table-striped">
            <?php if ($history != NULL): ?>
                <thead>
                    <tr>
                        <th>SN</th>
                        <th>Policy Name</th>
                        <th>File</th>
                        <th>Uploaded Date</th>
                        <th>Downloaded Date</th>
                        <th>Downloaded By</th>
                        <th>Downloaded Region</th>
                        <th>Access Level</th>
                        <th>Download From</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $i = 1;
                    foreach ($history as $res):
                        ?>
                        <tr>
                            <td><?php echo $i; ?></td>
                            <td><?php echo $res->policy_name; ?></td>
                            <td><?php echo $res->actual_file; ?></td>
                            <td><?php echo $res->uploaded_date; ?></td>
                            <td><?php echo $res->download_date; ?></td>
                            <td><?php echo $res->username; ?></td>
                            <td><?php echo $res->region_name; ?></td>
                            <td><?php echo $res->access_level; ?></td>
                            <td><?php echo $res->ip; ?></td>
                        </tr>
                        <?php
                        $i++;
                    endforeach;
                    ?>
                </tbody>
            <?php endif; ?>
        </table>
    </div>
</div>
<?php $this->load->view("footer"); ?>