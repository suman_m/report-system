<?php $this->load->view("head-officer/header"); ?>
<div class="page-wrapper">
    <div class="container">
        <div class="row">

            <?php $this->load->view("head-officer/leftNav"); ?>

            <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main marginLeft0 listWrap">
                <a href="<?php echo site_url(); ?>head-officer/projects/add"><button type="button" class="btn btn-primary addProjectBtn addBtn">Add Project</button></a>
                <h3 class="page-header">Projects</h3>
                <?php if (isset($success) && $success): ?>
                    <div class="alert alert-<?php echo $successType ?> margintop10" role="alert"><?php echo $successMsg; ?></div>
                <?php endif; ?>

                <?php $assignedPh = ''; ?>
               <!-- <form class="form-inline" action="<?php echo site_url(); ?>head-officer/projects/assign" method="post">
                    <p>Assign
                        <select name="projectName" id="" class="form-control">
                            <option value="">Select Project</option>
                <?php foreach ($projects as $project): $assignedPh[] = $project->assign_to_project_holder; ?>
                    <?php if ($project->assign_to_project_holder == 0) { ?><option value="<?php echo $project->id; ?>"><?php echo $project->name; ?></option><?php } ?>
                <?php endforeach; ?>
                        </select>
                        Project to
                        <select name="holderName" id="" class="form-control">
                            <option value="">Select Project Holder</option>
                <?php foreach ($projectHolders as $ph): ?>
                    <?php if (!in_array($ph->ID, $assignedPh)) { ?><option value="<?php echo $ph->ID; ?>"><?php echo $ph->name; ?></option><?php } ?>
                <?php endforeach; ?>
                        </select>
                        Project Holder
                        <button type="submit" class="btn btn-sm btn-default">Save</button>
                    </p>
                </form>
                <form class="form-inline" action="<?php echo site_url(); ?>head-officer/projects/assign" method="post">
                    <p>Remove
                        <select name="projectName" id="" class="form-control">
                            <option value="">Select Project</option>
                <?php foreach ($projects as $project): ?>
                    <?php if ($project->assign_to_project_holder > 0) { ?><option value="<?php echo $project->id; ?>"><?php echo $project->name; ?></option><?php } ?>
                <?php endforeach; ?>
                        </select>
                        Project from any assigned Project Holder.
                        <input type="hidden" name="holderName" id="" class="" value="0" />
                        <button type="submit" class="btn btn-sm btn-default">Save</button>
                    </p>
                </form>
                <form class="form-inline" action="<?php echo site_url(); ?>head-officer/projects/assign" method="post">
                    <p>Remove
                        <select name="holderName" id="" class="form-control">
                            <option value="">Select Project Holder</option>
                <?php foreach ($projectHolders as $ph): ?>
                    <?php if (in_array($ph->ID, $assignedPh)) { ?><option value="<?php echo $ph->ID; ?>"><?php echo $ph->name; ?></option><?php } ?>
                <?php endforeach; ?>
                        </select>
                        Project Holder from any assigned Project.
                        <input type="hidden" name="projectName" id="" class="" value="0" />
                        <button type="submit" class="btn btn-sm btn-default">Save</button>
                    </p>
                </form> -->

                <div class="line"></div>
                <div class="table-responsive">
                    <?php $count = 0; ?>
                    <?php if (count($projects) > 0): ?>
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th class="col-lg-1">S. N.</th>
                                    <th class="col-lg-3">Project Name</th>
                                    <th>Description</th>
                                    <th class="col-lg-2">Project Holder</th>
                                    <th class="col-lg-2">Program Manager</th>
                                    <th class="col-lg-2">Program Officer</th>
                                    <th class="col-lg-2">Options</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($projects as $pro): $count++; ?>
                                    <tr>
                                        <td><?php echo $count; ?></td>
                                        <td><?php echo $pro->name; ?></td>
                                        <td><?php echo $pro->description; ?></td>
                                        <td>
                                            <!-- <?php
                                            foreach ($projectHolders as $ph) {
                                                if ($pro->assign_to_project_holder == $ph->ID) {
                                                    echo $ph->name;
                                                }
                                            }
                                            ?>
                                            <?php
                                            if ($pro->assign_to_project_holder == 0) {
                                                echo "-";
                                            }
                                            ?> -->
                                            <?php // echo $this->misc_lib->getProjectHolderName($pro->id); ?>
                                            <?php echo (count($this->misc_lib->getUserData($pro->assign_to_project_holder)) > 0 )  ? $this->misc_lib->getUserData($pro->assign_to_project_holder)->name : '-'; ?>
                                        </td>
                                        <td>
                                            <?php echo (count($this->misc_lib->getUserData($pro->project_manager)) > 0 )  ? $this->misc_lib->getUserData($pro->project_manager)->name : '-'; ?>
                                        </td>
                                        <td>
                                            <?php echo (count($this->misc_lib->getUserData($pro->project_officer)) > 0 )  ? $this->misc_lib->getUserData($pro->project_officer)->name : '-'; ?>
                                        </td>
                                        <td>
                                            <a class="btn btn-sm btn-primary" href="<?php echo site_url("head-officer/projects/edit?id=$pro->id"); ?>">Edit</a>
                                            <a class="btn btn-sm btn-danger" href="<?php echo site_url("head-officer/projects/delete/$pro->id"); ?>">Delete</a>
                                            <?php /*
                                              <a class="btn btn-sm btn-primary" href="<?php echo site_url("head-officer/projects/assignProject/$pro->id"); ?>">Assign</a>
                                             */ ?>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                    <?php else: ?>
                        <p>There are no projects to list.</p>
                    <?php endif; ?>
                </div>

                <div class="line"></div>
                <div class="table-responsive">
                    <?php if (!empty($logs)): ?>
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>SN</th>
                                    <th>Log Message</th>
                                    <th>Date</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $i = 1;
                                foreach ($logs as $l):
                                    ?>
                                    <tr>
                                        <td><?php echo $i; ?></td>
                                        <td><?php echo $l->logs; ?></td>
                                        <td><?php echo $l->date; ?></td>
                                    </tr>
                                    <?php
                                    $i++;
                                endforeach;
                                ?>
                            </tbody>
                        </table>
                        <?php
                    endif;
                    ?>
                </div>

            </div>

        </div>
    </div>
</div>
<?php $this->load->view("footer"); ?>