<?php $this->load->view("head-officer/header"); ?>
<div class="page-wrapper">
    <div class="container">
        <div class="row">

            <?php $this->load->view("head-officer/leftNav"); ?>

            <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main marginLeft0">
                <a href="<?php echo site_url(); ?>head-officer/users/add"><button type="button" class="btn btn-primary addUserBtn addBtn">Add User</button></a>
                <h3 class="page-header">Users</h3>
                <?php if (isset($success) && $success): ?>
                    <div class="alert alert-success margintop10" role="alert">User <?php echo $successMsg; ?> successfully.</div>
                <?php endif; ?>
                <div class="table-responsive">
                    <?php $count = 0; ?>
                    <?php if (count($userList) > 0): ?>
                        <?php
                        $reg[0] = "";
                        foreach ($regnList as $region) {
                            $reg[$region->id] = $region->name;
                        }
                        ?>
                        <fieldset>
                            <form>
                                <table>
                                    <tr>
                                        <td>User Access Level : </td>
                                        <td>
                                            <select name="accesslevel" id="accesslevel" class="form-control required">
                                                <option value="">Select an Access Level</option>
                                                <?php foreach ($accesslvl as $aclvl): ?>
                                                    <option value="<?php echo $aclvl->name; ?>" <?php
                                                    if ($accessLvl == $aclvl->name) {
                                                        echo "selected";
                                                    }
                                                    ?>>
                                                                <?php echo $aclvl->name; ?>
                                                    </option>
                                                <?php endforeach; ?>
                                            </select>
                                        </td>
                                        <td><input type="submit" value="Search" class="btn btn-primary"/></td>
                                    </tr>
                                </table>
                            </form>
                        </fieldset>
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>S. N.</th>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Region</th>
                                    <th>Access Level</th>
                                    <th>Options</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($userList as $user): $count++; ?>
                                    <tr>
                                        <td><?php echo $count; ?></td>
                                        <td><?php echo $user->name; ?></td>
                                        <td><?php echo $user->email; ?></td>
                                        <td><?php echo $reg[$user->region]; ?></td>
                                        <td><?php echo $user->accessLevel ; echo ($user->sub_level) ? "<br/>(".$user->sub_level.")":""; ?></td>
                                        <td>
                                            <a class="btn btn-sm btn-primary" href="<?php echo site_url("head-officer/users/edit?id=$user->ID"); ?>">Edit</a> 
                                            <a class="btn btn-sm btn-primary" href="<?php echo site_url("head-officer/users/view_hierarchy?id=$user->ID"); ?>">Hierarchy</a>
                                            <a class="btn btn-sm btn-danger" onclick="return confirm('Are you sure to delete this user?')" href="<?php echo site_url("head-officer/users/delete?id=$user->ID"); ?>">Delete</a>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view("footer"); ?>