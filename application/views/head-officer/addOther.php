<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 7/15/14
 * Time: 2:41 PM
 */
$this->load->view("head-officer/header");
?>
<script src="<?php echo site_url('assets/js/jquery.validate.min.js'); ?>"></script>
<script src="<?php echo site_url('assets/js/additional-methods.min.js'); ?>"></script>
<script>
    $(document).ready(function() {
        $("#otherForm").validate({
            rules: {
                file: {
                    required: true,
                    extension: "xls|csv|doc|docx|xlsx|pdf"
                }
            }
        });
    });
</script>
<div class="page-wrapper">
    <div class="container">
        <?php
        $data = $this->session->flashdata('msg');
        echo $data;
        ?>
        <h3 class="page-header"><?php echo $title; ?></h3>
        <form   action="" method="post" enctype="multipart/form-data" id="otherForm">
            <label>Title</label>
            <input type="text" name="otherTitle" value="" class="form-control required">
            <label>Description</label>
            <textarea name="otherDescription"  class="form-control required"></textarea>
            <label>Upload Forms And Format</label>
            <input type='file' id="imgInp" name="file" />


            <label>Valid Upto</label>
            <input type="date" name="validDate" value=""><br>


            <label>Region</label>
            <input type="checkbox" name="all" value="All" id="selecctall">Select All
            <ul class="topLevelUL">
                <?php foreach ($region as $res): ?>
                    <li><input type="checkbox" class="checkbox1" name="" ><?php echo $res->name; ?>
                        <?php foreach ($usrhier as $usr): ?>
                            <?php if ($res->id == $usr->region && $usr->accessLevel == 'Program Manager') { ?>
                                <ul>
                                    <li><input type="checkbox" class="checkbox1" name="optional[]" value="<?php echo $usr->ID ?>"><?php echo $usr->name; ?>
                                        <ul>
                                            <?php
                                            foreach ($usrhier as $usr1):
                                                if ($res->id == $usr1->region && $usr1->accessLevel == 'Program Officer') {
                                                    ?>
                                                    <li><input type="checkbox" class="checkbox1" name="optional[]" value="<?php echo $usr1->ID ?>"><?php echo $usr1->name; ?>
                                                        <ul>
                                                            <?php foreach ($usrhier as $usr2): ?>
                                                                <?php if ($usr2->parent_user_id == $usr1->ID): ?>
                                                                    <li><input type="checkbox" class="checkbox1" name="optional[]" value="<?php echo $usr2->ID ?>"><?php echo $usr2->name; ?>
                                                                    <?php endif; ?>
                                                                <?php endforeach; ?>
                                                        </ul>
                                                    </li>
                                                <?php } ?>
                                            <?php endforeach; ?>
                                        </ul>
                                    </li>
                                </ul>
                            <?php } ?>
                        <?php endforeach; ?>
                    </li>
                <?php endforeach; ?>
            </ul>
            <label></label>
            <input type="submit" name="submitBtn" value="send" class="btn btn-primary">
        </form>
    </div>
</div>
<?php $this->load->view("footer"); ?>

<script type="text/javascript">

    $(document).ready(function() {
        $('#selecctall').click(function(event) {  //on click
            if (this.checked) { // check select status
                $('.checkbox1').each(function() { //loop through each checkbox
                    this.checked = true;  //select all checkboxes with class "checkbox1"
                });
            } else {
                $('.checkbox1').each(function() { //loop through each checkbox
                    this.checked = false; //deselect all checkboxes with class "checkbox1"
                });
            }
        });

        $("ul.topLevelUL input[type='checkbox']").click(function() {
            var elem = $(this).siblings();
            //elem.css("background","red");
            if (this.checked) {
                elem.find("input[type='checkbox']").each(function() {
                    this.checked = true;
                });
            }
            else
                elem.find("input[type='checkbox']").each(function() {
                    this.checked = false;
                });
        });
    });

</script>