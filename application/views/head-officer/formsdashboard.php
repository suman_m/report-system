<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 7/21/14
 * Time: 11:09 AM
 */
$this->load->view('head-officer/header');
?>
<div class="page-wrapper">
    <div class="container">
        <div class="col-sm-12 main marginLeft0 listWrap">
            <a  class="btn btn-primary addBtn" href="<?php echo site_url('head-officer/forms/add'); ?>">Add</a>
            <h3 class="page-header">Forms & Formats</h3>
            <table class="table table-striped">
                <?php
                echo $this->session->flashdata('msg');
                if ($form != FALSE):
                    ?>
                    <tr>
                        <th>SN</th>
                        <th>Forms & Formats Name</th>
                        <th>Forms & Formats Description</th>
                        <th>File Name</th>
                        <th>Date</th>
                        <th>Option</th>
                    </tr>
                    <tbody>
                        <?php
                        $i = 1;
                        foreach ($form as $res):
                            ?>
                            <tr>
                                <td><?php echo $i; ?></td>
                                <td><?php echo $res->form_format_name; ?></td>
                                <td><?php echo $res->description; ?></td>
                                <td><?php echo $res->actual_form; ?></td>
                                <td><?php echo $res->date; ?></td>
                                <td><a href="<?php echo site_url('head-officer/forms/history/' . $res->id); ?>" class="btn btn-info">View History</a></td>
                            </tr>
                            <?php
                            $i++;
                        endforeach;
                        ?>
                    </tbody>
                <?php endif; ?>
            </table>
        </div>
    </div>
</div>
<?php $this->load->view("footer"); ?>