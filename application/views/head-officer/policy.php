<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 7/15/14
 * Time: 2:41 PM
 */
$this->load->view("head-officer/header");
?>
<script src="<?php echo site_url('assets/js/jquery.validate.min.js'); ?>"></script>
<script>
    $(document).ready(function() {
        $("#addPolicyForm").validate();
    });
</script>
<div class="page-wrapper">
    <div class="container">
        <div class="row">
            <h3 class="page-header">Policies</h3>
            <?php
            $data = $this->session->flashdata('msg');
            echo $data;
            ?>
            <form id="addPolicyForm" action="" method="post" enctype="multipart/form-data">
                <div class="row-cat">
                    <label>Title</label>
                    <input type="text" name="policytitle" value="" class="required form-control">
                </div>
                <div class="row-cat">
                    <label>Description</label>
                    <textarea name="desc"  class="form-control"></textarea>
                </div>
                <div class="row-cat">
                    <label>Upload Forms And Format</label>
                    <input type='file' id="imgInp" name="file" />
                </div>
                <?php /*
                  <label>Valid Upto</label>
                  <input type="date" name="valdate" value=""><br>
                 */ ?>
                <div class="row-cat">
                    <label>Category</label>
                    <select name="category" class="required form-control">
                        <option value="">Select Any Category</option>
                        <?php foreach ($categories as $cat): ?>
                            <option value="<?php echo $cat->id; ?>"><?php echo $cat->name; ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
                <div class="row-cat">
                    <label>Region</label>
                    <input type="checkbox" name="all" value="All" id="selecctall"> General
                    <ul class="topLevel UL region-list">
                        <?php foreach ($region as $res): ?>
                            <li><input type="checkbox" class="checkbox1" name="" ><?php echo $res->name; ?>
                                <?php foreach ($usrhier as $usr): ?>
                                    <?php if ($res->id == $usr->region && $usr->accessLevel == 'Program Manager') { ?>
                                        <ul>
                                            <li><input type="checkbox" class="checkbox1" name="optional[]" value="<?php echo $usr->ID ?>"><?php echo $usr->name; ?>
                                                <ul>
                                                    <?php
                                                    foreach ($usrhier as $usr1):
                                                        if ($res->id == $usr1->region && $usr1->accessLevel == 'Program Officer') {
                                                            ?>
                                                            <li>
                                                                <input type="checkbox" class="checkbox1" name="optional[]" value="<?php echo $usr1->ID ?>"><?php echo $usr1->name; ?>
                                                                <ul>
                                                                    <?php foreach ($usrhier as $usr2): ?>
                                                                        <?php if ($usr2->parent_user_id == $usr1->ID): ?>
                                                                            <li><input type="checkbox" class="checkbox1" name="optional[]" value="<?php echo $usr2->ID ?>"><?php echo $usr2->name; ?>
                                                                            <?php endif; ?>
                                                                        <?php endforeach; ?>
                                                                </ul>
                                                            </li>
                                                        <?php } ?>
                                                    <?php endforeach; ?>
                                                </ul>
                                            </li>
                                        </ul>
                                    <?php } ?>
                                <?php endforeach; ?>
                            </li>
                        <?php endforeach; ?>
                    </ul>
                </div>
                <label></label>
                <div class="clearfix">
                    <input type="submit" name="submitBtn" value="Send" class="btn btn-primary">
                    </form>

                </div>
        </div>
    </div>
    <?php $this->load->view("footer"); ?>

    <script type="text/javascript">

        $(document).ready(function() {
            $('#selecctall').click(function(event) {  //on click
                if (this.checked) { // check select sta tus
                    $('.checkbox1').each(function() { //loop through each checkbox
                        this.checked = true;  //sel  ect all checkboxes with class "checkbox1"
                    });
                    $('.region-list').hide('slow');
                } else {
                    $('.checkbox1').each(function() { //loop through each checkbox 
                        this.checked = false; //deselect all checkboxes with class "checkbox1"
                    });
                    $('.region-list').show('slow');
                }
            });

            $("ul.topLevel UL input[type='checkbox' ] ").click(function() {
                var elem = $(this).siblings();
                //elem.css("background","red");
                if (this.checked) {
                    elem.find("input[type='checkbox']").each(function() {
                        this.checked = true;
                    });
                }
                else {
                    elem.find("input[type='checkbox']").each(function() {
                        this.checked = false;
                    });
                }
            });
        });

    </script>