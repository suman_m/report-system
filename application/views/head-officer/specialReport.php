<?php $this->load->view("head-officer/header"); ?>
    <script type="application/javascript" src="<?php echo site_url('assets/js/jquery-ui.js'); ?>"></script>
    <link rel="stylesheet" href="<?php echo site_url('assets/css/jquery-ui.css'); ?>" />
    <script src="<?php echo site_url('assets/js/jquery.validate.min.js'); ?>"></script>
<script>
    $(document).ready(function(){
        $('#myTab a').click(function(e) {
            e.preventDefault();
            $(this).tab('show');
        });
        $('#submissionButton').click(function(){
            var form = $('#formformat').val();
            var status = $('#status').val();
            if(form !='')
            {
                $.ajax({
                   url:'<?php echo site_url('head-officer/specialReport/submissionListData'); ?>',
                    type:'post',
                    data:'form='+form+'&status='+status,
                    success:function(response)
                    {
                        $('#submissionListData').html(response);
                    }
                });
            }
        });
    });
</script>
<div class="page-wrapper">
    <div class="container">
        <ul class="nav nav-tabs" id="myTab">
            <li class="active"><a href="#reports">Special Reports</a></li>
            <li><a href="#reportSubmissionList">Report Submission List</a></li>
        </ul>
    <div class="tab-content">
        <div id="reports" class="tab-pane fade in active">
        <h3 class="page-header">Special Report</h3>
            <?php echo $this->session->flashdata('error'); ?>
        <h4>Download Zip</h4>
        <form action="<?php echo site_url('head-officer/specialReport/downloadZip'); ?>" method="post">
            Select Form and Format : <select name="formformat">
                <option value="0">Choose form and format</option>
                <?php foreach($form_format as $f): ?>
                    <option value="<?php echo $f->id; ?>"><?php echo $f->form_format_name; ?></option>
                <?php endforeach; ?>
            </select>
            Region : <select name="region">
                <option value="0">Choose Region</option>
                <?php foreach($region as $r): ?>
                    <option value="<?php echo $r->id; ?>"><?php echo $r->name; ?></option>
                <?php endforeach; ?>
            </select>
            <input type="submit" name="submit" class="btn btn-default" />
        </form>
        <form action="" method="post" id="searchSpecialReport">
            Select Form and Format : <select name="formformat">
                <option value="0">Choose form and format</option>
                <?php foreach($form_format as $f): ?>
                    <option value="<?php echo $f->id; ?>"><?php echo $f->form_format_name; ?></option>
                <?php endforeach; ?>
            </select>

            Select Region :
                <select name="region">
                    <option value="0">Choose Region</option>
                    <?php foreach($region as $r): ?>
                        <option value="<?php echo $r->id; ?>"><?php echo $r->name; ?></option>
                    <?php endforeach; ?>
                </select>
            <input type="submit" name="submit" class="btn" />
        </form>
        <table class="table table-striped ">
            <tr>
                <th>SN</th>
                <th>Report Title</th>
                <th>Submitted By</th>
                <th>Report List</th>
                <th>Special Notes</th>
                <th>Submission Date</th>
            </tr>
            <?php
            $i = 1;
            foreach ($specialReport as $sr):
                ?>
                <tr>
                    <td><?php echo $i; ?></td>
                    <td><?php echo $sr->title; ?></td>
                    <td><?php echo $this->misc_lib->getUserName($sr->user_id); ?></td>
                    <td><?php echo $sr->files; ?> <p><a class="btn btn-primary" href="<?php echo site_url('head-officer/specialReport/downloadReport/' . $sr->id); ?>">Download</a></p></td>
                    <td><?php echo $sr->special_notes; ?></td>
                    <td><?php echo $sr->date; ?></td>
                </tr>
                <?php
                $i++;
            endforeach;
            ?>
        </table>

        <div id="reports-logs">
            <h3>Special Report Logs</h3>
            <table class="table table-striped ">
                    <tr>
                        <th>SN</th>
                        <th>Logs Message</th>
                        <th>Date</th>
                    </tr>
                <tbody>
                    <?php
                    $i = 1;
                    foreach ($logs as $l):
                        ?>

                        <?php $userData = $this->misc_lib->getUserName($l->user_id); ?>
                        <tr>
                            <td><?php echo $i; ?></td>
                            <td><?php echo $l->logs . ' by ' . $userData; ?></td>
                            <td><?php echo $l->date; ?></td>
                        </tr>
                        <?php
                        $i++;
                    endforeach;
                    ?>
                </tbody>
            </table>
        </div>
        </div>
            <div id="reportSubmissionList" class="tab-pane fade">
                Select Form and Format : <select name="formformat" id="formformat">
                    <?php foreach($form_format as $f): ?>
                        <option value="<?php echo $f->id; ?>"><?php echo $f->form_format_name; ?></option>
                    <?php endforeach; ?>
                </select>
                Select Status : <select name="status" id="status">
                    <option value="1">Submitted</option>
                    <option value="2">Not Submitted</option>
                </select>
                <input type="button" name="submissionButton" id="submissionButton" value="Search" class="btn" />
                <div id="submissionListData">

                </div>
                </div>

        </div>
    </div>
</div>
<?php $this->load->view("footer"); ?>