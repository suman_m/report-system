<?php $this->load->view('head-officer/header'); ?>
<script src="<?php echo site_url('assets/js/jquery.validate.min.js'); ?>"></script>
<script>
    $(document).ready(function() {
        $("#settingsForm").validate();
    });
</script>
<div class="page-wrapper">
    <div class="container">
        <h3 class="form-signin-heading"><?php echo $title; ?></h3>
        <form action="" method="post" enctype="multipart/form-data" id="settingsForm">
            <div class="form-group">
                <label>Organization Name</label><input type="text" name="organizationName" class="form-control required" value="<?php echo $content->organization_name; ?>" />
            </div>
            <div class="form-group">
                <label>Organization Tag Line</label><input type="text" name="organizationTagLine" class="form-control" value="<?php echo $content->organization_tag_line; ?>" />
            </div>
            <div class="form-group">
                <label>Organization Address</label><textarea name="organizationAddress" class="form-control required"><?php echo $content->organization_address; ?></textarea>
            </div>
            <div class="form-group">
                <img src="<?php echo site_url('assets/logo/' . $content->logo); ?>" width="100px" />
                <input type="hidden" name="image" value="<?php echo $content->logo; ?>" />
                <label>Organization Logo</label>
                <input type="file" name="logo" id="logo" class="form-control" />
            </div>
            <button class="btn btn-primary btn-block" name="submit" value="submit" type="submit">Save</button>
        </form>
    </div>
</div>
<?php $this->load->view("footer"); ?>