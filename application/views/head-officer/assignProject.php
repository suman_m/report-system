<?php $this->load->view('head-officer/header'); ?>
<section class="container">
    <h3 class="page-header"><?php echo $title; ?></h3>
    <span>Assign <?php echo $projectTitle; ?> to :</span>
    <form action="" method="post">
        <div class="form-group">
            <label>Project holder</label>
            <select name="projectHolder" class="required form-control">
                <option value="">Choose Project Holder</option>
                <?php foreach($projectHolder as $ph): ?>
                    <option value="<?php echo $ph->ID; ?>" <?php echo (isset($projectInfo) && $projectInfo->user_id == $ph->ID)?'selected="selected"':''; ?>><?php echo $ph->name; ?></option>
                <?php endforeach; ?>
            </select>
        </div>
        <input type="submit" name="submit" class="btn btn-sm btn-primary" value="Assign" />

    </form>
</section>
<?php $this->load->view('footer'); ?>