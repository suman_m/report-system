<?php $this->load->view("head-officer/header"); ?>
    <script src="<?php echo site_url('assets/js/jquery.validate.min.js'); ?>"></script>
    <script>
        $(document).ready(function(){
            $('#addProjectForm').validate();
        })
    </script>
    <div class="container">
        <div class="row">

            <?php $this->load->view("head-officer/leftNav"); ?>

            <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main marginLeft0 listWrap">
                <h3 class="page-header">Edit Project</h3>

                <form id="addProjectForm" class="small-forms" action="<?php echo site_url(); ?>head-officer/projects/update" method="post">
                    <input type="hidden" name="editid" value="<?php echo $project[0]->id; ?>"/>
                    <p><label for="projectName">Name:</label> <input type="text" name="projectName" id="projectName" value="<?php echo $project[0]->name; ?>" class="form-control required"/></p>
                    <p><label for="description">Description:</label> <textarea name="description" id="description" class="form-control required" rows="5"><?php echo $project[0]->description; ?></textarea></p>
                    <button class="btn btn-primary btn-block" type="submit">Save</button>
                </form>
            </div>

        </div>
    </div>
<?php $this->load->view("footer"); ?>