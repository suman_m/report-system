<?php $this->load->view("head-officer/header"); ?>
    <div class="container">
        <div class="row">

            <?php $this->load->view("head-officer/leftNav"); ?>

            <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main marginLeft0 listWrap">
                <div class="alert alert-danger margintop10" role="alert">Alert! to delete this user, following points needs to be considered.</div>
                <p>This officer consist of following subordinates working under him/her.</p>
                <?php
                    $map = '';
                    $allId = $id = $this->input->get("id");
                    foreach($userList as $user){
                        $flag = true;
                        foreach($userTree as $tree){
                            if($tree->user_id == $user->ID){
                                $map[$user->ID] = $tree->parent_user_id;
                                $flag = false;
                            }
                        }
                        if($flag)
                            $map[$user->ID] = '';
                    }
                ?>
                <ul>
                    <li>
                        <?php foreach($userList as $user): ?>
                            <?php if($user->ID == $id): ?>
                                <?php echo "<b>".$user->name . " - " . $user->accessLevel . "</b> (user you need to deleted)"; ?>
                                <ul>
                                    <?php foreach($userList as $user1): ?>
                                        <?php if($map[$user1->ID] == $user->ID): $allId .= ",".$user1->ID; ?>
                                            <li><?php echo $user1->name . " - " . $user1->accessLevel; ?>
                                                <ul>
                                                    <?php foreach($userList as $user2): ?>
                                                        <?php if($map[$user2->ID] == $user1->ID): $allId .= ",".$user2->ID; ?>
                                                            <li><?php echo $user2->name . " - " . $user2->accessLevel; ?></li>
                                                        <?php endif; ?>
                                                    <?php endforeach; ?>
                                                </ul>
                                            </li>
                                        <?php endif; ?>
                                    <?php endforeach; ?>
                                </ul>
                            <?php endif; ?>
                        <?php endforeach; ?>
                    </li>
                </ul>
                <p>So, either you need to delete this officer as well as entire subordinates under him. <a class="btn btn-sm btn-danger" href="<?php echo site_url("head-officer/users/delete_all?id=$allId"); ?>" onclick="return confirm('You understand the risk, are you sure to delete?')">Delete including all subordinates</a></p>
                <p>or, replace this position by other officer and you can delete after that. <a class="btn btn-sm btn-info" href="<?php echo site_url("head-officer/users/change_position?id=$id"); ?>">Replace the position</a></p>
            </div>

        </div>
    </div>
<?php $this->load->view("footer"); ?>