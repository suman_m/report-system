<?php $segment = $this->uri->segment(2); ?>
<?php $segment3 = $this->uri->segment(3); ?>
<div class="col-sm-3 col-md-2 sidebar">
    <ul class="nav nav-sidebar">
        <li class="<?php if($segment == "users" && $segment3 != "change_position"){ echo "active"; } ?>"><a href="<?php echo site_url(); ?>head-officer/users">Users</a></li>
        <li class="<?php if($segment3 == "change_position"){ echo "active"; } ?>"><a href="<?php echo site_url(); ?>head-officer/users/change_position">Change user designation</a></li>
        <li class="<?php if($segment == "regions"){ echo "active"; } ?>"><a href="<?php echo site_url(); ?>head-officer/regions">Regions</a></li>
        <li class="<?php if($segment == "projects"){ echo "active"; } ?>"><a href="<?php echo site_url(); ?>head-officer/projects">Projects</a></li>
    </ul>
</div>