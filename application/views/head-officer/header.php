<?php $this->load->view("header"); ?>
<?php $head = $this->common_model->getTableData('site_settings')->row(); ?>
<?php $segment1 = $this->uri->segment(1); ?>
<?php $segment2 = $this->uri->segment(2); ?>
<div id="header" style="background-color: rgb(238, 232, 214); border-bottom: 5px solid #FFFFFF;">
    <div class="container">
        <span style="vertical-align: middle; overflow: hidden;">
            <img style="float: left; margin: 10px;" src="<?php echo site_url('assets/logo/' . $head->logo) ?>" width="100px" />
            <h3 style="float: left; font-family: times new roman; font-size: 31px; color: #825A42; margin-top: 25px;"><?php echo $head->organization_name; ?></h3>
        </span>
    </div>

</div>
<div class="navbar navbar-default">
    <div class="container">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#report-system-nav">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <div class="navbar-collapse collapse" id="report-system-nav">
            <ul class="nav navbar-nav">
                <li class="<?php
                if ($segment2 == "reports") {
                    echo "active";
                }
                ?>"><a href="<?php echo site_url('head-officer/reports'); ?>">Reports</a>
                </li>
                <li class="<?php
                if ($segment2 == "backup") {
                    echo "active";
                }
                ?>"><a href="<?php echo site_url('head-officer/backup'); ?>">Backups</a>
                </li>
                <li class="<?php
                if ($segment2 == "specialReport" || $segment2 == "forms") {
                    echo "active";
                }
                ?>">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" >Special Reports <span class="caret"></span></a>
                    <ul class="dropdown-menu" role="menu">
                        <li>
                            <a href="<?php echo site_url('head-officer/specialReport'); ?>">List Special Report</a>
                        </li>
                        <?php if ($this->session->userdata("access_level") == "Head Officer" && $this->session->userdata("sub_level") == "Manager") { ?>
                            <li>
                                <a href="<?php echo site_url('head-officer/forms'); ?>">Forms & Formats</a>
                            </li>
                        <?php } ?> 
                    </ul>
                </li>
                <?php if ($this->session->userdata("access_level") == "Head Officer" && $this->session->userdata("sub_level") == "Manager") { ?>
                    <li class="<?php
                    if ($segment2 == "policy") {
                        echo "active";
                    }
                    ?>">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" >Policies <span class="caret"></span></a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="<?php echo site_url('head-officer/policy'); ?>">Policies</a></li>
                            <li><a href="<?php echo site_url('head-officer/policy/list_category'); ?>">Category</a></li>
                        </ul>
                    </li>
                <?php } ?>
                <li class="<?php
                if ($segment2 == "others") {
                    echo "active";
                }
                ?>">
                    <a href="<?php echo site_url('head-officer/others'); ?>">Others</a>
                </li>
                <?php if ($this->session->userdata("access_level") == "Head Officer" && $this->session->userdata("sub_level") == "Manager") { ?>
                    <li class="<?php
                    if ($segment2 == "users") {
                        echo "active";
                    }
                    ?>"><a href="<?php echo site_url(); ?>head-officer/users">Users</a>
                    </li>
                <?php } ?>
                <li class="<?php
                if ($segment1 == "myAccount") {
                    echo "active";
                }
                ?>"><a href="<?php echo site_url(); ?>myAccount">My Account</a></li>
                    <?php /*
                      <li class="<?php
                      if ($segment1 == "settings") {
                      echo "active";
                      }
                      ?>">
                      <a href="<?php echo site_url('head-officer/settings'); ?>">Settings</a></li>
                     */ ?>
                <li><a href="<?php echo site_url('loginpage/logout') ?>">Logout</a></li>
            </ul>
        </div>
    </div>
</div>