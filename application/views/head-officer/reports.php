<?php $this->load->view("head-officer/header"); ?>
<script type="application/javascript" src="<?php echo site_url('assets/js/jquery-ui.js'); ?>"></script>
<link rel="stylesheet" href="<?php echo site_url('assets/css/jquery-ui.css'); ?>" />
    <script src="<?php echo site_url('assets/js/jquery.validate.min.js'); ?>"></script>
<script>
    $(document).ready(function() {
        $('#myTab a').click(function(e) {
            e.preventDefault();
            $(this).tab('show');
        });
        $('#dateStart').datepicker();
        $('#dateEnd').datepicker();
        $('#add-report-type').click(function() {
            var total = parseInt($('#total').val());
            var text = '<div class="form-group" id="reportType-' + total + '"><label>Report Type</label><input  type="text" name="reportType[]" class="form-control required" /><span><a href="javascript:;" id="reportData-' + total + '" onclick="removeReportType(this .id)">Remove</a></span></div>';
            total = total + 1;
            $('#report-type').append(text);
            $('#total').val(total);
        });
        $('#quarterStartTime').datepicker();
        $('#reportSubmissionTime').change(function() {
            var value = $('#reportSubmissionTime').val();
            if (value != '')
            {
                if (value == "quar terly")
                    $('#quarter-start').show();
                else
                    $('#quarter-start').hide();
            }
            else
                $('#quarter-start').hide();
        });
        $("#zipDownload").validate();
    });
    function removeReportType(id)
    {
        var value = id.split('-');
        $('#reportType-' + value['1']).remove();
    }

    function reportAccept(report_id, report_type_id)
    {

        $.ajax({
            url: '<?php echo site_url('head-officer/reports/acceptReport'); ?>',
            data: 'report_id=' + report_id + '&report_type_id=' + report_type_id,
            type: 'post',
            success: function(response)
            {

            }
        });
    }
    function reportReject(report_id, report_type_id)
    {

        $.ajax({
            url: '<?php echo site_url('head-officer/reports/rejectReport'); ?>',
            data: 'report_id=' + report_id + '&report_type_id=' + report_type_id,
            type: 'post',
            success: function(response)
            {
            }
        });
    }
</script>
<div class="page-wrapper">
    <div class="container">
        <h3 class="page-header">Reports</h3>
        <?php echo $this->session->flashdata('success'); ?>
        <ul class="nav nav-tabs" id="myTab">
            <li class="active"><a href="#reports">Manage Reports</a></li>
            <li><a href="#settings">Report Settings</a></li>
            <li><a href="#reportSubmissionList">Report Submission List</a></li>
        </ul>
        <div class="tab-content">
            <div id="reports" class="tab-pane fade in active">
                <?php echo $this->session->flashdata('month'); ?>
                <fieldset>
                    <form action="<?php echo site_url('head-officer/reports/downloadZip'); ?>" method="post" id="zipDownload">
                        <table class="field-table">
                            <tr>
                                <td width="120" style="vertical-align: middle;"><label>Choose Month : </label></td>
                                <td width="300">
                                    <select name="month" class="required form-control">
                                        <option value="">Month to download the report of</option>
                                        <?php for ($i = 1; $i <= 12; $i++) { ?>
                                            <option value="<?php echo $i; ?>" <?php echo (isset($month) && $month == $i) ? 'selected="selected"' : ''; ?>><?php echo date('F', strtotime('01-' . $i . '-2001')); ?></option>
                                        <?php } ?>
                                    </select>
                                </td>
                                <td>
                                    <select name="region" class="required form-control">
                                        <option value="0">Choose Region</option>
                                        <?php foreach ($regions as $re): ?>
                                            <option value="<?php echo $re->id; ?>"><?php echo $re->name; ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </td>
                                <td style="vertical-align: middle; padding: 0 15px;">
                                    <input type="submit" name="submit" class="btn btn-primary" value="Download Zip" />
                                </td>
                            </tr>
                        </table>
                    </form>
                    <hr>
                    <form action="" method="post">
                        <table width="100%" class="field-table">
                            <tr>
                                <td width="120" style="vertical-align: middle; padding: 0 15px;">Date Start : </td><td><input class="required form-control" type="text" name="dateStart" id="dateStart" value="<?php echo (isset($dateStart)) ? $dateStart : ''; ?>"  /></td>
                                <td width="120" style="vertical-align: middle; padding: 0 15px;">Date End : </td><td><input class="required form-control" type="text" name="dateEnd" id="dateEnd" value="<?php echo (isset($dateEnd)) ? $dateEnd : ''; ?>"  /></td>
                                <td width="120" style="vertical-align: middle;padding: 0 15px;">Month : </td>
                                <td>
                                    <select name="month" class="required form-control">
                                        <option value="0">Choose Month</option>
                                        <?php for ($i = 1; $i <= 12; $i++) { ?>
                                            <option value="<?php echo $i; ?>"><?php echo date('F', strtotime('01-' . $i . '-2001')); ?></option>
                                        <?php } ?>
                                    </select>
                                </td>
                            </tr>
                            <tr>

                                <td width="120" style="vertical-align: middle; padding: 0 15px;">Region : </td>
                                <td>
                                    <select name="region" class="required form-control">
                                        <option value="0">Choose Region</option>
                                        <?php foreach ($regions as $re): ?>
                                            <option value="<?php echo $re->id; ?>" <?php echo (isset($region) && $region == $re->id) ? 'selected="selected"' : ''; ?>><?php echo $re->name; ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </td>
                                <td width="120" style="vertical-align: middle; padding: 0 15px;">Project :</td>
                                <td>
                                    <select name="project" class="required form-control">
                                        <option value="0">Choose Project</option>
                                        <?php foreach ($projects as $pro): ?>
                                            <option value="<?php echo $pro->id; ?>" <?php echo (isset($project) && $project == $pro->id) ? 'selected="selected"' : ''; ?>><?php echo $pro->name; ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </td>
                                <td style="vertical-align: middle; padding: 0 15px;">
                                    <input type="submit" name="submit" value="Search" class="btn btn-primary"/>
                                </td>
                            </tr>
                        </table>


                    </form>
                </fieldset>
                <h3>Manage Reports</h3>
                <?php if (!empty($reports)) { ?>
                    <table class="table table-striped ">
                        <tr>
                            <th>SN</th>
                            <th>Report Title</th>
                            <th>Report List</th>
                            <th>Report of the month</th>
                            <th>Project Name</th>
                            <th>Special Notes</th>
                            <th>Submitted By</th>
                            <th>Submission Date</th>
                        </tr>

                        <?php
                        $i = 1;
                        foreach ($reports as $re):
                            ?>
                            <?php
                            $files = json_decode($re->files);

                            $key_array = array();
                            foreach ($files as $key => $val):
                                $key_array[] = $key;
                            endforeach;
                            ?>
                            <tr>
                                <td><?php echo $i; ?></td>
                                <td><?php echo $re->title; ?></td>
                                <td>
                                    <?php foreach ($reportType as $v): $id = $v->id; ?>
                                        <?php if (in_array($v->id, $key_array)): ?>
                                            <?php
                                            if (!empty($reportStatus)) {
                                                foreach ($reportStatus as $r_s):
                                                    if ($r_s->report_id == $re->id && $r_s->report_type_id == $v->id) {
                                                        if ($r_s->status == 1) {
                                                            $accepted = 'checked="checked"';
                                                            $rejected = '';
                                                            break;
                                                        } else if ($r_s->status == 2) {
                                                            $rejected = 'checked="checked"';
                                                            $accepted = '';
                                                            break;
                                                        }
                                                    } else {
                                                        $accepted = '';
                                                        $rejected = '';
                                                    }
                                                endforeach;
                                            } else {
                                                $accepted = '';
                                                $rejected = '';
                                            }
                                            ?>
                                            <span>
                                                <p><?php echo $v->title; ?></p>
                                                <p><?php echo $files->$id; ?><a class="btn btn-primary" href="<?php echo site_url('head-officer/reports/downloadReport/' . $re->id . '/' . $v->id); ?>">Download Report</a></p>
                                                <p><input type="radio" name="report_<?php echo $v->id; ?>" value="1" onclick="reportAccept('<?php echo $re->id; ?>', '<?php echo $v->id; ?>');" <?php echo $accepted; ?> /> Accept <input type="radio" name="report_<?php echo $v->id; ?>" value="0" onclick="reportReject('<?php echo $re->id; ?>', '<?php echo $v->id; ?>');" <?php echo $rejected; ?> /> Reject</p>
                                            </span>
                                        <?php endif; ?>
                                    <?php endforeach; ?>
                                </td>
                                <td><?php echo date('F', strtotime('01-' . $re->month . '-' . date('Y'))); ?></td>
                                <td><?php echo $this->misc_lib->getProjectName($re->project_id); ?></td>
                                <td><?php echo $re->special_notes; ?></td>
                                <td><?php echo $this->misc_lib->getUserName($re->user_id); ?></td>
                                <td><?php echo $re->date; ?></td>

                            </tr>
                            <?php
                            $i++;
                        endforeach;
                        ?>
                    </table>
                    <?php
                } else {
                    echo "No Reports Available";
                }
                ?>

                <div id="reports-logs">
                    <h3>Reports Logs</h3>
                    <table class="table table-striped ">
                        <tr>
                            <th>SN</th>
                            <th>Logs Message</th>
                            <th>Date</th>
                        </tr>
                        <?php
                        $i = 1;
                        foreach ($reportLogs as $r_l):
                            ?>
                            <tr>
                                <td><?php echo $i; ?></td>
                                <td>
                                    <?php $user_data = $this->misc_lib->getUserData($r_l->user_id); ?>
                                    <?php echo $this->misc_lib->logsMessage($r_l->logs) . ' by ' . $user_data->name . '(' . $user_data->accessLevel . ')'; ?>
                                </td>
                                <td><?php echo $r_l->date; ?></td>
                            </tr>
                            <?php
                            $i++;
                        endforeach;
                        ?>
                    </table>
                </div>
            </div>
            <div id="settings" class="tab-pane fade">
                <h3 class="form-signin-heading">Report Type Settings</h3>
                <form action="<?php echo site_url('head-officer/reports/addReportType'); ?>" method="post">
                    <div id="report-type">
                        <?php
                        $i = 1;
                        if (!isset($value)) {
                            $id = '';
                            ?>
                            <div class="form-group"><label>Report Type</label><input type="text" name="reportType[]" class="form-control required"  /> </div>
                            <?php
                        } else {
                            $id = '';
                            foreach ($reportType as $r_t):
                                $id .= $r_t->id . ',';
                                ?>
                                <div class="form-group" <?php echo ($i != 1) ? 'id="reportType-' . $i . '"' : ''; ?>><label>Report Type</label><input type="text" name="reportCategory-<?php echo $r_t->id; ?>" class="form-control required" value="<?php echo $r_t->title; ?>" /><?php if ($i != 1): ?><span><a href="javascript:;" id="reportData-<?php echo $i; ?>" onclick="removeReportType(this.id)">Remove</a></span><?php endif; ?></div>
                                <?php
                                $i++;
                            endforeach;
                            $id = trim($id, ',');
                            ?>
                            <?php
                        }
                        ?>
                        <input type="hidden" name="id" value="<?php echo $id; ?>" />
                    </div>
                    <input type="hidden" name="total" value="<?php echo $i; ?>" id="total" />
                    <div><a href="javascript:;" id="add-report-type">Add New</a></div>
                    <div class="form-group"><label>Report Submission Time</label>
                        <select name="reportSubmissionTime" class="form-control required" id="reportSubmissionTime">
                            <option value="">Choose Report Submission Time</option>
                            <option value="monthly" <?php echo (isset($value) && $value->report_submission_time == "monthly") ? 'selected="selected"' : ''; ?>>Monthly</option>
                           <!-- <option value="quarterly" <?php echo (isset($value) && $value->report_submission_time == "quarterly") ? 'selected="selected"' : ''; ?>>Quarterly</option> -->

                        </select>
                    </div>
                    <?php
                    if (isset($value) && $value->report_submission_time == "quarterly") {
                        $style = "";
                    } else {
                        $style = "display:none";
                    }
                    ?>
                   <!-- <div class="form-group" id="quarter-start" style="<?php echo $style; ?>">
                        <label>Quarter Start Time</label>
                        <input type="text" name="quarter_start_time" class="form-control" id="quarterStartTime" value="<?php echo (isset($value)) ? $value->quarterly_start : ''; ?>" />
                    </div> -->

                    <div class="form-group">
                        <label>Report Submission End</label>
                        <select name="report_submission_end" class="form-control">
                            <option value="">Choose Report Submission End Peroid</option>
                            <!--<option value="last-day" <?php echo (isset($value) && $value->submission_end == "last-day") ? 'selected="selected"' : ''; ?>>Last day of the month</option>
                            <option value="last-friday" <?php echo (isset($value) && $value->submission_end == "last-friday") ? 'selected="selected"' : ''; ?>>Last Friday of the month</option> -->
                            <option value="next-month" <?php echo (isset($value) && $value->submission_end == "next-month") ? 'selected="selected"' : ''; ?>>5th of the next month</option>
                        </select>

                    </div>

                    <div class="form-group">
                        <label>Email Frequency</label>
                        <select class="form-control" name="email_frequency">
                            <option value="">Choose notification sending time</option>
                            <option value="10" <?php echo (isset($value) && $value->email_frequency == "10") ? 'selected="selected"' : ''; ?>>Before 10 days</option>
                            <option value="8" <?php echo (isset($value) && $value->email_frequency == "8") ? 'selected="selected"' : ''; ?>>Before 8 days</option>
                            <option value="5" <?php echo (isset($value) && $value->email_frequency == "5") ? 'selected="selected"' : ''; ?>>Before 5 days</option>
                            <option></option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Status </label><input type="checkbox" name="status" value="1" class="checkbox" <?php echo (isset($value) && $value->status == "1") ? 'checked="checked"' : ''; ?> />
                    </div>
                    <button class="btn btn-primary btn-block" name="submit" value="submit" type="submit">Save</button>
                </form>

            </div>
            <div id="reportSubmissionList" class="tab-pane fade">
                <script>
                    $(document).ready(function() {
                        /*$('#month').change(function() {
                            var value = $('#month').val();
                            if (value != '')
                            {
                                $.ajax({
                                    url: '<?php echo site_url('head-officer/reports/getSubmissionList') ?>',
                                    data: 'month=' + value,
                                    type: 'post',
                                    success: function(response)
                                    {
                                        $('#submissionData').html(response);
                                    }
                                });
                            }
                        }); */
                        $('#search-filter').click(function(){
                            var month = $('#month').val();
                            var status = $('#status').val();
                            if (month != '')
                            {
                                $.ajax({
                                    url: '<?php echo site_url('head-officer/reports/getSubmissionList') ?>',
                                    data: 'month='+ month+'&status='+status,
                                    type: 'post',
                                    success: function(response)
                                    {
                                        $('#submissionData').html(response);
                                    }
                                });
                            }
                        });
                    })
                </script>
                <form>
                <select name="month" id="month">
                    <option value="">Choose Month</option>
                    <?php
                    $currentMonth = $months + 2;
                    for ($i = $months; $i <= $currentMonth; $i++) {
                        ?>
                        <option value="<?php echo $i; ?>"><?php echo date('F', strtotime('01-' . $i . '-2001')); ?></option>
                        <?php
                    }
                    ?>
                </select>
                <select name="status" id="status">
                    <option value="0">Submitted</option>
                    <option value="1">Accepted</option>
                    <option value="2">Rejected</option>
                    <option value="3">Not Submitted</option>
                </select>
                    <input type="button" value="Filter" id="search-filter" />
                </form>
                <div id="submissionData"></div>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view("footer"); ?>