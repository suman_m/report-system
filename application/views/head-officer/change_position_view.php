<?php $this->load->view("head-officer/header"); ?>
<script src="<?php echo site_url('assets/js/jquery.validate.min.js'); ?>"></script>
<script>
    $(document).ready(function() {
        $("#assign").validate();
        $("#swap").validate();
        $("#addUserForm").validate();
    });
</script>
<div class="page-wrapper">
    <div class="container">
        <div class="row">

            <?php $this->load->view("head-officer/leftNav"); ?>

            <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main marginLeft0">
                <h3 class="page-header">Change User Designation</h3>
                <?php if ($msg = $this->session->flashdata('swap')): ?>
                    <div class="alert alert-success margintop10" role="alert"><?php echo $msg; ?></div>
                <?php endif; ?>
                <?php $id = $this->input->get("id") ? $this->input->get("id") : 0; ?>
                <?php /* Removed Assign as Feature
                  <form class="small-forms" action="<?php echo site_url(); ?>head-officer/users/assign_as" method="post" id="assign">
                  <p>
                  <label for="person1">Assign</label>
                  <select name="person1" id="" class="form-control required">
                  <option value="">Select Officer</option>
                  <?php foreach($inactive as $user): ?>
                  <option value="<?php echo $user->ID; ?>"><?php echo $user->name; ?></option>
                  <?php endforeach; ?>
                  </select>
                  </p>
                  <p>
                  <label for="person2">As</label>
                  <select name="person2" id="" class="form-control required">
                  <option value="">Select Officer</option>
                  <?php foreach($active as $user): ?>
                  <option value="<?php echo $user->ID; ?>" <?php if($id == $user->ID){ echo "selected"; } ?>><?php echo $user->name; ?></option>
                  <?php endforeach; ?>
                  </select>
                  </p>
                  <button type="submit" class="btn btn-sm btn-primary">Save</button>
                  </form>
                 */ ?>
                <div class="line1"></div>

                <?php $id = $this->input->get("id") ? $this->input->get("id") : 0; ?>
                <form class="small-forms" action="<?php echo site_url(); ?>head-officer/users/swap" method="post" id="swap">
                    <p>
                        <label for="person1">Assign</label>
                        <select name="person1" id="" class="form-control required">
                            <option value="">Select Officer</option>
                            <?php foreach ($inactive as $user): ?>
                                <option value="<?php echo $user->ID; ?>"><?php echo $user->name; ?></option>
                            <?php endforeach; ?>
                        </select>
                    </p>
                    <p>
                        <label for="person2">In place of</label>
                        <select name="person2" id="" class="form-control required">
                            <option value="">Select Officer</option>
                            <?php foreach ($active as $user): ?>
                                <option value="<?php echo $user->ID; ?>" <?php
                                if ($id == $user->ID) {
                                    echo "selected";
                                }
                                ?>><?php echo $user->name; ?></option>
                                    <?php endforeach; ?>
                        </select>
                        <?php /* Removed "Delete this user as well"
                          <input type="checkbox" name="deletealso" value="1"/> Delete this user as well

                         */ ?>
                    </p>
                    <button type="submit" class="btn btn-sm btn-primary">Save</button>
                </form>

                <div class="line1"></div>

                <?php if ($this->input->get('action') != "delete"): ?>
                    <?php if ($msg = $this->session->flashdata('assign')): ?>
                        <div class="alert alert-success margintop10" role="alert"><?php echo $msg; ?></div>
                    <?php endif; ?>
                    <form id="addUserForm" class="small-forms" action="<?php echo site_url(); ?>head-officer/users/assign" method="post">
                        <input type="hidden" name="action" value="<?php echo $this->input->get('action'); ?>"/>
                        <p>
                            <label for="person1">Assign</label>
                            <select name="person1" id="" class="form-control required">
                                <option value="">Select Officer</option>
                                <?php foreach ($inactive as $user): ?>
                                    <option value="<?php echo $user->ID; ?>"><?php echo $user->name; ?></option>
                                <?php endforeach; ?>
                            </select>
                        </p>
                        <p class="acclvlWrap"><label for="accesslevel">Access Level:</label>
                            <select name="accesslevel" id="accesslevel" class="form-control required">
                                <option value="">Select an Access Level</option>
                                <?php foreach ($accesslvl as $aclvl): ?>
                                    <option value="<?php echo $aclvl->name; ?>"><?php echo $aclvl->name; ?></option>
                                <?php endforeach; ?>
                            </select>
                            <span id="alert-message"></span>
                        </p>
                        <p class="regionWrap"><label for="region">Region:</label>
                            <select name="region" id="region" class="form-control">
                                <option value="">Select a Region</option>
                                <?php foreach ($regions as $region): ?>
                                    <option value="<?php echo $region->id; ?>"><?php echo $region->name; ?></option>
                                <?php endforeach; ?>
                            </select>
                        </p>
                        <p class="parentOfficer"><label for="parentuser">Direct Officer above this user:</label>
                            <select name="parentuser" id="parentuser" class="form-control">
                                <option value="">Select an Officer</option>
                                <?php foreach ($userList as $user): ?>
                                    <option value="<?php echo $user->ID; ?>"><?php echo $user->name; ?></option>
                                <?php endforeach; ?>
                            </select>
                        </p>
                        <p class="userProject"><label for="userProject">Assign Project for this user:</label>
                            <select name="userProject" id="userProject" class="form-control">
                                <option value="">Select a Project</option>
                                <?php foreach ($projects as $project): ?>
                                    <?php if ($project->assign_to_project_holder == 0) { ?><option value="<?php echo $project->id; ?>"><?php echo $project->name; ?></option><?php } ?>
                                <?php endforeach; ?>
                            </select>
                        </p>
                        <button type="submit" class="btn btn-sm btn-primary">Save</button>
                    </form>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function() {
        $('#accesslevel').change(function() {
            var accessLevel = $('#accesslevel').val();
            if (accessLevel == "Program Manager")
            {
                var msg = "The Current program manager of this region (if  exists) will be replaced by the user you have choose.";
                $('#alert-message').html(msg);
            }
            else
            {
                $('#alert-message').html('');
            }
        });
    });
</script>
<?php $this->load->view("footer"); ?>