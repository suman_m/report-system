<?php $this->load->view('head-officer/header'); ?>
<script type="application/javascript" src="<?php echo site_url('assets/js/jquery-ui.js'); ?>"></script>
<link rel="stylesheet" href="<?php echo site_url('assets/css/jquery-ui.css'); ?>" />
<script>
    $(document).ready(function() {
        $('#myTab a').click(function(e) {
            e.preventDefault();
            $(this).tab('show');
        });
        $('#quarterStartTime').datepicker();
        $('#backupSubmissionTime').change(function() {
            var value = $('#backupSubmissionTime').val();
            if (value != '')
            {
                if (value == "quarterly")
                    $('#quarter-start').show();
                else
                    $('#quarter-start').hide();

            }
            else
                $('#quarter-start').hide();
        });
    });
    function backupAccept(id)
    {
        $.ajax({
            url: '<?php echo site_url('head-officer/backup/acceptBackup') ?>',
            data: 'id=' + id,
            type: 'post',
            success: function(response)
            {
                $('#backup-status').html('Accepted');
            }
        }
        );
    }
    function backupReject(id)
    {
        $.ajax({
            url: '<?php echo site_url('head-officer/backup/rejectBackup') ?>',
            data: 'id=' + id,
            type: 'post',
            success: function(response)
            {
                $('#backup-status').html('Rejected');
            }
        });
    }
</script>
<div class="page-wrapper">
    <div class="container">
        <h3 class="page-header">Backups</h3>
        <ul class="nav nav-tabs" id="myTab">
            <li class="active"><a href="#backup">Manage Backup</a></li>
            <li><a href="#settings">Backup Settings</a></li>
            <li><a href="#submissionList">Backup Submission List</a></li>
        </ul>
        <div class="tab-content">
            <div id="backup" class="tab-pane fade in active">
                <h3>Manage Backup</h3>
                <p></p>
                <fieldset>
                    <form action="<?php echo site_url('head-officer/backup/downloadZip'); ?>" method="post" id="zipDownload">
                        <table class="field-table">
                            <tr>
                                <td width="120" style="vertical-align: middle;"><label>Choose Month : </label></td>
                                <td width="300">
                                    <select name="month" class="required form-control">
                                        <option value="">Month to download the backup of</option>
                                        <?php for ($i = 1; $i <= 12; $i++) { ?>
                                            <option value="<?php echo $i; ?>" <?php echo (isset($month) && $month == $i) ? 'selected="selected"' : ''; ?>><?php echo date('F', strtotime('01-' . $i . '-2001')); ?></option>
                                        <?php } ?>
                                    </select>
                                </td>
                                <td>
                                    <select name="region" class="required form-control">
                                        <option value="0">Choose Region</option>
                                        <?php foreach ($regions as $re): ?>
                                            <option value="<?php echo $re->id; ?>"><?php echo $re->name; ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </td>
                                <td style="vertical-align: middle; padding: 0 15px;">
                                    <input type="submit" name="submit" class="btn btn-primary" value="Download Zip" />
                                </td>
                            </tr>
                        </table>
                    </form>
                    <hr>
                    <form action="" method="post">
                        <table  width="100%" class="field-table">
                            <tr>
                                <td width="120" style="vertical-align: middle; padding: 0 15px;">Month : </td>
                                <td width="300">
                                    <select name="month" class="required form-control">
                                        <option value="0">Choose Month</option>
                                        <?php for ($i = 1; $i <= 12; $i++) { ?>
                                            <option value="<?php echo $i; ?>" <?php echo (isset($month) && $month == $i) ? 'selected="selected"' : ''; ?>><?php echo date('F', strtotime('01-' . $i . '-2001')); ?></option>
                                        <?php } ?>

                                    </select>
                                </td>
                                <td width="120" style="vertical-align: middle; padding: 0 15px;">Region :</td>
                                <td width="300">
                                    <select name="region" class="required form-control">
                                        <option value="0">Choose Region</option>
                                        <?php foreach ($regions as $re): ?>
                                            <option value="<?php echo $re->id; ?>" <?php echo (isset($region) && $region == $re->id) ? 'selected="selected"' : ''; ?>><?php echo $re->name; ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </td>
                                <td style="vertical-align: middle; padding: 0 15px;">
                                    <input type="submit" name="search" value="Search" class="btn btn-primary"/>
                                </td>
                            </tr>
                        </table>
                    </form>
                </fieldset>
                <p></p> 
                <table class="table table-striped ">
                    <tr>
                        <th>SN</th>
                        <th>Backup Title</th>
                        <th>Backup List</th>
                        <th>Backup for the month of</th>
                        <th>Special Notes</th>
                        <th>Submitted By</th>
                        <th>Submission Date</th>
                        <th>Status</th>
                    </tr>
                    <tbody>
                        <?php
                        $i = 1;
                        foreach ($backupData as $bd):
                            ?>
                            <?php $backup = json_decode($value->content); ?>
                            <?php $file = json_decode($bd->files); ?>
                            <tr>
                                <td><?php echo $i; ?></td>
                                <td><?php echo $bd->title; ?></td>
                                <td>
                                    <?php foreach ($backup as $k => $bk): ?>
                                        <?php foreach ($file as $ke => $fi): ?>
                                            <?php if ($k == $ke): ?>
                                                <?php echo "<p>" . $bk . "</p>"; ?>
                                                <?php echo "<p>" . $fi . "</p>"; ?>
                                                <p><a class="btn btn-primary" href="<?php echo site_url('head-officer/backup/downloadBackup/' . $bd->id . '/' . $k); ?>">Download</a></p>
                                            <?php endif; ?>
                                        <?php endforeach; ?>
                                    <?php endforeach; ?>
                                </td>
                                <td><?php echo date('F', strtotime('01-' . $bd->month . '-' . date('Y'))); ?></td>
                                <td><?php echo $bd->special_notes; ?></td>
                                <td><?php echo $this->misc_lib->getUserName($bd->user_id); ?></td>
                                <td><?php echo $bd->date; ?></td>
                                <td id="backup-status">
                                    <?php
                                    if ($bd->status == 1)
                                        echo "Accepted";
                                    else if ($bd->status == 2)
                                        echo "Rejected";
                                    else {
                                        ?>
                                        <input type="radio" name="status-<?php echo $bd->id; ?>" value="1" class="status" onclick="backupAccept('<?php echo $bd->id; ?>');" /> Accepted
                                        <input type="radio" name="status-<?php echo $bd->id; ?>" value="2" class="status" onclick="backupReject('<?php echo $bd->id; ?>');" /> Rejected
                                        <?php
                                    }
                                    ?>
                                </td>
                            </tr>
                            <?php
                            $i++;
                        endforeach;
                        ?>
                    </tbody>

                </table>

                <div id="backup-logs">
                    <h3>Backup Logs</h3>
                    <table class="table table-striped ">
                        <tr>
                            <th>SN</th>
                            <th>Logs Message</th>
                            <th>Date</th>
                        </tr>
                        <?php
                        $i = 1;
                        foreach ($backupLogs as $bl):
                            ?>
                            <tr>
                                <td><?php echo $i; ?></td>
                                <td>
                                    <?php $user_data = $this->misc_lib->getUserData($bl->user_id); ?>
                                    <?php echo $bl->logs . ' by ' . $user_data->name . '(' . $bl->access_level . ')'; ?></td>
                                <td><?php echo $bl->date; ?></td>
                            </tr>
                            <?php
                            $i++;
                        endforeach;
                        ?>

                    </table>
                </div>
            </div>
            <div id="settings" class="tab-pane fade">
                <h3 class="form-signin-heading">Backup Type Settings</h3>
                <form action="" method="post">
                    <?php
                    if (isset($value)) {
                        $backupType = json_decode($value->content);
                    }
                    ?>
                    <div class="form-group">
                        <label>Backup Name</label>
                        <input type="text" name="backupName[]" class="form-control" id="backupName" value="<?php echo (isset($value) && !empty($backupType)) ? $backupType->first : ''; ?>" />
                    </div>
                    <div class="form-group">
                        <label>Backup Name</label>
                        <input type="text" name="backupName[]" class="form-control" id="backupName" value="<?php echo (isset($value) && !empty($backupType)) ? $backupType->second : ''; ?>" />
                    </div>
                    <div class="form-group"><label>Backup Submission Time</label>
                        <select name="backupSubmissionTime" class="form-control required" id="backupSubmissionTime">
                            <option value="">Choose Backup Submission Time</option>
                            <option value="monthly" <?php echo (isset($value) && $value->report_submission_time == "monthly") ? 'selected="selected"' : ''; ?>>Monthly</option>
                            <option value="quarterly" <?php echo (isset($value) && $value->report_submission_time == "quarterly") ? 'selected="selected"' : ''; ?>>Quarterly</option>
                        </select>
                    </div>
                    <?php
                    if (isset($value) && $value->report_submission_time == "quarterly") {
                        $style = "";
                    } else {
                        $style = "display:none";
                    }
                    ?>
                    <div class="form-group" id="quarter-start" style="<?php echo $style; ?>">
                        <label>Quarter Start Time</label>
                        <input type="text" name="quarter_start_time" class="form-control" id="quarterStartTime" value="<?php echo (isset($value)) ? $value->quarterly_start : ''; ?>" />
                    </div>

                    <div class="form-group">
                        <label>Backup Submission End</label>
                        <select name="report_submission_end" class="form-control">
                            <option value="">Choose Backup Submission End Peroid</option>
                            <option value="last-day" <?php echo (isset($value) && $value->submission_end == "last-day") ? 'selected="selected"' : ''; ?>>Last day of the month</option>
                            <option value="last-friday" <?php echo (isset($value) && $value->submission_end == "last-friday") ? 'selected="selected"' : ''; ?>>Last Friday of the month</option>
                        </select>

                    </div>

                    <div class="form-group">
                        <label>Email Frequency</label>
                        <select class="form-control" name="email_frequency">
                            <option value="">Choose notification sending time</option>
                            <option value="10" <?php echo (isset($value) && $value->email_frequency == "10") ? 'selected="selected"' : ''; ?>>Before 10 days</option>
                            <option value="8" <?php echo (isset($value) && $value->email_frequency == "8") ? 'selected="selected"' : ''; ?>>Before 8 days</option>
                            <option value="5" <?php echo (isset($value) && $value->email_frequency == "5") ? 'selected="selected"' : ''; ?>>Before 5 days</option>
                            <option></option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Status </label><input type="checkbox" name="status" value="1" class="checkbox" <?php echo (isset($value) && $value->status == "1") ? 'checked="checked"' : ''; ?> />
                    </div>
                    <button class="btn btn-primary btn-block" name="submit" value="submit" type="submit">Save</button>
                </form>
            </div>
            <div id="submissionList" class="tab-pane fade">
                <script>
                    $(document).ready(function() {
                        $('#month').change(function() {
                            var value = $('#month').val();
                            if (value != '')
                            {
                                $.ajax({
                                    url: '<?php echo site_url('head-officer/backup/getSubmissionList') ?>',
                                    data: 'month=' + value,
                                    type: 'post',
                                    success: function(response)
                                    {
                                        $('#submissionData').html(response);
                                    }
                                });
                            }
                        });
                    })
                </script>
                <select name="month" id="month">
                    <option value="">Choose Month</option>
                    <?php
                    $currentMonth = $months + 2;
                    for ($i = $months; $i <= $currentMonth; $i++) {
                        ?>
                        <option value="<?php echo $i; ?>"><?php echo date('F', strtotime('01-' . $i . '-2001')); ?></option>
                    <?php
                    }
                    ?>
                </select>
                <div id="submissionData"></div>
                </div>
        </div>

    </div>
</div>
<?php $this->load->view("footer"); ?>