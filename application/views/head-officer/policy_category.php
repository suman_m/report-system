<?php $this->load->view("head-officer/header"); ?>
<script src="<?php echo site_url('assets/js/jquery.validate.min.js'); ?>"></script>
<script>
    $(document).ready(function() {
        $("#addCategoryForm").validate();
    });
</script>
<div class="page-wrapper">
    <div class="container">
        <div class="col-sm-12 main marginLeft0 listWrap">
            <button type="button" class="btn btn-primary addCategoryBtn addBtn">Add Category</button>
            <h3 class="page-header">Policy Categories</h3>
            <?php if (isset($success) && $success): ?>
                <div class="alert alert-<?php echo $successType ?> margintop10" role="alert"><?php echo $successMsg; ?></div>
            <?php endif; ?>
            <div class="table-responsive">
                <?php $count = 0; ?>
                <?php if (count($categories) > 0): ?>
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>S. N.</th>
                                <th>Region</th>
                                <th>Options</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($categories as $reg): $count++; ?>
                                <tr>
                                    <td><?php echo $count; ?></td>
                                    <td class="regionName_<?php echo $reg->id; ?>"><?php echo $reg->name; ?></td>
                                    <td>
                                        <a class="btn btn-sm btn-primary editCategory" data="<?php echo $reg->id; ?>" href="<?php echo site_url("head-officer/policy/update_category?id=$reg->id"); ?>">Edit</a> 
                                        <a class="btn btn-sm btn-danger" href="<?php echo site_url("head-officer/policy/delete_category?id=$reg->id"); ?>">Delete</a>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                <?php endif; ?>
            </div>

        </div>

        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main marginLeft0 addFormWrap" style="display: none;">
            <a class="close"></a>
            <h3>Add Category</h3>
            <form id="addCategoryForm" class="small-forms" action="<?php echo site_url(); ?>head-officer/policy/save_category" method="post">
                <input type="hidden" id="editid" name="editid" value=""/>
                <label for="username">Category Name:</label>
                <input type="text" class="form-control required" name="categoryname" id="categoryname" value="">
                <button class="btn btn-primary btn-block margintop10" type="submit">Save</button>
            </form>
        </div>
        <div class="overlay" style="display: none;"></div>
    </div>
</div>
<?php $this->load->view("footer"); ?>