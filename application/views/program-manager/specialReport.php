<?php $this->load->view("program-manager/header"); ?>
<div class="page-wrapper">
    <div class="container">
        <h3 class="page-header">Special Reports</h3>
        <table class="table table-striped ">
            <tr>
                <th>SN</th>
                <th>Report Title</th>
                <th>Submitted By</th>
                <th>Report List</th>
                <th>Special Notes</th>
                <th>Forms and Format</th>
                <th>Submission Date</th>
            </tr>
            <?php
            $i = 1;
            foreach ($specialReport as $sr):
                ?>
                <tr>
                    <td><?php echo $i; ?></td>
                    <td><?php echo $sr->title; ?></td>
                    <td><?php echo $this->misc_lib->getUserName($sr->user_id); ?></td>
                    <td><?php echo $sr->files; ?> <p><a class="btn btn-success" href="<?php echo site_url('head-officer/specialReport/downloadReport/' . $sr->id); ?>">Download</a></p></td>
                    <td><?php echo $sr->special_notes; ?></td>
                    <td><?php echo $this->common_model->getTableData('forms_format',array('id'=>$sr->form_id))->row()->form_format_name; ?></td>
                    <td><?php echo $sr->date; ?></td>
                </tr>
                <?php
                $i++;
            endforeach;
            ?>
        </table>

        <div id="reports-logs">
            <h3>Special Report Logs</h3>
            <table class="table table-striped ">
                <thead>
                    <tr>
                        <td>SN</td>
                        <td>Logs Message</td>
                        <td>Date</td>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $i = 1;
                    foreach ($logs as $l):
                        ?>

                        <?php $userData = $this->misc_lib->getUserName($l->user_id); ?>
                        <tr>
                            <td><?php echo $i; ?></td>
                            <td><?php echo $l->logs . ' by ' . $userData; ?></td>
                            <td><?php echo $l->date; ?></td>
                        </tr>
                        <?php
                        $i++;
                    endforeach;
                    ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
<?php $this->load->view("footer"); ?>