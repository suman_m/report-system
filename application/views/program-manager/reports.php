<?php $this->load->view("program-manager/header"); ?>
<div class="page-wrapper">
    <div class="container">
        <?php echo $this->session->flashdata('success'); ?>
        <?php echo $this->session->flashdata('month'); ?>
        <h3 class="page-header">Manage Reports</h3>
        <table class="table table-striped ">
            <tr>
                <th>SN</th>
                <th>Report Title</th>
                <th>Submitted By</th>
                <th>Report List</th>
                <th>Special Notes</th>
                <th>Submission Date</th>
            </tr>
            <?php
            $i = 1;
            foreach ($reports as $re):
                ?>
                <?php
                $files = json_decode($re->files);

                $key_array = array();
                foreach ($files as $key => $val):
                    $key_array[] = $key;
                endforeach;
                ?>
                <tr>
                    <td><?php echo $i; ?></td>
                    <td><?php echo $re->title; ?></td>
                    <td><?php echo $this->misc_lib->getUserName($re->user_id); ?></td>
                    <td>
                        <?php foreach ($reportType as $v): $id = $v->id; ?>
                            <?php if (in_array($v->id, $key_array)): ?>
                                <span>
                                    <p><?php echo $v->title; ?></p>
                                    <p><?php echo $files->$id; ?><a class="btn btn-success" href="<?php echo site_url('program-manager/reports/downloadReport/' . $re->id . '/' . $v->id); ?>">Download Report</a></p>

                                </span>
                            <?php endif; ?>
                        <?php endforeach; ?>
                    </td>
                    <td><?php echo $re->special_notes; ?></td>
                    <td><?php echo $re->date; ?></td>

                </tr>
                <?php
                $i++;
            endforeach;
            ?>
        </table>

        <div id="reports-logs">
            <h3>Reports Logs</h3>
            <table class="table table-striped ">
                <tr>
                    <th>SN</th>
                    <th>Logs Message</th>
                    <th>Date</th>
                </tr>
                <?php
                $i = 1;
                foreach ($reportLogs as $r_l):
                    ?>
                    <tr>
                        <td><?php echo $i; ?></td>
                        <td>
                            <?php $user_data = $this->misc_lib->getUserData($r_l->user_id); ?>
                            <?php echo $this->misc_lib->logsMessage($r_l->logs) . ' by ' . $user_data->name . '(' . $user_data->accessLevel . ')'; ?>
                        </td>
                        <td><?php echo $r_l->date; ?></td>
                    </tr>
                    <?php
                    $i++;
                endforeach;
                ?>
            </table>
        </div>
    </div>
</div>
<?php $this->load->view("footer"); ?>