<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 7/20/14
 * Time: 9:39 AM
 */
$this->load->view("program-manager/header");
?>
<div class="page-wrapper">
    <div class="container">
        <h3 class="page-header">Forms and Formats</h3>
        <div id ="ava" class="alert alert-success" role="alert" style="display: none;"> <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>New Forms And Format Available</div>
        <div id ="message"class="alert alert-info" role="alert" style="display: none;"> <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>No Forms And Format Available</div>
        <div class="table-responsive">
            <table class="table table-striped">
                <?php if ($form != null) { ?>
                    <tr>
                        <td>SN</td>
                        <th>Form Name</th>
                        <th>Form Description</th>
                        <th>Actual File</th>
                        <th>Valid Date</th>
                        <th>Download</th>
                        <th>&nbsp;</th>
                    </tr>
                    <tbody>
                        <?php
                        $i = 1;
                        $curr_date = date('Y-m-d');

                        foreach ($form as $res):
                            if ($res->valid_date < $curr_date && $res->valid_date != '0000-00-00') {
                                $disable = 'disabled="disabled"';
                            } else {
                                $disable = '';
                            }
                            ?>
                            <tr>
                                <td><?php echo $i; ?></td>
                                <td><?php echo $res->form_format_name; ?></td>
                                <td><?php echo $res->description; ?> </td>
                                <td><?php echo $res->actual_form; ?></td>
                                <td><?php
                                    if ($res->valid_date != '0000-00-00') {
                                        echo $res->valid_date;
                                    } else {
                                        echo "Available";
                                    }
                                    ?></td>
                                <td><form  action="" method="post">
                                        <input type="hidden" name="id" value="<?php echo $res->ID; ?>">
                                        <input type="hidden" name="form_id" value="<?php echo $res->formid; ?>">
                                        <input type="hidden" name="act" value="<?php echo $res->actual_form; ?>">
                                        <input type="hidden" name="reg_id" value="<?php echo $res->region; ?>">
                                        <input type="hidden" name="up_date" value="<?php echo $res->date ?>">
                                        <input type="hidden" name="act_name" value="<?php echo $res->form_format_name; ?>">
                                        <input type="hidden" name="uname" value="<?php echo $res->username; ?>">
                                        <input type="hidden" name="region" value="<?php echo $res->region_name; ?>">
                                        <input type="hidden" name="access_level" value="<?php echo $res->accessLevel; ?>">
                                        <input type="submit" name="submitBtn" value="Download" class="btn btn-success" <?php echo $disable; ?>>
                                    </form>
                                </td>
                                <td><a href="<?php echo site_url('program-manager/forms/history/' . $res->id); ?>" class="btn btn-info">View History</a> </td>
                            </tr>
                            <?php
                            $i++;
                        endforeach;
                        ?>
                    </tbody>
                <?php } else { ?>
                    <tr>
                        <td>No  Record Found;</td>
                    </tr>
                <?php } ?>
            </table>

        </div>
    </div>
    <?php $this->load->view("footer"); ?>
    <script type="text/javascript">

        setInterval(function() {
            $.ajax({
                type: "POST",
                url: "<?php echo site_url('project-holder/forms/count'); ?>",
                success: function(response) {
                    if (response == 1)
                    {
                        $('#ava').show();
                        $('#message').hide();

                    }
                    else
                    {
                        $('#message').show();
                        $('#ava').hide();
                    }

                }
            });
        }, 10000);

    </script>