<?php $this->load->view('program-manager/header'); ?>
<div class="page-wrapper">
    <div class="container">
        <h3 class="page-header">Manage Backup</h3>
        <table class="table table-striped ">
            <tr>
                <th>SN</th>
                <th>Backup Title</th>
                <th>Backup List</th>
                <th>Special Notes</th>
                <th>Submission Date</th>
                <th>Status</th>
            </tr>
            <tbody>
                <?php
                $i = 1;
                foreach ($backupData as $bd):
                    ?>
                    <?php $backup = json_decode($value->content); ?>
                    <?php $file = json_decode($bd->files); ?>
                    <tr>
                        <td><?php echo $i; ?></td>
                        <td><?php echo $bd->title; ?></td>
                        <td>
                            <?php foreach ($backup as $k => $bk): ?>
                                <?php foreach ($file as $ke => $fi): ?>
                                    <?php if ($k == $ke): ?>
                                        <?php echo "<p>" . $bk . "</p>"; ?>
                                        <?php echo "<p>" . $fi . "</p>"; ?>
                                        <p><a class="btn btn-success" href="<?php echo site_url('head-officer/backup/downloadBackup/' . $bd->id . '/' . $k); ?>">Download</a></p>
                                    <?php endif; ?>
                                <?php endforeach; ?>
                            <?php endforeach; ?>
                        </td>
                        <td><?php echo $bd->special_notes; ?></td>
                        <td><?php echo $bd->date; ?></td>
                        <td id="backup-status">
                            <?php
                            if ($bd->status == 1)
                                echo "Accepted";
                            else if ($bd->status == 2)
                                echo "Rejected";
                            else {
                                ?>
                                <input type="radio" name="status-<?php echo $bd->id; ?>" value="1" class="status" onclick="backupAccept('<?php echo $bd->id; ?>');" /> Accepted
                                <input type="radio" name="status-<?php echo $bd->id; ?>" value="2" class="status" onclick="backupReject('<?php echo $bd->id; ?>');" /> Rejected
                                <?php
                            }
                            ?>
                        </td>
                    </tr>
                    <?php
                    $i++;
                endforeach;
                ?>
            </tbody>

        </table>

        <div id="backup-logs">
            <h3>Backup Logs</h3>
            <table class="table table-striped ">
                <tr>
                    <td>SN</td>
                    <td>Logs Message</td>
                    <td>Date</td>
                </tr>
                <?php
                $i = 1;
                foreach ($backupLogs as $bl):
                    ?>
                    <tr>
                        <td><?php echo $i; ?></td>
                        <td>
                            <?php $user_data = $this->misc_lib->getUserData($bl->user_id); ?>
                            <?php echo $bl->logs . ' by ' . $user_data->name . '(' . $bl->access_level . ')'; ?></td>
                        <td><?php echo $bl->date; ?></td>
                    </tr>
                    <?php
                    $i++;
                endforeach;
                ?>

            </table>

        </div>

    </div>
</div>
<?php $this->load->view("footer"); ?>