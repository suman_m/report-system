<?php $this->load->view('program-manager/header'); ?>
<div class="page-wrapper">
    <div class="container">
        <h3 class="page-header">Other Documents</h3>
        <div class="table-responsive">

            <?php if (!empty($content)): ?>
                <table class="table table-striped">
                    <tr>
                        <th>SN</th>
                        <th>Title</th>
                        <th>Description</th>
                        <th>Valid Date</th>
                        <th>Options</th>
                    </tr>
                    <tbody>
                        <?php
                        $i = 1;
                        foreach ($content as $con):
                            ?>
                            <tr>
                                <td><?php echo $i; ?></td>
                                <td><?php echo $con->others_title; ?></td>
                                <td><?php echo $con->others_description; ?></td>
                                <td><?php echo $con->valid_date; ?></td>
                                <td>
                                    <?php
                                    $currentDate = date('Y-m-d');
                                    if (strtotime($currentDate) > strtotime($con->valid_date)):
                                        echo "Date Expired";
                                    else:
                                        ?>
                                        <a href="<?php echo site_url('program-manager/others/download_other/' . $con->other_id); ?>" class="btn btn-success">Download</a>
                                    <?php endif; ?>
                                </td>
                            </tr>
                            <?php
                            $i++;
                        endforeach;
                        ?>
                    </tbody>
                </table>
                <?php
            else:
                echo "No Data Available";
            endif;
            ?>
        </div>
    </div>
</div>
<?php $this->load->view('footer'); ?>