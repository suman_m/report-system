<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Login</title>
        <link rel="stylesheet" href="<?php echo site_url("assets/css/bootstrap.min.css"); ?>">
        <link rel="stylesheet" href="<?php echo site_url("assets/css/bootstrap-theme.min.css"); ?>">
        <link rel="stylesheet" href="<?php echo site_url("assets/css/style.css"); ?>">
        <script type='text/javascript' src='<?php echo site_url("assets/js/jquery.min.js"); ?>'></script>
        <script type='text/javascript' src='<?php echo site_url("assets/js/bootstrap.min.js"); ?>'></script>
    </head>
    <body>
        <div class="page-wrapper">
            <div class="container">

                <form class="form-signin" action="<?php echo site_url(); ?>loginpage/login_check" role="form" method="post">
                    <h2 class="form-signin-heading">Please sign in</h2>
                    <input type="email" class="form-control" placeholder="Email address" required="" autofocus="" name="logEmail">
                    <input type="password" class="form-control" placeholder="Password" required="" name="logPass">
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" value="remember-me"> Remember me
                        </label>
                    </div>
                    <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
                    <?php if ($this->session->userdata("logging_error")): ?>
                        <div class="alert alert-danger margintop10" role="alert"><?php echo $this->session->userdata("alertMsg"); ?></div>
                        <?php $this->session->set_userdata("logging_error", false); ?>
                    <?php endif; ?>
                </form>

            </div>
        </div>
    </body>
</html>