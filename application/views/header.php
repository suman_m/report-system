<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title></title>
    <link rel="stylesheet" href="<?php echo site_url("assets/css/bootstrap.min.css"); ?>">
    <link rel="stylesheet" href="<?php echo site_url("assets/css/bootstrap-theme.min.css"); ?>">
    <link rel="stylesheet" href="<?php echo site_url("assets/css/style.css"); ?>">
    <script type='text/javascript' src='<?php echo site_url("assets/js/jquery.min.js"); ?>'></script>
    <script type='text/javascript' src='<?php echo site_url("assets/js/bootstrap.min.js"); ?>'></script>
    <script type='text/javascript' src='<?php echo site_url("assets/js/main.js"); ?>'></script>
</head>
<body>
<input type="hidden" id="siteURL" value="<?php echo site_url(); ?>"/>