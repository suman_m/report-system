<?php $this->load->view("header"); ?>
<script src="<?php echo site_url('assets/js/jquery.validate.min.js'); ?>"></script>
<script src="<?php echo site_url('assets/js/additional-methods.min.js'); ?>"></script>
<script>
    $(document).ready(function() {
        $("#u ploadReportForm").validate();
    });
</script>
<div class="page-wrapper">
    <div class="container">
        <h2 class="form-signin-heading"><?php echo $title; ?></h2>
        <form action="" method="post" enctype="multipart/form-data" id="uploadReportForm">
            <div class="form-group">
                <label>Report Title</label><input type="text" name="reportTitle" class="form-control required" value="<?php echo $value->title; ?>" />
            </div>
            <h2>Upload Reports</h2>
            <?php //$report = json_decode($reportType->report_category); ?>
            <?php $report_value = json_decode($value->files); ?>
            <?php
            $key_array = array();
            foreach ($report_value as $key => $val):
                $key_array[] = $key;
            endforeach;
            ?>
            <?php foreach ($reportType as $re): $i = $re->id; ?>
                <?php $reportStatus = $this->misc_lib->getReportStatus($value->id, $re->id); ?>
                <div class="form-group">
                    <label><?php echo $re->title; ?></label><?php if ($reportStatus == 0) { ?> <input type="file" name="<?php echo $this->misc_lib->slugify($re->title) . '-' . $re->id; ?>" class="form-control" /><?php } else echo "<br /> Report Accepted by Head Office."; ?>
                    <input type="hidden" name="<?php echo $this->misc_lib->slugify($re->title) . '-' . $re->id . '-old'; ?>" value="<?php echo (in_array($re->id, $key_array)) ? $report_value->$i : ''; ?>" />
                </div>
            <?php endforeach; ?>
            <div class="form-group">
                <label>Report For the month : </label>
                <select name="month" class="required form-control">
                    <option value="">Choose the Month</option>
                    <?php for ($i = 1; $i <= 12; $i++) { ?>
                        <option value="<?php echo $i; ?>" <?php echo ($value->month == $i) ? 'selected="selected"' : ''; ?>><?php echo date('F', strtotime('01-' . $i . '-2001')); ?></option>
                    <?php } ?>
                </select>
            </div>
            <div class="form-group">
                <label>Report of the Project : </label>
                <select name="project" class="required form-control">
                    <option value="">Choose Project</option>
                    <?php foreach ($projects as $pro): ?>
                        <option value="<?php echo $pro->project_id; ?>" <?php echo ($value->project_id == $pro->project_id) ? 'selected="selected"' : ''; ?>><?php echo $this->misc_lib->getProjectName($pro->project_id); ?></option>
                    <?php endforeach; ?>
                </select>
            </div>
            <div class="form-goup">
                <label>Special Notes : </label><textarea name="special_notes" class="form-control"><?php echo $value->special_notes; ?></textarea>
            </div>
            <br />
            <button class="btn btn-lg btn-primary btn-block" name="submit" value="submit" type="submit">Save</button>
        </form>
    </div>
</div>
<?php $this->load->view("footer"); ?>