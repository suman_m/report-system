<?php $this->load->view('project-holder/header'); ?>
<div class="page-wrapper">
    <div class="container">
        <h3 class="page-header">Backups</h3>
        <p>
            <a class="btn btn-primary addRegionBtn" href="<?php echo site_url('project-holder/backup/submitBackup'); ?>">Submit Backup Report</a>
        </p>
        <table class="table table-striped ">
            <tr>
                <th>SN</th>
                <th>Backup Title</th>
                <th>Backup List List</th>
                <th>Backup for the month of</th>
                <th>Special Notes</th>
                <th>Submission Date</th>
                <th>Status</th>
            </tr>
            <tbody>
                <?php
                $i = 1;
                foreach ($backupData as $bt):
                    ?>
                    <?php $backup = json_decode($backupType->content); ?>
                    <?php $file = json_decode($bt->files); ?>
                    <tr>
                        <td><?php echo $i; ?></td>
                        <td><?php echo $bt->title; ?></td>
                        <td>
                            <?php foreach ($backup as $k => $bk): ?>
                                <?php foreach ($file as $ke => $fi): ?>
                                    <?php if ($k == $ke): ?>
                                        <?php echo "<p>" . $bk . "</p>"; ?>
                                        <?php echo "<p>" . $fi . "</p>"; ?>
                                    <?php endif; ?>
                                <?php endforeach; ?>
                            <?php endforeach; ?>
                        </td>
                        <td><?php echo date('F', strtotime('01-' . $bt->month . '-' . date('Y'))); ?></td>
                        <td><?php echo $bt->special_notes; ?></td>
                        <td><?php echo $bt->date; ?></td>
                        <td>
                            <?php
                            if ($bt->status == 1) {
                                echo "Accepted";
                            } else if ($bt->status == 2) {
                                echo "Rejected";
                            } else
                                echo "Pending";
                            if ($bt->status == 2 || $bt->status == 0) {
                                ?>
                                <a class="btn btn-sm btn-primary" href="<?php echo site_url('project-holder/backup/editBackup/' . $bt->id); ?>">Edit</a>
                            <?php }
                            ?>
                        </td>
                    </tr>
                    <?php
                    $i++;
                endforeach;
                ?>
            </tbody>
        </table>

        <div id="backup-logs">
            <h3>Backup Logs</h3>
            <table class="table table-striped ">
                <thead>
                    <tr>
                        <th>SN</th>
                        <th>Logs Message</th>
                        <th>Date</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $i = 1;
                    foreach ($backupLogs as $bl):
                        ?>
                        <tr>
                            <td><?php echo $i; ?></td>
                            <td>
                                <?php
                                if ($this->session->userdata('logedin_id') == $bl->user_id) {
                                    $userName = '';
                                } else {
                                    $user_data = $this->misc_lib->getUserData($bl->user_id);
                                    $userName = ' by ' . $user_data->name . '(' . $bl->access_level . ')';
                                }
                                ?>
                                <?php echo $bl->logs . $userName; ?>
                            </td>
                            <td><?php echo $bl->date; ?></td>
                        </tr>
                        <?php
                        $i++;
                    endforeach;
                    ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
<?php $this->load->view('footer'); ?>