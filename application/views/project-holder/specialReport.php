<?php $this->load->view('project-holder/header'); ?>
<script src="<?php echo site_url('assets/js/jquery.validate.min.js'); ?>"></script>
<script src="<?php echo site_url('assets/js/additional-methods.min.js'); ?>"></script>
<script>
    $(document).ready(function() {
        $("#myform").validate({
            rules: {
                specialReport: {
                    required: true,
                    extension: "xls|csv|doc|docx|xlsx|pdf"
                }
            }
        });
    });
</script>
<div class="page-wrapper">
    <div class="container">
        <h3 class="form-signin-heading"><?php echo $title; ?></h3>
        <p>
            <a class="btn btn-primary addRegionBtn" href="<?php echo site_url('project-holder/specialReport/submitReport'); ?>">Submit Special Report</a>
        </p>
        <?php if (!empty($specialData)) {
            ?>
            <table class="table table-striped ">
                <tr>
                    <th>SN</th>
                    <th>Report Title</th>
                    <th>Report Lists</th>
                    <th>Submission Date</th>
                </tr>
                <tbody>
                    <?php
                    $i = 1;
                    foreach ($specialData as $sd):
                        ?>
                        <tr>
                            <td><?php echo $i; ?></td>
                            <td><?php echo $sd->title; ?></td>
                            <td><?php echo $sd->files; ?></td>
                            <td><?php echo $sd->date; ?></td>
                        </tr>
                        <?php
                        $i++;
                    endforeach;
                    ?>
                </tbody>
            </table>
            <?php
        }
        else {
            echo "You have not submitted any special reports.";
        }
        ?>

        <div id="reports-logs">
            <h3>Special Report Logs</h3>
            <table class="table table-striped ">
                <thead>
                    <tr>
                        <td>SN</td>
                        <td>Logs Message</td>
                        <td>Date</td>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $i = 1;
                    foreach ($logs as $l):
                        ?>

                        <?php $userData = $this->misc_lib->getUserName($l->user_id); ?>
                        <tr>
                            <td><?php echo $i; ?></td>
                            <td><?php echo $l->logs . ' by ' . $userData; ?></td>
                            <td><?php echo $l->date; ?></td>
                        </tr>
                        <?php
                        $i++;
                    endforeach;
                    ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
<?php $this->load->view("footer"); ?>