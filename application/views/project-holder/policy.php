<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 7/16/14
 * Time: 4:37 PM
 */
$this->load->view("project-holder/header");
?>
<div class="page-wrapper">
    <div class="container">
        <h3 class="page-header">Policies</h3>
        <div id ="ava" class="alert alert-success" role="alert" style="display: none;"> <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>New Policy Available</div>
        <div id ="message"class="alert alert-info" role="alert" style="display: none;"> <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>No Policy Available</div>
        <div class="table-responsive">
            <table class="table table-striped">
                <?php if ($policy != null) { ?>
                    <tr>
                        <th>SN</th>
                        <th>Policy Name</th>
                        <th>Policy Description</th>
                        <th>Actual File</th>
                        <th>Valid Date</th>
                        <th>Download</th>
                    </tr>
                    <tbody>
                        <?php
                        $curr_date = date('Y-m-d');
                        $i = 1;
                        foreach ($policy as $res):
                            if ($res->valid_date < $curr_date && $res->valid_date != '0000-00-00') {
                                $disable = 'disabled="disabled"';
                            } else {
                                $disable = '';
                            }
                            ?>
                            <tr>
                                <td><?php echo $i; ?></td>
                                <td><?php echo $res->policy_name; ?></td>
                                <td><?php echo $res->description; ?> </td>
                                <td><?php echo $res->actual_policy; ?></td>
                                <td><?php
                                    if ($res->valid_date != '0000-00-00') {
                                        echo $res->valid_date;
                                    } else {
                                        echo "Available";
                                    }
                                    ?></td>
                                <td><form  action="" method="post">
                                        <input type="hidden" name="id" value="<?php echo $res->ID; ?>">
                                        <input type="hidden" name="form_id" value="<?php echo $res->policy_id; ?>">
                                        <input type="hidden" name="up_date" value="<?php echo $res->date; ?>">
                                        <input type="hidden" name="act" value="<?php echo $res->actual_policy; ?>">
                                        <input type="hidden" name="act_name" value="<?php echo $res->policy_name; ?>">
                                        <input type="hidden" name="reg_id" value="<?php echo $res->region; ?>">
                                        <input type="hidden" name="uname" value="<?php echo $res->username; ?>">
                                        <input type="hidden" name="region" value="<?php echo $res->region_name; ?>">
                                        <input type="hidden" name="access_level" value="<?php echo $res->accessLevel; ?>">
                                        <input type="submit" name="submitBtn" value="Download" class="btn btn-success" <?php echo $disable; ?>>
                                    </form>
                                </td>
                            </tr>
                            <?php
                            $i++;
                        endforeach;
                        ?>
                    </tbody>
                <?php } else { ?>
                    <tr>
                        <td>NO  Record Found;</td>
                    </tr>
                <?php } ?>
            </table>

        </div>
    </div>
    <script type="text/javascript">

        setInterval(function() {
            $.ajax({
                type: "POST",
                url: "<?php echo site_url('project-holder/policy/count'); ?>",
                success: function(response) {
                    if (response == 1)
                    {
                        $('#ava').show();
                        $('#message').hide();

                    }
                    else
                    {
                        $('#message').show();
                        $('#ava').hide();
                    }

                }
            });
        }, 10000);

    </script>
    <?php $this->load->view("footer"); ?>
    