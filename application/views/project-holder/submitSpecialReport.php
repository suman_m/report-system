<?php $this->load->view('project-holder/header'); ?>
<script src="<?php echo site_url('assets/js/jquery.validate.min.js'); ?>"></script>
<script src="<?php echo site_url('assets/js/additional-methods.min.js'); ?>"></script>
<script>
    $(document).ready(function() {
        $("#myform").validate({
            rules: {
                specialReport: {
                    required: true,
                    extension: "xls|csv|doc|docx|xlsx|pdf"
                }
            }
        });
    });
</script>
<div class="page-wrapper">
    <div class="container">
        <h3 class="form-signin-heading"><?php echo $title; ?></h3>
        <p><?php  echo $this->session->flashdata('error'); ?></p>
        <form action="" method="post" enctype="multipart/form-data" id="myform">
            <div class="form-group">
                <label>Special Report Title</label><input type="text" name="reportTitle" class="form-control required"  />
            </div>
            <div>
                <label>Special Report : </label>
                <select name="form_id" id="form_id" class="required form-control">
                    <option value="">Choose the available format to upload the report</option>
                    <?php foreach($specialReport as $sr): ?>
                        <option value="<?php echo $sr->id; ?>"><?php echo $sr->form_format_name; ?></option>
                    <?php endforeach; ?>
                </select>
            </div>

            <div class="form-group">
                <label>Upload Special Report</label><input type="file" name="specialReport" id="specialReport" class="form-control" />
            </div>

            <div class="form-goup">
                <label>Special Notes : </label><textarea name="special_notes" class="form-control"></textarea>
            </div>
            <br />
            <button class="btn btn-lg btn-primary btn-block" name="submit" value="submit" type="submit">Save</button>
        </form>
    </div>
</div>
<?php $this->load->view("footer"); ?>