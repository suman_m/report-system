<?php $this->load->view("project-holder/header"); ?>
<script src="<?php echo site_url('assets/js/jquery.validate.min.js'); ?>"></script>
<script src="<?php echo site_url('assets/js/additional-methods.min.js'); ?>"></script>
<script>
    $(document).ready(function() {
        $("#uploadReportForm").validate();
    });
</script>
<div class="page-wrapper">
    <div class="container">
        <h3 class="form-signin-heading"><?php echo $title; ?></h3>
        <?php if (!empty($projects)): ?>
            <form action="" method="post" enctype="multipart/form-data" id="uploadReportForm">

                <div class="form-group">
                    <label>Report Title</label><input type="text" name="reportTitle" class="form-control required" />
                </div>
                <h3>Upload Reports</h3>
                <?php $i = 1; ?>
                <?php foreach ($reportType as $re): ?>

                    <div class="form-group">
                        <label><?php echo $re->title; ?></label><input type="file" name="<?php echo $this->misc_lib->slugify($re->title) . '-' . $re->id; ?>" class="form-control"  />
                    </div>
                    <?php
                    $i++;
                endforeach;
                ?>
                <div class="form-group">
                    <label>Report For the month : </label>
                    <select name="month" class="required form-control">
                        <option value="">Choose the Month</option>
                        <?php for ($i = 1; $i <= 12; $i++) { ?>
                            <option value="<?php echo $i; ?>"><?php echo date('F', strtotime('01-' . $i . '-2001')); ?></option>
                        <?php } ?>
                    </select>
                </div>
                <div class="form-group">
                    <label>Report of the Project : </label>
                    <select name="project" class="required form-control">
                        <option value="">Choose Project</option>
                        <?php foreach ($projects as $pro): ?>
                            <option value="<?php echo $pro->project_id; ?>"><?php echo $this->misc_lib->getProjectName($pro->project_id); ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
                <div class="form-goup">
                    <label>Special Notes : </label><textarea name="special_notes" class="form-control"></textarea>
                </div>
                <br />
                <button class="btn btn-lg btn-primary btn-block" name="submit" value="submit" type="submit">Save</button>
            </form>
            <?php
        else:
            echo "You have not been assigned any projects.";
        endif;
        ?>
    </div>
</div>
<?php $this->load->view("footer"); ?>