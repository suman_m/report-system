<?php $this->load->view("project-holder/header"); ?>
<div class="page-wrapper">
    <div class="container">
        <h3 class="page-header">Reports</h3>
        <?php if ($reportNotification != ''): ?>
            <div class="alert fade in">You have to send the reports by <?php echo $reportNotification; ?>.</div>
        <?php endif; ?>
        <div>
            <a class="btn btn-primary addRegionBtn" href="<?php echo site_url('project-holder/reports/submitReport'); ?>">Submit Report</a>
        </div>
        <p><?php echo $this->session->flashdata('success'); ?></p>
        <p><?php echo $this->session->flashdata('error'); ?></p>
        <?php if (!empty($reports)) { ?>
            <table class="table table-striped ">
                <tr>
                    <th>SN</th>
                    <th>Report Title</th>
                    <th>Report Lists</th>
                    <th>Report For the month of</th>
                    <th>Project Name</th>
                    <th>Submission Date</th>
                    <th>Options</th>
                </tr>
                <?php
                $i = 1;
                foreach ($reports as $rep):
                    ?>
                    <?php
                    $files = json_decode($rep->files);

                    $key_array = array();
                    foreach ($files as $key => $val):
                        $key_array[] = $key;
                    endforeach;
                    ?>
                    <tr>
                        <td><?php echo $i; ?></td>
                        <td><?php echo $rep->title; ?></td>
                        <td>
                            <?php foreach ($reportType as $v): $id = $v->id; ?>
                                <?php if (in_array($v->id, $key_array)): ?>
                                    <?php
                                    if (!empty($reportStatus)) {
                                        foreach ($reportStatus as $r_s):
                                            if ($r_s->report_id == $rep->id && $r_s->report_type_id == $v->id) {
                                                if ($r_s->status == 1) {
                                                    $status = "Accepted";
                                                } else if ($r_s->status == 2) {
                                                    $status = "Rejected";
                                                }
                                                break;
                                            } else {
                                                $status = "Pending";
                                            }
                                        endforeach;
                                    } else {
                                        $status = "Pending";
                                    }
                                    ?>
                                    <span>
                                        <p><?php echo $v->title; ?></p>
                                        <p><?php echo $files->$id; ?></p>
                                        <p><b><?php echo $status; ?></b></p>

                                    </span>
                                <?php endif; ?>
                            <?php endforeach; ?>
                        </td>
                        <td><?php echo date('F', strtotime('01-' . $rep->month . '-' . date('Y'))); ?></td>
                        <td><?php echo $this->misc_lib->getProjectName($rep->project_id); ?></td>
                        <td><?php echo $rep->date; ?></td>
                        <td>
                            <?php
                            $check_edit_status = $this->misc_lib->getReportEditStatus($rep->id);
                            if ($check_edit_status == 0) {
                                ?>
                                <a class="btn btn-sm btn-primary" href="<?php echo site_url('project-holder/reports/editReport/' . $rep->id); ?>">Edit</a>
                                <?php
                            }
                            ?>
                        </td>
                    </tr>
                    <?php
                    $i++;
                endforeach;
                ?>
            </table>
            <?php
        } else {
            echo "No report Submitted yet.";
        }
        ?>

        <div id="reports-logs">
            <h3>Reports Logs</h3>
            <table class="table table-striped ">
                <tr>
                    <th>SN</th>
                    <th>Logs Message</th>
                    <th>Date</th>
                </tr>
                <?php
                $i = 1;
                foreach ($reportLogs as $r_l):
                    ?>
                    <tr>
                        <td><?php echo $i; ?></td>
                        <td>
                            <?php $user_data = $this->misc_lib->getUserData($r_l->user_id); ?>
                            <?php
                            echo $this->misc_lib->logsMessage($r_l->logs);
                            if ($r_l->logs == "REPORT ACCEPTED" || $r_l->logs == "REPORT DOWLOADED" || $r_l->logs == "REPORT REJECTED")
                                echo ' by ' . $user_data->name . '(' . $user_data->accessLevel . ')';
                            ?>
                        </td>
                        <td><?php echo $r_l->date; ?></td>
                    </tr>
                    <?php
                    $i++;
                endforeach;
                ?>
            </table>
        </div>
    </div>
</div>
<?php $this->load->view("footer"); ?>