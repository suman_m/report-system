<?php $this->load->view('project-holder/header'); ?>
<script src="<?php echo site_url('assets/js/jquery.validate.min.js'); ?>"></script>
<script src="<?php echo site_url('assets/js/additional-methods.min.js'); ?>"></script>
<script>
    $(document).ready(function() {
        $("#myform").validate({
            rules: {
                first: {
                    required: true,
                    extension: "xls|csv|doc|docx|xlsx|pdf"
                },
                second: {
                    required: true,
                    extension: "xls|csv|doc|docx|xlsx|pdf"
                }
            }
        });
    });
</script>
<div class="page-wrapper">
    <div class="container">
        <h3 class="form-signin-heading"><?php echo $title; ?></h3>
        <form action="" method="post" enctype="multipart/form-data" id="myform">
            <div class="form-group">
                <label>Backup Title</label><input type="text" name="backupTitle" class="form-control required" value="<?php echo $value->title; ?>"  />
            </div>
            <h2>Upload Backups</h2>
            <?php $type = json_decode($backupType->content); ?>
            <div class="form-group">
                <label><?php echo $type->first; ?></label><input type="file" name="first" id="first" class="form-control" />
            </div>
            <div class="form-group">
                <label><?php echo $type->second; ?></label><input type="file" name="second" id="second" class="form-control" />
            </div>

            <div class="form-group">
                <label>Backup For the month : </label>
                <select name="month" class="required form-control">
                    <option value="">Choose the Month</option>
                    <?php for ($i = 1; $i <= 12; $i++) { ?>
                        <option value="<?php echo $i; ?>" <?php echo ($value->month == $i) ? 'selected="selected"' : ''; ?>><?php echo date('F', strtotime('01-' . $i . '-2001')); ?></option>
                    <?php } ?>
                </select>
            </div>

            <div class="form-goup">
                <label>Special Notes : </label><textarea name="special_notes" class="form-control"><?php echo $value->special_notes; ?></textarea>
            </div>
            <br />
            <button class="btn btn-lg btn-primary btn-block" name="submit" value="submit" type="submit">Save</button>
        </form>
    </div>
</div>
<?php $this->load->view("footer"); ?>