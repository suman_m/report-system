<?php
if ($this->session->userdata("access_level") == "Head Officer")
    $this->load->view("head-officer/header");
if ($this->session->userdata("access_level") == "Program Manager")
    $this->load->view("program-manager/header");
if ($this->session->userdata("access_level") == "Program Officer")
    $this->load->view("program-officer/header");
if ($this->session->userdata("access_level") == "Project Holder")
    $this->load->view("project-holder/header");
?>
<script src="<?php echo site_url('assets/js/jquery.validate.min.js'); ?>"></script>
<script>
    $(document).ready(function() {
        $("#changePassword").validate();
    });
</script>
<div class="page-wrapper">
    <div class="container">
        <?php if(isset($report_notification) && $report_notification !=''): ?>
            <div class="alert alert-danger margintop10" role="alert">
                <p>You need to send the project reports by 5th of <?php echo $report_notification; ?></p>
            </div>
        <?php endif; ?>
        <?php if(isset($backup_notification) && $backup_notification !=''): ?>
            <div class="alert alert-danger margintop10" role="alert">
                <p>You need to send the backup reports by 5th of <?php echo $backup_notification; ?></p>
            </div>
        <?php endif; ?>

        <?php if(isset($report_ends) && $report_ends !=''): ?>
            <div class="alert alert-danger margintop10" role="alert">
                <p>You have not submitted the project  reports for the month of  <?php echo $report_ends; ?></p>
            </div>
        <?php endif; ?>

        <?php if(isset($backup_ends) && $backup_ends !=''): ?>
            <div class="alert alert-danger margintop10" role="alert">
                <p>You have not submitted the backup  reports for the month of  <?php echo $backup_ends; ?></p>
            </div>
        <?php endif; ?>

        <?php if(isset($forms) && $forms =='1'): ?>
            <div class="alert alert-danger margintop10" role="alert">
                <p>New Form and Formats Available.</p>
            </div>
        <?php endif; ?>

        <?php if(isset($policies) && $policies =='1'): ?>
            <div class="alert alert-danger margintop10" role="alert">
                <p>New Policies Available.</p>
            </div>
        <?php endif; ?>

        <?php if ($edit): ?>
            <form class="small-forms" action="<?php echo site_url(); ?>myAccount/savePassword" method="post" id="changePassword">
                <p><label for="password">Old Password:</label> <input type="password" class="form-control required"  autofocus="" name="oldPass"></p>
                <p><label for="newPass">New Password:</label> <input type="password" class="form-control required"  autofocus="" name="newPass" id="newPass"></p>
                <p><label for="confirmPass">Confirm Password:</label> <input type="password" class="form-control required"  autofocus="" name="conPass"></p>
                <button class="btn btn-lg btn-primary btn-block" type="submit">Save</button>
            </form>
        <?php else: ?>
            <?php if (isset($success)) { ?>
                <?php if ($success) { ?>
                    <div class="alert alert-success margintop10" role="alert">Password changed successfully.</div>
                <?php } else { ?>
                    <div class="alert alert-danger margintop10" role="alert">Old password provided is incorrect.</div>
                <?php } ?>
            <?php } ?>
            <p><label for="">Name:</label> <?php echo $data[0]->name; ?></p>
            <p><label for="">Email:</label> <?php echo $data[0]->email; ?></p>
            <?php if ($data[0]->region > 0): ?>
                <p><label for="">Region:</label> <?php echo $regn[0]->name; ?></p>
            <?php endif; ?>
            <p><label for="">Access Level:</label> <?php echo $data[0]->accessLevel; ?></p>
            <a href="<?php echo site_url(); ?>myAccount/changePassword"><button type="button" class="btn btn-default">Change Password</button></a>
        <?php endif; ?>



    </div>
</div>
<?php $this->load->view("footer"); ?>