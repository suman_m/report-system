<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 7/22/14
 * Time: 10:25 AM
 */
$this->load->view("program-officer/header");
?>
<div class="page-wrapper">
    <div class="container">
        <h3 class="page-header">Policies Download History</h3>
        <table class="table table-striped">
            <?php if ($history != NULL): ?>
                <tr>
                    <th>SN</th>
                    <th>Policy Name</th>
                    <th>File</th>
                    <th>Uploaded Date</th>
                    <th>Downloaded Date</th>
                    <th>Downloaded By</th>
                    <th>Downloaded Region</th>
                    <th>Access Level</th>
                    <th>Download From</th>
                </tr>
                <tbody>
                    <?php
                    $i = 1;
                    foreach ($history as $res):
                        ?>
                        <tr>
                            <td><?php echo $i; ?></td>
                            <td><?php echo $res->policy_name; ?></td>
                            <td><?php echo $res->actual_file; ?></td>
                            <td><?php echo $res->uploaded_date; ?></td>
                            <td><?php echo $res->download_date; ?></td>
                            <td><?php echo $res->username; ?></td>
                            <td><?php echo $res->region_name; ?></td>
                            <td><?php echo $res->access_level; ?></td>
                            <td><?php echo $res->ip; ?></td>
                        </tr>
                        <?php
                        $i++;
                    endforeach;
                    ?>
                </tbody>
            <?php endif; ?>
        </table>
    </div>
</div>
  <?php $this->load->view("footer"); ?>