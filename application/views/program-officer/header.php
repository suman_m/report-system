<?php $this->load->view("header"); ?>
<?php $head = $this->common_model->getTableData('site_settings')->row(); ?>
<?php $segment1 = $this->uri->segment(1); ?>
<?php $segment2 = $this->uri->segment(2); ?>
<div id="header" style="background-color: rgb(238, 232, 214); border-bottom: 5px solid #FFFFFF;">
    <div class="container">
        <span style="vertical-align: middle; overflow: hidden;">
            <img style="float: left; margin: 10px;" src="<?php echo site_url('assets/logo/' . $head->logo) ?>" width="100px" />
            <h2 style="float: left; font-family: times new roman; font-size: 31px; color: #825A42; margin-top: 25px;"><?php echo $head->organization_name; ?></h2>
        </span>
    </div>
</div>
<div class="navbar navbar-default">
    <div class="container">
        <div class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
                <li class="<?php if($segment2 == "reports"){ echo "active"; } ?>"><a href="<?php echo site_url('program-officer/reports'); ?>">Reports</a></li>
                <li class="<?php if($segment2 == "backups"){ echo "active"; } ?>"><a href="<?php echo site_url('program-officer/backup') ?>">Backups</a></li>
                <li class="<?php if($segment2 == "forms"){ echo "active"; } ?>"><a href="<?php echo site_url('program-officer/forms');?>">Forms & Formats</a></li>
                <li class="<?php if($segment2 == "policy"){ echo "active"; } ?>"><a href="<?php echo site_url('program-officer/policy');?>">Policies</a></li>
                <li class="<?php if($segment2 == "others"){ echo "active"; } ?>"><a href="<?php echo site_url('program-officer/others');?>">Others</a></li>
                <li class="<?php if($segment1 == "myAccount"){ echo "active"; } ?>"><a href="<?php echo site_url(); ?>myAccount">My Account</a></li>
                <li><a href="<?php echo site_url('loginpage/logout') ?>">Logout</a></li>
            </ul>
        </div>
    </div>
</div>