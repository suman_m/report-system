<?php
class Misc_lib
{
    function __construct()
    {
        $this->CI = & get_instance();
    }
    public function slugify($text)
    {
        $text = preg_replace('~[^\\pL\d]+~u', '-', $text);
        $text = trim($text, '-');
        $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);
        $text = strtolower($text);
        $text = preg_replace('~[^-\w]+~', '', $text);
        if (empty($text))
        {
            return 'n-a';
        }
        return $text;
    }

    public function logsMessage($logTitle)
    {
        $data = $this->CI->db->get_where('logs_message',array('title'=>$logTitle))->row();
        return $data->message;
    }
    public function getUserData($id)
    {
        return  $this->CI->db->get_where('users',array('id'=>$id))->row();
    }

    public function getUserName($id)
    {
        $userData = $this->getUserData($id);

            $region = $this->CI->db->get_where('regions',array('id'=>$userData->region))->row();
        if($userData->region!=0)
            return $userData->name.'('.$userData->accessLevel.' '.$region->name.')';
        else
            return $userData->name.'('.$userData->accessLevel.')';
    }

    public function getReportEditStatus($rep_id)
    {
        $available = $this->CI->db->get_where('report_status',array('report_id'=>$rep_id))->result();
        $reportType = $this->CI->db->get_where('report_category',array('status'=>1))->result();
        if(!empty($available))
        {
            foreach($available as $a):
                foreach($reportType as $r_t):
                    if($r_t->id==$a->report_type_id)
                    {
                        if($a->status==1)
                        {
                            $status = 1;
                        }
                        else
                        {
                            $status = 0;
                            break;
                        }
                    }
                    else
                    {
                        $status = 0;
                        break;
                    }
                endforeach;
            endforeach;
        }
        else
        {
            $status = 0;
        }
        return $status;
    }

    public function getReportStatus($report_id, $report_type_id)
    {
        $status = $this->CI->db->get_where('report_status',array('report_id'=>$report_id,'report_type_id'=>$report_type_id))->row();
        if(!empty($status))
        {
            if($status->status==1)
                $value = 1;
            else if($status->status==2)
                $value = 0;
        }
        else
            $value =0;

        return $value;
    }

    function getReportNotificationStatus()
    {
        $return = '';
        $data = $this->CI->db->get_where('report_settings',array('title'=>"REPORT SETTINGS"))->row();
        if($data->report_submission_time=="quarterly")
        {
            $start_time = date('Y-m-d',strtotime($data->quarterly_start));
            $start_time = new DateTime($start_time);
            $current_time = date('Y-m-d');
            $current_time = new DateTime($current_time);
            $interval = date_diff($start_time, $current_time);
            $months = $interval->format('%m');
            if($months % 2==0)
            {
                if($data->submission_end=="last-day")
                {
                    $lastSubmissionDate =  date('Y-m-t');
                }
                else if($data->submission_end="last-friday")
                {
                    $month = date('F');
                    $year = date('Y');
                    $lastSubmissionDate =  date('Y-m-d',strtotime('last friday of '.$month.' '.$year));
                }
                if(isset($lastSubmissionDate))
                {
                    $currentDate = date('Y-m-d');

                    if(strtotime($currentDate)<=strtotime($lastSubmissionDate))
                    {
                        $currentDate = new DateTime($currentDate);
                        $last = $lastSubmissionDate;
                        $lastSubmissionDate = new DateTime($lastSubmissionDate);
                        $interval = $currentDate->diff($lastSubmissionDate);
                        $daysInterval = $interval->days;
                        if($daysInterval<=$data->email_frequency)
                        {
                            $return = $last;
                        }
                    }
                }
            }
        }
        else if($data->report_submission_time=="monthly")
        {
            if($data->submission_end=="last-day")
            {
                $lastSubmissionDate =  date('Y-m-t');
            }
            else if($data->submission_end="last-friday")
            {
                $month = date('F');
                $year = date('Y');
                $lastSubmissionDate =  date('Y-m-d',strtotime('last friday of '.$month.' '.$year));
            }
            if(isset($lastSubmissionDate))
            {
                $currentDate = date('Y-m-d');

                if(strtotime($currentDate)<=strtotime($lastSubmissionDate))
                {
                    $currentDate = new DateTime($currentDate);
                    $last = $lastSubmissionDate;
                    $lastSubmissionDate = new DateTime($lastSubmissionDate);
                    $interval = $currentDate->diff($lastSubmissionDate);
                    $daysInterval = $interval->days;
                    if($daysInterval<=$data->email_frequency)
                    {
                        $return = $last;
                    }
                }
            }
        }
        return $return;
    }

    function getProjectHolderName($id)
    {
        $av = $this->CI->db->get_where('project_holder_detail',array('project_id'=>$id))->row();
        if(!empty($av))
        {
            $user = $this->CI->db->get_where('users',array('ID'=>$av->user_id))->row();
            if(!empty($user))
                $name = $user->name;
            else
                $name = '-';
        }
        else
        {
            $name = "-";
        }
        return $name;
    }

    function getProjectName($id)
    {
        $av = $this->CI->db->get_where('projects',array('id'=>$id))->row();
        if(!empty($av))
            return $av->name;
        else
            return '-';
    }
}