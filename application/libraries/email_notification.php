<?php
class Email_notification
{
    function __construct()
    {
        $this->CI = & get_instance();
    }

    function getEmailForHolder($id)
    {
        $userData = $this->CI->db->get_where('users',array('ID'=>$id))->row();
        $regionId = $userData->region;
        $userAbove = $this->CI->db->get_where('user_hierarchy',array('user_id'=>$id))->row();
        if(!empty($userAbove))
        {
            $userAboveEmail = $this->CI->db->get_where('users',array('ID'=>$userAbove->parent_user_id))->row();
            $email[] = $userAboveEmail->email;
        }
        $allUserOfRegion = $this->CI->db->get_where('users',array('active'=>'1','region'=>$regionId,'accessLevel'=>"Project Manager"))->result();
        foreach($allUserOfRegion as $all):
            $email[] = $all->email;
            endforeach;
        $email[] = $this->CI->db->get_where('users',array('active'=>1,'accessLevel'=>"Head Officer"))->row()->email;
        return $email;
    }

    function getEmailForOfficer($id)
    {
        $userData = $this->CI->db->get_where('users',array('ID'=>$id))->row();
        $regionId = $userData->region;
        $userAbove = $this->CI->db->get_where('user_hierarchy',array('user_id'=>$id))->row();
        if(!empty($userAbove))
        {
            $userAboveEmail = $this->CI->db->get_where('users',array('ID'=>$userAbove->parent_user_id))->roiw();
            $email[] = $userAboveEmail->email;
        }
        $allUserOfRegion = $this->CI->db->get_where('users',array('active'=>'1','region'=>$regionId,'accessLevel'=>"Project Manager"))->result();
        foreach($allUserOfRegion as $all):
            $email[] = $all->email;
        endforeach;
        $email[] = $userData->email;
        return $email;
    }

    function getBackupMessage($type,$backupId  = '')
    {
        if($type=="submit")
            $msg = $this->BackupSubmittedMessage();
        else if($type=="edit")
            $msg = $this->BackupModifiedMessage();
        else if($type=="accept")
            $msg = $this->BackupAcceptedMessage($backupId);
        else if($type=="reject")
            $msg = $this->BackupRejectedMessage($backupId);

        return $msg;

    }

    function BackupSubmittedMessage()
    {
        $userData = $this->CI->db->get_where('users',array('ID'=>$this->CI->session->userdata('logedin_id')))->row();
        $msg = "Hi there, <br />";
        $msg .= $userData->name.'('.$userData->accessLevel.') has submitted the backup report for the month of '.date('F').'<br />';
        $msg .= 'Please <a href="'.site_url('loginPage').'">Login</a> here to view the backup data <br /><br />';
        $msg .="Thanks!<br />Heifier International";
        return $msg;
    }

    function BackupModifiedMessage()
    {
        $userData = $this->CI->db->get_where('users',array('ID'=>$this->CI->session->userdata('logedin_id')))->row();
        $msg = "Hi there, <br />";
        $msg .= $userData->name.'('.$userData->accessLevel.') has modified the backup report for the month of '.date('F').'<br />';
        $msg .= 'Please <a href="'.site_url('loginPage').'">Login</a> here to view the backup data <br /><br />';
        $msg .="Thanks!<br />Heifier International";
        return $msg;
    }

    function BackupAcceptedMessage($backupId)
    {
        $userData = $this->CI->db->get_where('users',array('ID'=>$this->CI->session->userdata('logedin_id')))->row();
        $backupData = $this->CI->db->get_where('backup',array('id'=>$backupId))->row();
        $uploadData = $this->CI->db->get_where('users',array('ID'=>$backupData->user_id))->row();
        $msg = "Hi there, <br />";
        $msg .= $userData->name.'('.$userData->accessLevel.') has accepted the backup report for the month of '.date('F').' submitted by '.$uploadData->name.'('.$uploadData->accessLevel.') from the '.$this->getRegionData($uploadData->region).'<br />';
        $msg .= 'Please <a href="'.site_url('loginPage').'">Login</a> here to view the backup data <br /><br />';
        $msg .="Thanks!<br />Heifier International";
        return $msg;
    }

    function BackupRejectedMessage($backupId)
    {
        $userData = $this->CI->db->get_where('users',array('ID'=>$this->CI->session->userdata('logedin_id')))->row();
        $backupData = $this->CI->db->get_where('backup',array('id'=>$backupId))->row();
        $uploadData = $this->CI->db->get_where('users',array('ID'=>$backupData->user_id))->row();
        $msg = "Hi there, <br />";
        $msg .= $userData->name.'('.$userData->accessLevel.') has rejected the backup report for the month of '.date('F').' submitted by '.$uploadData->name.'('.$uploadData->accessLevel.') from the '.$this->getRegionData($uploadData->region).'<br />';
        $msg .= 'Please <a href="'.site_url('loginPage').'">Login</a> here to view the backup data <br /><br />';
        $msg .="Thanks!<br />Heifier International";
        return $msg;
    }

    function getRegionData($id)
    {
        $data = $this->CI->db->get_where('regions',array('id'=>$id))->row();
        return $data->name;
    }

    function getReportMessage($type,$reportId = '')
    {
        if($type=="submit")
            $msg = $this->ReportSubmittedMessage();
        else if($type=="edit")
            $msg = $this->ReportModifiedMessage();
        else if($type=="accept")
            $msg = $this->ReportAcceptedMessage($reportId);
        else if($type=="reject")
            $msg = $this->ReportRejectedMessage($reportId);

        return $msg;

    }

    function ReportSubmittedMessage()
    {
        $userData = $this->CI->db->get_where('users',array('ID'=>$this->CI->session->userdata('logedin_id')))->row();
        $msg = "Hi there, <br />";
        $msg .= $userData->name.'('.$userData->accessLevel.') has submitted the report for the month of '.date('F').'<br />';
        $msg .= 'Please <a href="'.site_url('loginPage').'">Login</a> here to view the backup data <br /><br />';
        $msg .="Thanks!<br />Heifier International";
        return $msg;
    }

    function ReportModifiedMessage()
    {
        $userData = $this->CI->db->get_where('users',array('ID'=>$this->CI->session->userdata('logedin_id')))->row();
        $msg = "Hi there, <br />";
        $msg .= $userData->name.'('.$userData->accessLevel.') has modified the report for the month of '.date('F').'<br />';
        $msg .= 'Please <a href="'.site_url('loginPage').'">Login</a> here to view the backup data <br /><br />';
        $msg .="Thanks!<br />Heifier International";
        return $msg;
    }

    function ReportAcceptedMessage($reportId)
    {
        $userData = $this->CI->db->get_where('users',array('ID'=>$this->CI->session->userdata('logedin_id')))->row();
        $reportData = $this->CI->db->get_where('reports',array('id'=>$reportId))->row();
        $uploadData = $this->CI->db->get_where('users',array('ID'=>$reportData->user_id))->row();
        $msg = "Hi there, <br />";
        $msg .= $userData->name.'('.$userData->accessLevel.') has accepted the  report for the month of '.date('F').' submitted by '.$uploadData->name.'('.$uploadData->accessLevel.') from the '.$this->getRegionData($uploadData->region).'<br />';
        $msg .= 'Please <a href="'.site_url('loginPage').'">Login</a> here to view the backup data <br /><br />';
        $msg .="Thanks!<br />Heifier International";
        return $msg;
    }

    function ReportRejectedMessage($reportId)
    {
        $userData = $this->CI->db->get_where('users',array('ID'=>$this->CI->session->userdata('logedin_id')))->row();
        $reportData = $this->CI->db->get_where('reports',array('id'=>$reportId))->row();
        $uploadData = $this->CI->db->get_where('users',array('ID'=>$reportData->user_id))->row();
        $msg = "Hi there, <br />";
        $msg .= $userData->name.'('.$userData->accessLevel.') has rejected the  report for the month of '.date('F').' submitted by '.$uploadData->name.'('.$uploadData->accessLevel.') from the '.$this->getRegionData($uploadData->region).'<br />';
        $msg .= 'Please <a href="'.site_url('loginPage').'">Login</a> here to view the backup data <br /><br />';
        $msg .="Thanks!<br />Heifier International";
        return $msg;
    }
}