<?php

/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 7/20/14
 * Time: 10:03 AM
 */
class Policy extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('common_model');
        if (!$this->session->userdata('logged_in'))
            redirect('loginpage');
        $this->load->model('policy_model');
    }

    public function index() {
        $data['policy'] = $this->policy_model->getallpolicy();
        $categories = $this->policy_model->getPolicyCategories();
        $data['categories'] = array(0 => 'Not Selected');
        foreach($categories as $cat){
            $data['categories'][$cat->id] = $cat->name;
        }
        $this->load->view('head-officer/policydashboard', $data);
    }

    public function add() {
        if ($this->input->post('submitBtn')) {
            $form = $_FILES['file']['name'];
            $config['upload_path'] = BASEPATH . '../policies/';
            $config['allowed_types'] = "pdf|doc|docx|xls|xlsx";
            $config['max_size'] = 0;
            //$config['encrypt_name'] = TRUE;
            $this->load->library('upload', $config);

            if (!$this->upload->do_upload('file')) {

                $error = array('error' => $this->upload->display_errors());
                $err = $error['error'];
                $data = '<div class="alert alert-danger" role="alert"> <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>' . $err . '</div>';
                $this->session->set_flashdata('msg', $data);
                redirect("head-officer/policy/add");
            } else {
                $data = $this->upload->data();
                $file_name = $data['file_name'];
                $result = $this->policy_model->sendform($file_name);
                if ($result) {
                    /*$emaillist = $this->input->post('optional');
                    $emails = $this->policy_model->allemail($emaillist);
                    $this->load->library('email');
                    $config['mailtype'] = "html";
                    $this->email->initialize($config);
                    $to_email = '';
                    foreach ($emails as $email) {
                        $to_email[] = $email->email;
                    }
                    $newemail = implode(',', $to_email);
                    $msg = 'Hi there , <br /><br /> A new policy documents has been uploaded by the Head Officer. <br /> Please Login to the site to view the documents. <br /><br />';
                    $msg .= 'Thanks!<br />Heifier International';
                    $this->email->from('info@heifierinternational.com', 'Heifier international');
                    $this->email->to($newemail);
                    $this->email->subject('New Policy');
                    $this->email->message($msg);
                    $this->email->send(); */
                    //echo $this->email->print_debugger();
                    $this->session->set_flashdata('msg', 'successfully added');
                    redirect("head-officer/policy");
                }
            }
        }
        $data['region'] = $this->common_model->getTableData('regions')->result();
        $data ['usrhier'] = $this->policy_model->getAll();
        $data ['categories'] = $this->policy_model->getPolicyCategories();
        $this->load->view("head-officer/policy", $data);
    }

    public function history() {
        if ($this->input->post('submitBtn')) {
            $data['history'] = $this->policy_model->search();
        } else {
            $id = $this->uri->segment(4);
            $data ['history'] = $this->policy_model->gethistory($id);
        }
        $data['region'] = $this->policy_model->regions();
        $this->load->view("head-officer/policy_history", $data);
    }

    public function list_category() {
        $data["categories"] = $this->policy_model->getPolicyCategories();
        $this->load->view('head-officer/policy_category', $data);
    }

    public function save_category(){
        if($this->policy_model->check_category_exist($this->input->post("categoryname"))){
            $this->data["success"] = 1;
            $this->data["successMsg"] = "Policy Category with this name already exist.";
            $this->data["successType"] = "danger";
        }else{
            $this->data["success"] = $this->policy_model->add_category($this->input->post("categoryname"));
            $this->data["successMsg"] = "Policy Category added successfully.";
            $this->data["successType"] = "success";
        }
        $this->list_category();
    }

    public function update_category(){
        $this->data["success"] = $this->policy_model->update_category($this->input->post("categoryname"), $this->input->post("editid"));
        $this->data["successMsg"] = "Policy Category updated successfully.";
        $this->data["successType"] = "success";
        $this->list_category();
    }

    public function delete_category(){
        $this->data["success"] = $this->policy_model->delete_category($this->input->get("id"));
        $this->data["successMsg"] = "Policy Category deleted successfully.";
        $this->data["successType"] = "success";
        $this->list_category();
    }
}
