<?php
    class SpecialReport extends CI_Controller
    {
        function __construct()
        {
            parent::__construct();
            $this->load->model('common_model');
            $this->load->library('misc_lib');
            $this->load->model('special_report_model','special');
        }

        function index()
        {
            $data['title'] = "Specials Report";
            $data['region'] = $this->common_model->getTableData('regions')->result();
            if($this->input->post('submit')):
                $data['form'] = $this->input->post('formformat');
                $data['regions'] = $this->input->post('region');
            $data['specialReport'] = $this->special->getSearchData($data);
            else:
                $data['specialReport'] = $this->common_model->getTableData('special_report')->result();
            endif;
            $data['form_format'] = $this->common_model->getTableData('forms_format',array('valid_date >'=>date('Y-m-d')))->result();
            $data['logs'] = $this->common_model->getTableData('special_report_logs')->result();
            $this->load->view('head-officer/specialReport',$data);
        }

        function downloadReport($id)
        {
            $data = $this->common_model->getTableData('special_report',array('id'=>$id))->row();

            $logsMessage['user_id'] = $this->session->userdata('logedin_id');
            $logsMessage['special_report_id'] = $id;
            $logsMessage['logs'] = "Special Report Downloaded";
            $logsMessage['access_level'] = $this->session->userdata('access_level');
            $this->common_model->insertData('special_report_logs',$logsMessage);

            $this->load->helper('download');
            if(file_exists(BASEPATH.'../specialReport/'.$data->files)):
            $file = file_get_contents(BASEPATH.'../specialReport/'.$data->files);
            $name = $data->files;
                else:
                redirect('head-officer/specialReport');
                    endif;
            force_download($name,$file);
        }

        function downloadZip()
        {
            if($this->input->post('submit')):
                $data['form'] = $this->input->post('formformat');
                $data['regions'] = $this->input->post('region');
                $specialReport = $this->special->getSearchData($data);
                if(!empty($specialReport))
                {
                    $filename = BASEPATH.'../specialZip';
                    foreach($specialReport as $sr):
                        $formname = $this->common_model->getTableData('forms_format',array('id'=>$sr->form_id))->row();
                        $innerFile = $filename.'/'.$this->slugify($formname->form_format_name);
                        if(!is_dir($innerFile))
                            mkdir($innerFile);
                        @copy(BASEPATH.'../specialReport/'.$sr->files, $innerFile.'/'.$sr->files);
                        $files_to_zip[] = $innerFile.'/'.$sr->files;
                    endforeach;
                    $this->create_zip($files_to_zip,'special-report.zip');
                    $this->load->helper('download');
                    $data = file_get_contents('special-report.zip');
                    $name = 'special-report.zip';
                    if(file_exists($name))
                        @unlink($name);
                    force_download($name,$data);
                }
            else
            {
              $this->session->set_flashdata('error','No data Available');
                redirect('head-officer/specialReport/');
            }
            endif;
        }

        function create_zip($files = array(),$destination = '',$overwrite = false) {
            //if the zip file already exists and overwrite is false, return false
            if(file_exists($destination) && !$overwrite) { return false; }
            //vars
            $valid_files = array();
            //if files were passed in...
            if(is_array($files)) {
                //cycle through each file
                foreach($files as $file) {
                    //make sure the file exists
                    if(file_exists($file)) {
                        $valid_files[] = $file;
                    }
                }
            }
            //if we have good files...
            if(count($valid_files)) {
                //create the archive
                $zip = new ZipArchive();
                if($zip->open($destination,$overwrite ? ZIPARCHIVE::OVERWRITE : ZIPARCHIVE::CREATE) !== true) {
                    return false;
                }
                //add the files
                foreach($valid_files as $file) {
                    $zip->addFile($file,$file);
                }
                //debug
                //echo 'The zip archive contains ',$zip->numFiles,' files with a status of ',$zip->status;

                //close the zip -- done!
                $zip->close();

                //check to make sure the file exists
                return file_exists($destination);
            }
            else
            {
                return false;
            }
        }

        public function slugify($text)
        {
            $text = preg_replace('~[^\\pL\d]+~u', '-', $text);
            $text = trim($text, '-');
            $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);
            $text = strtolower($text);
            $text = preg_replace('~[^-\w]+~', '', $text);
            if (empty($text))
            {
                return 'n-a';
            }
            return $text;
        }

        function submissionListData()
        {
            $form = $_POST['form'];
            $status = $_POST['status'];

            $formformat = $this->common_model->getTableData('forms_format',array('id'=>$form))->row();
            $formData = $this->common_model->getTableData('form_receiver',array('form_id'=>$form))->result();
            $submissionList = $this->common_model->getTableData('special_report',array('form_id'=>$form))->result();
            if($status ==1)
            {
                if(!empty($submissionList))
                {
                    $content ='<table class="table table-striped ">
                                <tr>
                                    <th>SN</th>
                                    <th>Report Title</th>
                                    <th>Submitted By</th>
                                    <th>Report List</th>
                                    <th>Special Notes</th>
                                    <th>Submission Date</th>
                                </tr>';

                    $i = 1;
                    foreach ($submissionList as $sr):

                        $content .='<tr>
                            <td>'.$i.'</td>
                            <td>'.$sr->title.'</td>
                            <td>'.$this->misc_lib->getUserName($sr->user_id).'</td>
                            <td>'.$sr->files.'</td>
                            <td>'.$sr->special_notes.'</td>
                            <td>'.$sr->date.'</td>
                        </tr>';
                        $i++;
                    endforeach;
                }
                else
                {
                    $content = "No record available";
                }

            }
            else if($status ==2){
            foreach($formData as $key=>$fd):
                foreach($submissionList as $sl):
                    if($fd->receiver_id==$sl->user_id)
                        unset($formData[$key]);
                    endforeach;
                endforeach;
                if(!empty($formData))
                {
                    $content ='<table class="table table-striped ">
                                <tr>
                                    <th>SN</th>
                                    <th>Form Format Name</th>
                                    <th>User</th>
                                </tr>';
                    $i = 1;
                    foreach($formData as $fd):
                        $content .='
                        <tr>
                            <td>'.$i.'</td>
                            <td>'.$formformat->form_format_name.'</td>
                            <td>'.$this->misc_lib->getUserName($fd->receiver_id).'</td>
                        </tr>';
                        $i++;
                        endforeach;
                }
                else
                {
                    $content = "All project holder have submitted the report";
                }
            }
            echo $content;

        }
    }