<?php

/**
 * Class Reports
 */
class Reports extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('common_model');
        $this->load->model('reports_model');
        $this->load->library('misc_lib');
        $this->load->library('email_notification');
        if(!$this->session->userdata('logged_in'))
            redirect('loginpage');
    }

    function index()
    {
        $data['title'] = "Report Management";
        $currentMonth = date('n');
        $lastTwoMonth = $currentMonth - 2;
        $date = date('Y').'-'.$lastTwoMonth.'-1';
        $lastTwoMonthDate = date('Y-m-d h:i:s',strtotime($date));
        if($this->input->post('submit'))
        {
            $data['dateStart'] = $this->input->post('dateStart');
            $data['dateEnd'] = $this->input->post('dateEnd');
            $data['region'] = $this->input->post('region');
            $data['month'] = $this->input->post('month');
            $data['project'] = $this->input->post('project');
            $value = $this->reports_model->getSearchResult($data);
            $data['reports'] = $value;
        }
        else
            $data['reports'] = $this->common_model->getTableData('reports',array('month >='=>$lastTwoMonth))->result();
        $data['months'] = $lastTwoMonth;
        $reportType = $this->common_model->getTableData('report_settings',array('title'=>"REPORT SETTINGS"))->row();
        if(!empty($reportType))
            $data['value'] = $reportType;

        $data['reportType'] = $this->common_model->getTableData('report_category',array('status'=>1))->result();
        $data['reportStatus'] = $this->common_model->getTableData('report_status')->result();
        $data['reportLogs'] = $this->common_model->getTableData('reports_logs',array('date >='=>$lastTwoMonthDate),'','','','','',array('date','DESC'))->result();
        $access_level = $this->slugify($this->session->userdata('access_level'));
        $data['regions'] = $this->common_model->getTableData('regions')->result();
        $data['projects'] = $this->common_model->getTableData('projects')->result();
        $this->load->view('head-officer/reports',$data);
    }

    public function slugify($text)
    {
        $text = preg_replace('~[^\\pL\d]+~u', '-', $text);
        $text = trim($text, '-');
        $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);
        $text = strtolower($text);
        $text = preg_replace('~[^-\w]+~', '', $text);
        if (empty($text))
        {
            return 'n-a';
        }
        return $text;
    }

    function addReportType()
    {
        if($this->input->post('submit'))
        {
            //$data['report_category'] = json_encode($this->input->post('reportType'));
            $report = $this->input->post('reportType');
            $data['report_submission_time'] = $this->input->post('reportSubmissionTime');
            if($data['report_submission_time']=="quarterly")
                $data['quarterly_start'] = $this->input->post('quarter_start_time');
            else
                $data['quarterly_start'] = "";
            $data['submission_end'] = $this->input->post('report_submission_end');
            $data['email_frequency'] = $this->input->post('email_frequency');
            if($this->input->post('status'))
                $data['status'] = $this->input->post('status');
            else
                $data['status'] = '0';
            $content = $this->common_model->getTableData('report_settings',array('title'=>'REPORT SETTINGS'))->row();
            if(empty($content))
            {
                $data['title'] = 'REPORT SETTINGS';
                $this->common_model->insertData('report_settings',$data);
            }
            else
            {
                $this->common_model->updateTableData('report_settings',$content->id,$data);
            }
            if(!empty($report)):
                foreach($report as $re):
                    $insertData['title'] = $re;
                    $insertData['status'] = 1;
                    $this->common_model->insertData('report_category',$insertData);
                endforeach;
            endif;
            $id = $this->input->post('id');
            if($id !="")
            {
                $id = explode(',',$id);
                foreach($id as $i):
                    if($this->input->post('reportCategory-'.$i))
                    {
                        $updateData['title'] = $this->input->post('reportCategory-'.$i);
                        $this->common_model->updateTableData('report_category',$i,$updateData);
                    }
                    else
                    {
                        $changeData['status'] = 0;
                        $this->common_model->updateTableData('report_category',$i,$changeData);
                    }
                endforeach;
            }

            $this->session->set_flashdata('success','Data Saved Succesfully.');
            redirect('head-officer/reports');
        }

    }

    function editReportType($id)
    {
        if($this->input->post('submit'))
        {
            $data['report_category'] = $this->input->post('reportType');
            $data['report_submission_time'] = $this->input->post('reportSubmissionTime');
            if($this->input->post('status'))
                $data['status'] = $this->input->post('status');
            else
                $data['status'] = '0';

            $this->common_model->updateTableData('report_settings',$id,$data);
            $this->session->set_flashdata('success','Data Saved Succesfully.');
            redirect('reports');
        }
        $data['title'] = "Edit Report Type";
        $data['value'] = $this->common_model->getTableData('report_settings',array('id'=>$id))->row();
        $this->load->view('head-officer/addReport',$data);
    }

    function deleteReportType($id)
    {
        $this->common_model->deleteTableData('report_settings',array('id'=>$id));
        $this->session->set_flashdata('success','Data Deleted Succesfully.');
        redirect('reports');
    }

    function downloadReport($report_id,$report_type_id)
    {
        $report = $this->common_model->getTableData('reports',array('id'=>$report_id))->row();
        $report_content = json_decode($report->files);
        foreach($report_content as $key=> $content):
            if($key==$report_type_id)
            {
                $insertData['report_id'] = $report_id;
                $insertData['report_type_id'] = $report_type_id;
                $insertData['user_id'] = $this->session->userdata('logedin_id');
                $insertData['logs'] = "REPORT DOWNLOADED";
                $this->common_model->insertData('reports_logs',$insertData);
                $this->load->helper('download');
                if(file_exists(BASEPATH.'../reports/'.$content)):
                $data = file_get_contents(BASEPATH.'../reports/'.$content);
                $name = $content;
                force_download($name,$data);
                    endif;

            }
            endforeach;
    }

    function acceptReport()
    {
        $report_id = $_POST['report_id'];
        $report_type_id = $_POST['report_type_id'];
        $insertData['report_id'] = $report_id;
        $insertData['report_type_id'] = $report_type_id;
        $insertData['status'] = 1;
        $insertData['update_by'] = $this->session->userdata('logedin_id');
        $available = $this->common_model->getTableData('report_status',array('report_id'=>$report_id,'report_type_id'=>$report_type_id))->row();
        if(empty($available))
        {
            $this->common_model->insertData('report_status',$insertData);
        }
        else
        {
            $this->common_model->updateTableData('report_status',$available->id,$insertData);
        }


        $logData['report_id'] = $report_id;
        $logData['report_type_id'] = $report_type_id;
        $logData['user_id'] = $this->session->userdata('logedin_id');
        $logData['logs'] = "REPORT ACCEPTED";
        $this->common_model->insertData('reports_logs',$logData);
        $this->sentNotificationEmail('accept',$report_id);
    }

    function rejectReport()
    {
        $report_id = $_POST['report_id'];
        $report_type_id = $_POST['report_type_id'];
        $insertData['report_id'] = $report_id;
        $insertData['report_type_id'] = $report_type_id;
        $insertData['status'] = 2;
        $insertData['update_by'] = $this->session->userdata('logedin_id');
        $available = $this->common_model->getTableData('report_status',array('report_id'=>$report_id,'report_type_id'=>$report_type_id))->row();
        if(empty($available))
        {
            $this->common_model->insertData('report_status',$insertData);
        }
        else
        {
            $this->common_model->updateTableData('report_status',$available->id,$insertData);
        }

        $logData['report_id'] = $report_id;
        $logData['report_type_id'] = $report_type_id;
        $logData['user_id'] = $this->session->userdata('logedin_id');
        $logData['logs'] = "REPORT REJECTED";
        $this->common_model->insertData('reports_logs',$logData);

        $this->sentNotificationEmail('reject',$report_id);
    }

    function sentNotificationEmail($type,$id)
    {
        $userId = $this->common_model->getTableData('backup',array('id'=>$id))->row()->user_id;
        $emailList = $this->email_notification->getEmailForOfficer($userId);
        $msg = $this->email_notification->getReportMessage($type,$id);
        $this->load->library('email');
        $config['mailtype'] = "html";
        $this->email->initialize($config);
        foreach($emailList as $email):
            $this->email->from('info@heifierinternational.com','Heifer International');
            $this->email->to($email);
            $this->email->subject('Report Status');
            $this->email->message($msg);
            $this->email->send();
            $this->email->clear();
        endforeach;
    }

    function downloadZip()
    {
        $month = $_POST['month'];
        $region = $_POST['region'];
        if($month == 0 && $region == 0):
            $this->session->set_flashdata('month','Choose the month to download the zip');
            redirect('head-officer/reports');
        endif;
        //$mon = date('m',strtotime('01-'.$month.'-2014'));
        $data = $this->reports_model->getDownloadData($month,$region);
        $userData = $this->common_model->getTableData('users',array('ID'=>$this->session->userdata('logedin_id')))->row();
        /*$reportData = $this->common_model->getTableData('reports')->result();
        foreach($reportData as $key=>$rd):
            if(date('m',strtotime($rd->date))==$mon)
            {
                $data[$key] = $rd;
            }
            endforeach; */
        if(empty($data))
        {
            $this->session->set_flashdata('month','No reports Available for the month of '.date('F',strtotime('01-'.$month.'-2014')));
            redirect('head-officer/reports');
        }
        else
        {
            $filename = BASEPATH.'../reportsZip/'.$this->slugify($userData->name).'-'.$this->session->userdata('logedin_id');
            if(!is_dir($filename))
                mkdir($filename);
            $innerFileName =BASEPATH.'../reportsZip/'.date('F',strtotime('01-'.$month.'-2014'));
            if(!is_dir($innerFileName))
                mkdir($innerFileName);
            foreach($data as $d):
                $getUser = $this->common_model->getTableData('users',array('ID'=>$d->user_id))->row();
                $region = $this->common_model->getTableData('regions',array('id'=>$getUser->region))->row();
                $project = $this->common_model->getTableData('projects',array('id'=>$d->project_id))->row();
                $userFile = $innerFileName.'/'.$this->slugify($getUser->name).'-'.$getUser->ID.'-'.$this->slugify($region->name);
                if(!is_dir($userFile))
                    mkdir($userFile);
                $projectFile = $userFile.'/'.$project->number;
                if(!is_dir($projectFile))
                    mkdir($projectFile);
                $files = json_decode($d->files);
                foreach($files as $f):
                        @copy(BASEPATH.'../reports/'.$f , $projectFile.'/'.$f);
                    $files_to_zip[] = $projectFile.'/'.$f;
                    endforeach;
                endforeach;
            $this->create_zip($files_to_zip,date('F',strtotime('01-'.$month.'-2014')).'.zip');
            $this->load->helper('download');
            $data = file_get_contents(date('F',strtotime('01-'.$month.'-2014')).'.zip');
            $name = date('F',strtotime('01-'.$month.'-2014')).'.zip';
            if(file_exists($name))
                @unlink($name);
            force_download($name,$data);
        }

    }

    function create_zip($files = array(),$destination = '',$overwrite = false) {
        //if the zip file already exists and overwrite is false, return false
        if(file_exists($destination) && !$overwrite) { return false; }
        //vars
        $valid_files = array();
        //if files were passed in...
        if(is_array($files)) {
            //cycle through each file
            foreach($files as $file) {
                //make sure the file exists
                if(file_exists($file)) {
                    $valid_files[] = $file;
                }
            }
        }
        //if we have good files...
        if(count($valid_files)) {
            //create the archive
            $zip = new ZipArchive();
            if($zip->open($destination,$overwrite ? ZIPARCHIVE::OVERWRITE : ZIPARCHIVE::CREATE) !== true) {
                return false;
            }
            //add the files
            foreach($valid_files as $file) {
                $zip->addFile($file,$file);
            }
            //debug
            //echo 'The zip archive contains ',$zip->numFiles,' files with a status of ',$zip->status;

            //close the zip -- done!
            $zip->close();

            //check to make sure the file exists
            return file_exists($destination);
        }
        else
        {
            return false;
        }
    }

    public function getSubmissionList()
    {
        $month = $_POST['month'];
        $status = $_POST['status'];
        $data = $this->common_model->getTableData('reports',array('month'=>$month))->result();
        $m = date('m') - 2;

        $lastDate = date('Y-m-d h:i:s',strtotime(date('Y').'-'.$m.'-01'));
        $reportsStatus = $this->common_model->getTableData('report_status',array('date >='=>$lastDate))->result();
        $reportsSetting = $this->common_model->getTableData('report_category',array('status'=>'1'))->result();
        if($status !=0 && $status !=3){
        if(!empty($data)):
            $statusArray = array();
            foreach($data as $d):
                $flag = 0;

                foreach($reportsSetting as $rs):
                    $available = $this->common_model->getTableData('report_status',array('report_id'=>$d->id,'report_type_id'=>$rs->id))->row();
                    if(empty($available))
                    {
                        $statusArray[$d->id] = 2;
                        $flag = 0;
                        break;
                    }
                    else
                    {
                       if($available->status==2)
                       {
                           $statusArray[$d->id] = 2;
                           $flag = 0;
                        break;
                       }
                        else if($available->status==1)
                        {
                            $flag = 1;
                        }
                    }
                    endforeach;
            if($flag ==1)
            {
                $statusArray[$d->id] = 1;
            }
            endforeach;
            $content = '<table class="table table-striped ">
                    <tr>
                        <th>SN</th>
                        <th>Submitted By</th>
                        <th>Project Name</th>
                        <th>Report of The Month</th>
                        <th>Submitted Date</th>
                    </tr>';
            $i = 1;
            foreach($statusArray as $key=>$sa):
                      if($sa == $status)
                      {
                          $reportData = $this->common_model->getTableData('reports',array('id'=>$key))->row();
                          $content .='
                        <tr>
                            <td>'.$i.'</td>
                            <td>'.$this->misc_lib->getUserName($reportData->user_id).'</td>
                            <td>'.$this->misc_lib->getProjectName($reportData->project_id).'</td>
                            <td>'.date('F',strtotime('01-'.$reportData->month.'-'.date('Y'))).'</td>
                            <td>'.$reportData->date.'</td>
                        </tr>
                    ';
                          $i++;
                      }
                endforeach;
        echo $content;

        else:
            echo "No Reports Available for the month";
            endif;
        }
        else if($status ==3)
        {
            $projectData = $this->common_model->getTableData('projects')->result();


            if(!empty($data))
            {
                foreach($projectData as $pd):
                    $userId[$pd->id] = $pd->assign_to_project_holder;
                endforeach;
                foreach($data as $da):
                    foreach($userId as $key=>$u):
                        if($u==$da->user_id && $key==$da->project_id)
                            unset($userId[$key]);
                        endforeach;
                    endforeach;
                $content = '<table class="table table-striped ">
                    <tr>
                        <th>SN</th>
                        <th>Project Holder</th>
                        <th>Project Name</th>
                        <th>Report of The Month</th>
                    </tr>';
                $i = 1;
                foreach($projectData as $pd):
                    if(in_array($pd->assign_to_project_holder,$userId)):
                    $projectHolderDetail = $this->common_model->getTableData('project_holder_detail',array('project_id'=>$pd->id))->row();
                    $content .='<tr>
                        <td>'.$i.'</td>
                        <td>'.$this->misc_lib->getUserName($projectHolderDetail->user_id).'</td>
                        <td>'.$pd->name.'</td>
                        <td>'.date('F',strtotime('01-'.$month.'-'.date('Y'))).'</td>
                    </tr>';
                    $i++;
                        endif;
                endforeach;
                echo $content;
            }
            else
            {
                $content = '<table class="table table-striped ">
                    <tr>
                        <th>SN</th>
                        <th>Project Holder</th>
                        <th>Project Name</th>
                        <th>Report of The Month</th>
                    </tr>';
                $i = 1;
                foreach($projectData as $pd):
                    $projectHolderDetail = $this->common_model->getTableData('project_holder_detail',array('project_id'=>$pd->id))->row();
                    $content .='<tr>
                        <td>'.$i.'</td>
                        <td>'.$this->misc_lib->getUserName($projectHolderDetail->user_id).'</td>
                        <td>'.$pd->name.'</td>
                        <td>'.date('F',strtotime('01-'.$month.'-'.date('Y'))).'</td>
                    </tr>';
                $i++;
                endforeach;
                echo $content;
            }
        }
        else
        {
        if(!empty($data)):
            $content = '<table class="table table-striped ">
                    <tr>
                        <th>SN</th>
                        <th>Submitted By</th>
                        <th>Project Name</th>
                        <th>Report of The Month</th>
                        <th>Submitted Date</th>
                    </tr>';
            $i = 1;
                foreach($data as $d):
                    $content .='
                        <tr>
                            <td>'.$i.'</td>
                            <td>'.$this->misc_lib->getUserName($d->user_id).'</td>
                            <td>'.$this->misc_lib->getProjectName($d->project_id).'</td>
                            <td>'.date('F',strtotime('01-'.$d->month.'-'.date('Y'))).'</td>
                            <td>'.$d->date.'</td>
                        </tr>
                    ';
                   $i++;
                    endforeach;
            echo $content;
            else:
            echo "No Reports Submission Available for the month";
                endif;
        }
    }
}