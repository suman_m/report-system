<?php

class Regions extends CI_Controller {

    public $data = '';

    function __construct(){
        parent::__construct();
        $this->load->model("head-officer/regions_model");
        if(!$this->session->userdata('logged_in'))
            redirect('loginpage');
    }

    public function index(){
        $this->data["regions"] = $this->regions_model->get_regions();
        $this->load->view("head-officer/regionlisting",$this->data);
    }

    public function save(){
        if($this->regions_model->check_region_exist($this->input->post("regionname"))){
            $this->data["success"] = 1;
            $this->data["successMsg"] = "Region with this name already exist.";
            $this->data["successType"] = "danger";
        }else{
            $this->data["success"] = $this->regions_model->add_region($this->input->post("regionname"));
            $this->data["successMsg"] = "Region added successfully.";
            $this->data["successType"] = "success";
        }
        $this->index();
    }

    public function update(){
        $this->data["success"] = $this->regions_model->update_region($this->input->post("regionname"), $this->input->post("editid"));
        $this->data["successMsg"] = "Region updated successfully.";
        $this->data["successType"] = "success";
        $this->index();
    }

    public function delete(){
        $this->data["success"] = $this->regions_model->delete_region($this->input->get("id"));
        $this->data["successMsg"] = "Region deleted successfully.";
        $this->data["successType"] = "success";
        $this->index();
    }
}
?>