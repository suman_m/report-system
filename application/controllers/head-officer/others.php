<?php
    class Others extends CI_Controller
    {
        function __construct()
        {
            parent::__construct();
            $this->load->model('common_model');
            $this->load->library('misc_lib');
            $this->load->model('others_model');
            $this->load->library('misc_lib');
        }

        function index()
        {
            $data['title'] = "Other Documents";
            $data['othersData'] = $this->common_model->getTableData('others')->result();
            $data['othersLog'] = $this->common_model->getTableData('others_log')->result();
            $this->load->view('head-officer/others',$data);
        }

        function add()
        {
            if($this->input->post('submitBtn'))
            {


                $form = $_FILES['file']['name'];
                $config['upload_path'] = BASEPATH.'../others/';
                $config['allowed_types'] = "pdf|doc|docx|xls|xlsx";
                $config['max_size'] = 0;
                $config['encrypt_name'] = TRUE ;
                $this->load->library('upload',$config);

                if ( ! $this->upload->do_upload('file'))
                {

                    $error = array('error' => $this->upload->display_errors());
                    $err =$error['error'];
                    $data = '<div class="alert alert-danger" role="alert"> <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>'.$err.'</div>';
                    $this->session->set_flashdata('msg',$data);
                    redirect("head-officer/others/add");

                }
                else
                {
                    $data = $this->upload->data();
                    $file_name =  $data['file_name'];
                    $result = $this->others_model->sendform($file_name);
                    if($result)
                    {
                        $emaillist = $this->input->post('optional');
                        $emails= $this->others_model->allemail($emaillist);
                        $email = $this->load->library('email');
                        $config['mailtype'] = "html";
                        $this->email->initialize($config);
                        $to_email = '';
                        foreach($emails as $email)
                        {
                            $to_email[] = $email->email;
                        }
                        $newemail =implode(',',$to_email);
                        $msg = 'Hi there , <br /><br /> A new Other documents has been uploaded by the Head Officer. <br /> Please Login to the site to view the documents. <br /><br />';
                        $msg .= 'Thanks!<br />Heifier International';
                        $this->email->from('info@heifierinternational.com', 'Heifier international');
                        $this->email->to($newemail);
                        $this->email->subject('Other Documents');
                        $this->email->message($msg);
                        $this->email->send();
                        $this->session->set_flashdata('msg','successfully added');
                        redirect("head-officer/others");
                    }
                }

            }
            $data['title'] = "Submit Other Documents";
            $data['region'] = $this->common_model->getTableData('regions')->result();
            $data ['usrhier'] =$this->others_model->getAll();
            $this->load->view('head-officer/addOther',$data);
        }

        function downloadDocuments($id)
        {
            $insertData['others_id'] = $id;
            $insertData['user_id'] = $this->session->userdata('logedin_id');
            $insertData['logs']= "Others Report Downloaded";
            $insertData['access_level'] = "Head Officer";
            $this->common_model->insertData('others_log',$insertData);

            $data = $this->common_model->getTableData('others',array('id'=>$id))->row();
            $this->load->helper('download');
            $content = file_get_contents(BASEPATH.'../others/'.$data->file);
            $name = $data->file;
            force_download($name,$content);

        }
    }