<?php

/**
 * Class Settings
 */
class Settings extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('common_model');
    }

    function index()
    {
        $data['title'] = "Organizational Settings";
        $data['content'] = $this->common_model->getTableData('site_settings')->row();
        if($this->input->post('submit')):
            $insertData['organization_name'] = $this->input->post('organizationName');
            $insertData['organization_tag_line'] = $this->input->post('organizationTagLine');
            $insertData['organization_address'] = $this->input->post('organizationAddress');
            $config['upload_path'] = BASEPATH.'../assets/logo/';
            $config['allowed_types'] = "jpg|jpeg|png";
            $config['max_size'] = 0;
            $this->load->library('upload',$config);
            if($_FILES['logo']['name'] !=''):
                if($this->upload->do_upload('logo')):
                    $file = $this->upload->data();
                    $file_name= $file['file_name'];
                else:
                    $this->session->set_flashdata('error','Image Uploading Failed. Please try again'.$this->upload->display_errors());
                    redirect('head-officer/settings');
                endif;
            else:
                $file_name = $this->input->post('image');
            endif;
        $insertData['logo'] = $file_name;
        $this->common_model->updateTableData('site_settings',$data['content']->id,$insertData);
        redirect('head-officer/settings');
            endif;

        $this->load->view('head-officer/settings',$data);
    }
}