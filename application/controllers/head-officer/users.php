<?php

class Users extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model("head-officer/users_model");
        $this->load->model("head-officer/projects_model");
        $this->load->model('common_model');
        $this->load->library('misc_lib');
        if (!$this->session->userdata('logged_in'))
            redirect('loginpage');
    }

    public function index() {
        
        $accs = $this->input->get("accesslevel");
        
        if($accs){
            $data["userList"] = $this->users_model->get_users_list_by_access($accs);
            $data["accessLvl"] = $accs;
        }else{
            $data["userList"] = $this->users_model->get_users_list();
            $data["accessLvl"] = Null;
        }
        
        $data["regnList"] = $this->users_model->get_regions();
        $data["accesslvl"] = $this->users_model->get_accesslvl();
        $this->load->view('head-officer/userlisting', $data);
    }

    public function add() {
        $data["regions"] = $this->users_model->get_regions();
        $data["accesslvl"] = $this->users_model->get_accesslvl();
        $data["sublvl"] = array('Manager','Officer','Other');
        $data["projects"] = $this->projects_model->get_projects();
        $this->load->view('head-officer/adduser_form', $data);
    }

    public function edit() {
        $data["userList"] = $this->users_model->get_users_list();
        $data["regions"] = $this->users_model->get_regions();
        $data["accesslvl"] = $this->users_model->get_accesslvl();
        $data["sublvl"] = array('Manager','Officer','Other');
        $data["projects"] = $this->projects_model->get_projects();
        $data["edituser"] = $this->users_model->get_single_user($this->input->get("id"));
        $data["userparent"] = $this->users_model->get_user_parent($this->input->get("id"));
        $this->load->view('head-officer/adduser_form', $data);
    }

    public function delete() {
        $id = $this->input->get("id");
        $subordinates = $this->users_model->has_subordinates($id);
        if ($subordinates) {
            $data["userList"] = $this->users_model->get_users_list();
            $data["userTree"] = $this->users_model->get_users_hierarchy();
            $this->load->view("head-officer/delete_confirmation", $data);
        } else {
            $data["success"] = $this->users_model->delete_user($id);
            if ($data["success"]) {
                $this->users_model->deleteUserParent($id);
                $this->projects_model->unassign_user($id);
            }
            $data["userList"] = $this->users_model->get_users_list();
            $data["regnList"] = $this->users_model->get_regions();
            $data["successMsg"] = "deleted";
            $this->load->view('head-officer/userlisting', $data);
        }
    }

    public function filterusers() {
        $reg = $this->input->post("reg");
        $acc = $this->input->post("acc");
        $data = $this->users_model->get_filtered_users($reg, $acc);
        $retData = '<option value="">Select an Officer</option>';
        foreach ($data as $user) {
            $retData .= '<option value="' . $user->ID . '">' . $user->name . '</option>';
        }
        echo $retData;
    }

    public function save() {
        $name = $this->input->post("username");
        $email = $this->input->post("useremail");
        $password = md5($this->input->post("userpass"));
        $level = $this->input->post("accesslevel");
        $sub_level = $this->input->post("sublevel");
        $region = $this->input->post("region") ? $this->input->post("region") : 0;
        $parentuser = $this->input->post("parentuser") ? $this->input->post("parentuser") : 0;
        $project = $this->input->post("userProject") ? $this->input->post("userProject") : 0;
        $active = 1;

        if ($level != "Head Officer") {
            if ($region == 0 || $region == '')
                $active = 0;
        }
        if ($level == "Program Manager") {
            if ($this->users_model->get_program_manager($region) > 0)
                $active = 0;
        }elseif ($level == "Program Officer") {
            if ($parentuser == 0 || $parentuser == '')
                $active = 0;
        }elseif ($level == "Project Holder") {
            if ($parentuser == 0 || $parentuser == '' || $project == 0 || $project == '')
                $active = 0;
        }

        $insertdata = array('name' => $name, 'email' => $email, 'password' => $password, 'region' => $region, 'accessLevel' => $level, 'sub_Level' => $sub_level,'active' => $active);
        $data["success"] = false;
        $data["success"] = $insertID = $this->users_model->addUser($insertdata);
        if ($insertID) {
            if ($parentuser > 0)
                $data["success"] = $this->users_model->addUserParent($parentuser, $insertID);
            if ($project > 0 && $level == "Project Holder")
                $data["success"] = $this->users_model->addUserProject($project, $insertID);
        }
        $data["userList"] = $this->users_model->get_users_list();
        $data["regnList"] = $this->users_model->get_regions();
        $data["successMsg"] = "added";
        $this->load->view('head-officer/userlisting', $data);
    }

    public function update() {
        $editID = $this->input->post("editid");
        $name = $this->input->post("username");
        $email = $this->input->post("useremail");
        $password = ($this->input->post("userpass") == '') ? $this->input->post("oldPass") : md5($this->input->post("userpass"));
        $level = $this->input->post("accesslevel");
        $region = $this->input->post("region") ? $this->input->post("region") : 0;
        $parentuser = $this->input->post("parentuser") ? $this->input->post("parentuser") : 0;
        $project = $this->input->post("userProject") ? $this->input->post("userProject") : 0;
        $active = 1;
        if ($project == '' || $project == 0)
            $active = 0;

        $insertdata = array('name' => $name, 'email' => $email, 'password' => $password, 'active' => $active);
        $data["success"] = false;
        $data["success"] = $insertID = $this->users_model->editUser($insertdata, $editID);
        if ($insertID) {
            $data["success"] = $this->projects_model->unassign_user($editID);
            $data["success"] = $this->projects_model->assign_user($project, $editID);
        }
        $data["userList"] = $this->users_model->get_users_list();
        $data["regnList"] = $this->users_model->get_regions();
        $data["successMsg"] = "updated";
        $this->load->view('head-officer/userlisting', $data);
    }

    public function delete_all() {
        $mainID = explode(",", $this->input->get("id"));
        foreach ($mainID as $id) {
            $data["success"] = $this->users_model->delete_user($id);
            if ($data["success"]) {
                $this->users_model->deleteUserParent($id);
                $this->projects_model->unassign_user($id);
            }
        }
        $data["userList"] = $this->users_model->get_users_list();
        $data["regnList"] = $this->users_model->get_regions();
        $data["successMsg"] = "deleted";
        $this->load->view('head-officer/userlisting', $data);
    }

    public function change_position() {
        $data["inactive"] = $this->users_model->get_inactive_users();
        $data["active"] = $this->users_model->get_active_users();

        $data["regions"] = $this->users_model->get_regions();
        $data["accesslvl"] = $this->users_model->get_accesslvl();
        $data["projects"] = $this->projects_model->get_projects();

        $this->load->view('head-officer/change_position_view', $data);
    }

    public function swap() {
        $newUser = $this->input->post("person1");
        $oldUser = $this->input->post("person2");
        $delete = $this->input->post("deletealso");

        $oldDetail = $this->common_model->getTableData('users', array('ID' => $oldUser))->row();
        $newUserDetail = $this->common_model->getTableData('users', array('ID' => $newUser))->row();
        $updateNewUser['region'] = $oldDetail->region;
        $updateNewUser['accessLevel'] = $oldDetail->accessLevel;
        $updateNewUser['active'] = 1;

        $this->common_model->updateTableData('users', $newUserDetail->ID, $updateNewUser);

        if ($delete == '1')
            $updateOldUser['active'] = -1;
        else
            $updateOldUser['active'] = 0;
        $this->common_model->updateTableData('users', $oldDetail->ID, $updateOldUser);

        if ($oldDetail->accessLevel == "Project Holder") {
            $oldLevel = $this->common_model->getTableData('user_hierarchy', array('user_id' => $oldDetail->ID))->row();
            if (!empty($oldLevel)) {
                $this->common_model->deleteTableData('user_hierarchy', array('user_id' => $oldDetail->ID));
                $userHierarchy['user_id'] = $newUserDetail->ID;
                $userHierarchy['parent_user_id'] = $oldLevel->parent_user_id;
                $this->common_model->insertData('user_hierarchy', $userHierarchy);
            }
        } else if ($oldDetail->accessLevel == "Program Officer") {
            $updateParentUser['parent_user_id'] = $newUserDetail->ID;
            //$this->common_model->updateTableData('user_hierarchy',array('parent_user_id'=>$oldDetail->ID),$updateParentUser);
            $this->users_model->UpdateUserHierarchyParent($oldDetail->ID, $updateParentUser);
            $updateUserId['user_id'] = $newUserDetail->ID;
            //$this->common_model->updateTableData('user_hierarchy',array('user_id'=>$oldDetail->ID),$updateUserId);
            $this->users_model->UpdateUserHierarchyUser($oldDetail->ID, $updateUserId);
        } else if ($oldDetail->accessLevel == "Program Manager") {
            $updateParentUser['parent_user_id'] = $newUserDetail->ID;
            $this->users_model->UpdateUserHierarchyParent($oldDetail->ID, $updateParentUser);
        }

        $this->session->set_flashdata('swap', 'User Swapped Successfully.');
        redirect('head-officer/users/change_position');
    }

    public function assign() {
        $userId = $this->input->post('person1');
        $updateData['accessLevel'] = $this->input->post('accesslevel');
        if ($updateData['accessLevel'] != "Head Officer") {
            $updateData['region'] = $this->input->post('region');
            if ($updateData['accessLevel'] != "Program Manager") {
                $parentUser = $this->input->post('parentuser');
                if ($updateData['accessLevel'] != "Program Officer")
                    $project = $this->input->post('userProject');
            }
        }
        if ($updateData['accessLevel'] == "Program Manager") {
            $available = $this->common_model->getTableData('users', array('accessLevel' => "Program Manager", 'region' => $updateData['region']))->row();
            if (!empty($available)) {
                $data['parent_user_id'] = $userId;
                $this->users_model->UpdateUserHierarchyParent($available->ID, $data);

                $updateUser['active'] = 0;
                $this->common_model->updateTableData('users', $available->ID, $updateUser);
            }
        } else if ($updateData['accessLevel'] == "Program Officer" || $updateData['accessLevel'] == "Project Holder") {
            $insertData['user_id'] = $userId;
            $insertData['parent_user_id'] = $parentUser;
            $this->common_model->insertData('user_hierarchy', $insertData);
            if ($updateData['accessLevel'] == "Project Holder") {
                $projectAssign['assign_to_project_holder'] = $userId;
                $this->common_model->updateTableData('projects', $project, $projectAssign);
            }
        }

        $updateData['active'] = 1;
        $this->common_model->updateTableData('users', $userId, $updateData);

        $this->session->set_flashdata('assign', 'User Position Assigned Successfully.');
        redirect('head-officer/users/change_position');
    }

    public function assign_as() {
        $newUser = $this->input->post("person1");
        $oldUser = $this->input->post("person2");

        $oldDetail = $this->common_model->getTableData('users', array('ID' => $oldUser))->row();
        
        $newUserDetail = $this->common_model->getTableData('users',array('ID'=>$newUser))->row();
        $updateNewUser['region'] = $oldDetail->region;
        $updateNewUser['accessLevel'] = $oldDetail->accessLevel;
        $updateNewUser['active'] = 1;

        $this->common_model->updateTableData('users', $newUserDetail->ID, $updateNewUser);

        if ($oldDetail->accessLevel == "Project Holder") {
            $userHierarchy = $this->common_model->getTableData('user_hierarchy', array('user_id' => $oldUser))->row();
            $userHierarchy->user_id = $newUser;
            $newData = (array) $userHierarchy;
            $this->common_model->insertData('user_hierarchy', $newData);
        } else {
            $userParentHierarchy = $this->common_model->getTableData('user_hierarchy', array('parent_user_id' => $oldUser))->result();
            if (count($userParentHierarchy) > 0) {
                foreach ($userParentHierarchy as $uph) {
                    $uph->parent_user_id = $newUser;
                    $newData = (array) $uph;
                    $this->common_model->insertData('user_hierarchy', $newData);
                }
            }
            $userHierarchy = $this->common_model->getTableData('user_hierarchy', array('user_id' => $oldUser))->result();
            if (count($userHierarchy) > 0) {
                foreach ($userHierarchy as $up) {
                    $up->user_id = $newUser;
                    $newData = (array) $up;
                    $this->common_model->insertData('user_hierarchy', $newData);
                }
            }
        }

        $this->session->set_flashdata('swap', 'User Assigned Successfully.');
        redirect('head-officer/users/change_position');
    }

    public function view_hierarchy() {
        $data["userparent"] = $this->users_model->getSubordianates($this->input->get("id"));
        $this->load->view('head-officer/view_hierarchy', $data);
    }
}

?>