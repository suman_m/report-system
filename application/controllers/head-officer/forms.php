<?php

class Forms extends CI_Controller{

    function __construct()
    {
        parent::__construct();
        $this->load->model('common_model');
        if(!$this->session->userdata('logged_in'))
            redirect('loginpage');
        $this->load->model('form_model');
    }

    public  function index()
    {
        $data['form'] = $this->form_model->getallforms();
        $this->load->view('head-officer/formsdashboard',$data);
    }
    public function add()
    {
        if($this->input->post('submitBtn'))
        {


            $form = $_FILES['file']['name'];
            $config['upload_path'] = BASEPATH.'../formformat/';
            $config['allowed_types'] = "pdf|doc|docx|xls|xlsx";
            $config['max_size'] = 0;
            $config['encrypt_name'] = TRUE ;
            $this->load->library('upload',$config);

            if ( ! $this->upload->do_upload('file'))
            {

                $error = array('error' => $this->upload->display_errors());
                $err =$error['error'];
                $data = '<div class="alert alert-danger" role="alert"> <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>'.$err.'</div>';
                $this->session->set_flashdata('msg',$data);
                redirect("head-officer/forms/add");

            }
            else
            {
                $data = $this->upload->data();
                $file_name =  $data['file_name'];
                $result = $this->form_model->sendform($file_name);
                if($result)
                {
                    $emaillist = $this->input->post('optional');
                    $emails= $this->form_model->allemail($emaillist);
                    $email = $this->load->library('email');
                    $to_email = '';
                    foreach($emails as $email)
                    {
                        $to_email[] = $email->email;
                    }
                    $newemail =implode(',',$to_email);
                    $this->email->from('your@example.com', 'Your Name');
                    $this->email->to('');
                    $this->email->cc($newemail);
                    $this->email->subject('Email Test');
                    $this->email->message('Testing the email class.');
                    $this->email->send();
                    echo $this->email->print_debugger();
                    $this->session->set_flashdata('msg','successfully added');
                    redirect("head-officer/forms");
                }

            }

        }
        $data['region'] = $this->common_model->getTableData('regions')->result();
        $data ['usrhier'] =$this->form_model->getAll();
        $this->load->view("head-officer/formsandformat",$data);
    }

    public function history()
    {
        if($this->input->post('submitBtn'))
        {
            $data['history'] = $this->form_model->search();

        }
        else
        {
            $id = $this->uri->segment(4);
            $data['history'] =$this->form_model->gethistory($id);
        }
        $data['region']  =$this->form_model->regions();
        $this->load->view('head-officer/forms_format_history',$data);
    }
}