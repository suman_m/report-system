<?php

class Projects extends CI_Controller {

    public $data = '';

    function __construct(){
        parent::__construct();
        $this->load->model("head-officer/projects_model");
        if(!$this->session->userdata('logged_in'))
            redirect('loginpage');
        $this->load->model('common_model');
        $this->load->library('misc_lib');
        $this->load->model("head-officer/users_model");
    }

    public function index(){
        $this->data["projects"] = $this->projects_model->get_projects();
        $this->data["projectHolders"] = $this->projects_model->get_project_holders();
        $this->data['logs'] = $this->common_model->getTableData('projects_logs')->result();
        $this->load->view("head-officer/projectlisting",$this->data);
    }

    public function add(){
        $data["regions"] = $this->users_model->get_regions();
        $data['projectHolder'] = $this->common_model->getTableData('users',array('active'=>'1','accessLevel'=>"Project Holder"),"","","","","",array('name','asc'))->result();
        $data['projectManager'] = $this->common_model->getTableData('users',array('active'=>'1','accessLevel'=>"Program Manager"))->result();
        $data['projectOfficer'] = $this->common_model->getTableData('users',array('active'=>'1','accessLevel'=>"Program Officer"))->result();
        $data["projectInfo"] =Null;
        $this->load->view("head-officer/addproject_form",$data);
    }

    public function save(){
        if($this->projects_model->check_project_exist($this->input->post("projectName"))){
            $this->data["success"] = 1;
            $this->data["successMsg"] = "Project with this name already exist.";
            $this->data["successType"] = "danger";
        }else{
            $postData['number'] = $this->input->post("projectNumbaer");
            $postData['name'] = $this->input->post("projectName");
            $postData['description'] = $this->input->post("description");
            $postData['program'] = $this->input->post("projectProgram");
            $postData['assign_to_project_holder'] = $this->input->post("projectHolder");
            $postData['project_manager'] = $this->input->post("projectManager");
            $postData['project_officer'] = $this->input->post("projectOfficer");
            $postData['district'] = $this->input->post("district");
            $postData['region'] = $this->input->post("region");
            
            $this->data["success"] = $this->projects_model->add_project($postData);
            $this->data["successMsg"] = "Project added successfully.";
            $this->data["successType"] = "success";
        }
        $this->index();
    }

    public function edit(){
        $this->data["regions"] = $this->users_model->get_regions();
        $this->data['projectHolder'] = $this->common_model->getTableData('users',array('active'=>'1','accessLevel'=>"Project Holder"),"","","","","",array('name','asc'))->result();
        $this->data['projectManager'] = $this->common_model->getTableData('users',array('active'=>'1','accessLevel'=>"Program Manager"))->result();
        $this->data['projectOfficer'] = $this->common_model->getTableData('users',array('active'=>'1','accessLevel'=>"Program Officer"))->result();
        $this->data["projectInfo"] = $this->common_model->get_one_value_by_id($this->input->get("id"),'projects');
        $this->load->view("head-officer/addproject_form", $this->data);
    }

    public function update(){
        if($this->projects_model->check_project_exist($this->input->post("projectName"), $this->input->post("editid"))){
            $postData['number'] = $this->input->post("projectNumber");
            $postData['name'] = $this->input->post("projectName");
            $postData['description'] = $this->input->post("description");
            $postData['program'] = $this->input->post("projectProgram");
            $postData['assign_to_project_holder'] = $this->input->post("projectHolder");
            $postData['project_manager'] = $this->input->post("projectManager");
            $postData['project_officer'] = $this->input->post("projectOfficer");
            $postData['district'] = $this->input->post("district");
            $postData['region'] = $this->input->post("region");
            $this->data["success"] = $this->projects_model->edit_project($postData, $this->input->post("editid"));
            $this->data["successMsg"] = "Project updated successfully.";
            $this->data["successType"] = "success";
        }
        $this->index();
    }

    public function assign(){
        $this->load->model("head-officer/users_model");
        $pid = $this->input->post("projectName");
        $uid = $this->input->post("holderName");
        if($uid == 0 && $pid != 0){
            $project = $this->projects_model->get_single_project($pid);
            $this->users_model->editUser(array('active' => 0), $project[0]->assign_to_project_holder);
        }
        if($pid == 0 && $uid != 0){
            $this->users_model->editUser(array('active' => 0), $uid);
        }
        if($pid != 0 && $uid != 0){
            $this->users_model->editUser(array('active' => 1), $uid);
        }
        if($pid == 0)
            $this->data["success"] = $this->projects_model->unassign_user($uid);
        else
            $this->data["success"] = $this->projects_model->assign_user($pid, $uid);
        if($uid == 0 || $pid ==0)
            $this->data["successMsg"] = "Project is un-assign to the user successfully.";
        else
            $this->data["successMsg"] = "Project is assign to the user successfully.";
        $this->data["successType"] = "success";
        $this->index();
    }

    function assignProject($id)
    {
        $data['title'] = "Assign Project To Holder";
        $data['projectTitle'] = $this->common_model->getTableData('projects',array('id'=>$id))->row()->name;
        $data['projectHolder'] = $this->common_model->getTableData('users',array('active'=>'1','accessLevel'=>"Project Holder"))->result();
        $available = $this->common_model->getTableData('project_holder_detail',array('project_id'=>$id))->row();
        if($this->input->post('submit'))
        {
            $insertData['user_id'] = $this->input->post('projectHolder');
            if(!empty($available))
                $this->common_model->updateTableData('project_holder_detail',$available->id,$insertData);
            else
            {
                $insertData['project_id'] = $id;
                $this->common_model->insertData('project_holder_detail',$insertData);
            }
            $this->session->set_flashdata('success','Project Assigned to the Project Holder Successfully.');
            redirect('head-officer/projects');
        }
        if(!empty($available))
            $data['projectInfo'] = $available;
        $this->load->view('head-officer/assignProject',$data);
    }

    function delete($id)
    {
        $this->common_model->deleteTableData('projects',array('id'=>$id));
        $this->common_model->deleteTableData('project_holder_detail',array('project_id'=>$id));
        redirect('head-officer/projects');
    }
}
?>