<?php
    class Backup extends CI_Controller
    {
        function __construct()
        {
            parent::__construct();
            $this->load->model('common_model');
            $this->load->library('misc_lib');
            $this->load->model('backup_model');
            $this->load->library('email_notification');
            if(!$this->session->userdata('logged_in'))
                redirect('loginpage');
        }

        function index()
        {
            $data['title'] = "Backup Management";
            $data['value'] = $this->common_model->getTableData('report_settings',array('title'=>"BACKUP SETTINGS"))->row();
            $data['backupData'] = $this->common_model->getTableData('backup')->result();
            $data['backupLogs'] = $this->common_model->getTableData('backup_logs')->result();
            $data['regions'] = $this->common_model->getTableData('regions')->result();
            $currentMonth = date('n');
            $lastTwoMonth = $currentMonth - 2;
            $date = date('Y').'-'.$lastTwoMonth.'-1';
            $lastTwoMonthDate = date('Y-m-d h:i:s',strtotime($date));
            if($this->input->post('search'))
            {
                $data['region'] = $this->input->post('region');
                $data['month'] = $this->input->post('month');
                $value = $this->backup_model->getSearchResult($data);
                $data['backupData'] = $value;
            }
            else
                $data['backupData'] = $this->common_model->getTableData('backup')->result();
            $data['months'] = $lastTwoMonth;
            if($this->input->post('submit'))
            {
                $backupType = $this->input->post('backupName');
                $type = array();
                $i = 1;
                foreach($backupType as $bt):
                    if($i==1)
                        $type['first'] = $bt;
                    else if($i == 2)
                        $type['second'] = $bt;
                    $i++;
                    endforeach;
                $insertData['content'] = json_encode($type);
                $insertData['report_submission_time'] = $this->input->post('backupSubmissionTime');
                if($insertData['report_submission_time']=="quarterly")
                    $insertData['quarterly_start'] = $this->input->post('quarter_start_time');
                else
                    $insertData['quarterly_start'] = "";
                $insertData['submission_end'] = $this->input->post('report_submission_end');
                $insertData['email_frequency'] = $this->input->post('email_frequency');
                if($this->input->post('status'))
                    $insertData['status'] = $this->input->post('status');
                else
                    $insertData['status'] = '0';
                $content = $this->common_model->getTableData('report_settings',array('title'=>'BACKUP SETTINGS'))->row();
                if(empty($content))
                {
                    $insertData['title'] = 'BACKUP SETTINGS';
                    $this->common_model->insertData('report_settings',$insertData);
                }
                else
                {
                    $this->common_model->updateTableData('report_settings',$content->id,$insertData);
                }
                redirect('head-officer/backup');
            }
            $this->load->view('head-officer/backup',$data);
        }

        function acceptBackup()
        {
            $id = $_POST['id'];
            $updateData['status'] = 1;
            $this->common_model->updateTableData('backup',$id,$updateData);

            $logMessage['user_id'] = $this->session->userdata('logedin_id');
            $logMessage['backup_id'] = $id;
            $logMessage['logs'] = "Backup Accepted";
            $logMessage['access_level'] = $this->session->userdata('access_level');
            $this->common_model->insertData('backup_logs',$logMessage);
            $this->sentNotificationEmail('accept',$id);
        }

        function rejectBackup()
        {
            $id = $_POST['id'];
            $updateData['status'] = 2;
            $this->common_model->updateTableData('backup',$id,$updateData);

            $logMessage['user_id'] = $this->session->userdata('logedin_id');
            $logMessage['backup_id'] = $id;
            $logMessage['logs'] = "Backup Rejected";
            $logMessage['access_level'] = $this->session->userdata('access_level');
            $this->common_model->insertData('backup_logs',$logMessage);
            $this->sentNotificationEmail('reject',$id);
        }

        function downloadBackup($id,$type)
        {
            $data = $this->common_model->getTableData('backup',array('id'=>$id))->row();
            $file  = json_decode($data->files);
            $logMessage['user_id'] = $this->session->userdata('logedin_id');
            $logMessage['backup_id'] = $id;
            $logMessage['logs'] = "Backup Downloaded";
            $logMessage['access_level'] = $this->session->userdata('access_level');
            $this->common_model->insertData('backup_logs',$logMessage);
            foreach($file as $key=>$fi):
                if($type == $key):
                    $this->load->helper('download');
                    $data = file_get_contents(BASEPATH.'../backup/'.$fi);
                    $name = $fi;
                    force_download($name,$data);
            endif;
                endforeach;
        }

        function sentNotificationEmail($type,$id)
        {
            $userId = $this->common_model->getTableData('backup',array('id'=>$id))->row()->user_id;
            $emailList = $this->email_notification->getEmailForOfficer($userId);
            $msg = $this->email_notification->getBackupMessage($type,$id);
            $this->load->library('email');
            $config['mailtype'] = "html";
            $this->email->initialize($config);
            foreach($emailList as $email):
                $this->email->from('info@heifierinternational.com','Heifer International');
                $this->email->to($email);
                $this->email->subject('Backup Status');
                $this->email->message($msg);
                $this->email->send();
                $this->email->clear();
            endforeach;
        }

        function getSubmissionList()
        {
            $month = $_POST['month'];
            $data = $this->common_model->getTableData('backup',array('month'=>$month))->result();
            if(!empty($data)):
                $content = '<table class="table table-striped ">
                    <tr>
                        <th>SN</th>
                        <th>Submitted By</th>
                        <th>Backup of The Month</th>
                        <th>Submitted Date</th>
                    </tr>';
                $i = 1;
                foreach($data as $d):
                    $content .='
                        <tr>
                            <td>'.$i.'</td>
                            <td>'.$this->misc_lib->getUserName($d->user_id).'</td>
                            <td>'.date('F',strtotime('01-'.$d->month.'-'.date('Y'))).'</td>
                            <td>'.$d->date.'</td>
                        </tr>
                    ';
                    $i++;
                endforeach;
                echo $content;
            else:
                echo "No Backup Submission Available for the month";
            endif;
        }

        function downloadZip()
        {
            $month = $_POST['month'];
            $region = $_POST['region'];
            if($month == 0 && $region == 0):
                $this->session->set_flashdata('month','Choose the month to download the zip');
                redirect('head-officer/backup');
            endif;
            //$mon = date('m',strtotime('01-'.$month.'-2014'));
            $data = $this->backup_model->getDownloadData($month,$region);
            $userData = $this->common_model->getTableData('users',array('ID'=>$this->session->userdata('logedin_id')))->row();
            if(empty($data))
            {
                $this->session->set_flashdata('month','No reports Available for the month of '.date('F',strtotime('01-'.$month.'-2014')));
                redirect('head-officer/backup');
            }
            else
            {
                $innerFileName =BASEPATH.'../backupZip/'.date('F',strtotime('01-'.$month.'-2014'));
                if(!is_dir($innerFileName))
                    mkdir($innerFileName);
                foreach($data as $d):
                    $getUser = $this->common_model->getTableData('users',array('ID'=>$d->user_id))->row();
                    $region = $this->common_model->getTableData('regions',array('id'=>$getUser->region))->row();
                    $userFile = $innerFileName.'/'.$this->slugify($getUser->name).'-'.$getUser->ID.'-'.$this->slugify($region->name);
                    if(!is_dir($userFile))
                        mkdir($userFile);
                    $files = json_decode($d->files);
                    foreach($files as $f):
                        @copy(BASEPATH.'../reports/'.$f , $userFile.'/'.$f);
                        $files_to_zip[] = $userFile.'/'.$f;
                    endforeach;
                endforeach;
                $this->create_zip($files_to_zip,date('F',strtotime('01-'.$month.'-2014')).'.zip');
                $this->load->helper('download');
                $data = file_get_contents(date('F',strtotime('01-'.$month.'-2014')).'.zip');
                $name = date('F',strtotime('01-'.$month.'-2014')).'.zip';
                if(file_exists($name))
                    @unlink($name);
                force_download($name,$data);
            }
        }

        public function slugify($text)
        {
            $text = preg_replace('~[^\\pL\d]+~u', '-', $text);
            $text = trim($text, '-');
            $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);
            $text = strtolower($text);
            $text = preg_replace('~[^-\w]+~', '', $text);
            if (empty($text))
            {
                return 'n-a';
            }
            return $text;
        }

        function create_zip($files = array(),$destination = '',$overwrite = false) {
            //if the zip file already exists and overwrite is false, return false
            if(file_exists($destination) && !$overwrite) { return false; }
            //vars
            $valid_files = array();
            //if files were passed in...
            if(is_array($files)) {
                //cycle through each file
                foreach($files as $file) {
                    //make sure the file exists
                    if(file_exists($file)) {
                        $valid_files[] = $file;
                    }
                }
            }
            //if we have good files...
            if(count($valid_files)) {
                //create the archive
                $zip = new ZipArchive();
                if($zip->open($destination,$overwrite ? ZIPARCHIVE::OVERWRITE : ZIPARCHIVE::CREATE) !== true) {
                    return false;
                }
                //add the files
                foreach($valid_files as $file) {
                    $zip->addFile($file,$file);
                }
                //debug
                //echo 'The zip archive contains ',$zip->numFiles,' files with a status of ',$zip->status;

                //close the zip -- done!
                $zip->close();

                //check to make sure the file exists
                return file_exists($destination);
            }
            else
            {
                return false;
            }
        }
    }