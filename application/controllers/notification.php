<?php
class Notification extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('common_model');
        $this->load->library('email');
    }

    function index()
    {
        $data = $this->common_model->getTableData('report_settings',array('title'=>"REPORT SETTINGS"))->row();
        if($data->report_submission_time=="quarterly")
        {
            $start_time = date('Y-m-d',strtotime($data->quarterly_start));
            $start_time = new DateTime($start_time);
            $current_time = date('Y-m-d');
            $current_time = new DateTime($current_time);
            $interval = date_diff($start_time, $current_time);
            $months = $interval->format('%m');
            if($months % 2==0)
            {
                if($data->submission_end=="last-day")
                {
                    $lastSubmissionDate =  date('Y-m-t');
                }
                else if($data->submission_end="last-friday")
                {
                    $month = date('F');
                    $year = date('Y');
                    $lastSubmissionDate =  date('Y-m-d',strtotime('last friday of '.$month.' '.$year));
                }
                if(isset($lastSubmissionDate))
                {
                    $currentDate = date('Y-m-d');

                    if(strtotime($currentDate)<=strtotime($lastSubmissionDate))
                    {
                        $currentDate = new DateTime($currentDate);
                        $lastSubmissionDate = new DateTime($lastSubmissionDate);
                        $interval = $currentDate->diff($lastSubmissionDate);
                        $daysInterval = $interval->days;
                        if($daysInterval<=$data->email_frequency)
                        {
                            $this->finalConfirmationForNotification();
                        }
                    }
                    else if(strtotime($currentDate)>strtotime($lastSubmissionDate))
                    {
                        $this->sendDatePassedNotification();
                    }

                }
            }
        }
        else if($data->report_submission_time=="monthly")
        {
            if($data->submission_end=="last-day")
            {
                $lastSubmissionDate =  date('Y-m-t');
            }
            else if($data->submission_end="last-friday")
            {
                $month = date('F');
                $year = date('Y');
                $lastSubmissionDate =  date('Y-m-d',strtotime('last friday of '.$month.' '.$year));
            }
            else if($data->submission_end=="next-month")
            {
                $m = date('n');
                $m = $m + 1;
                $curDate = date('Y-m-d',strtotime(date('Y').'-'.$m.'-05'));
                $lastSubmissionDate = date('Y-m-d',strtotime($curDate));
            }
            if(isset($lastSubmissionDate))
            {
                $currentDate = date('Y-m-d');

                if(strtotime($currentDate)<=strtotime($lastSubmissionDate))
                {
                    $currentDate = new DateTime($currentDate);
                    $lastSubmissionDate = new DateTime($lastSubmissionDate);
                    $interval = $currentDate->diff($lastSubmissionDate);
                    $daysInterval = $interval->days;
                    if($daysInterval<=$data->email_frequency)
                    {
                        $this->finalConfirmationForNotification();
                    }
                }
            }
        }
    }

    function finalConfirmationForNotification()
    {
        $userId = array();
        $reports = $this->common_model->getTableData('reports')->result();
        foreach($reports as $re):
            if(date('F', strtotime($re->date)) == date('F'))
            {
                $userId[] = $re->user_id;
            }
        endforeach;
        $users = $this->common_model->getTableData('users',array('accessLevel'=>'Project Holder'))->result();
        foreach($users as $use):
            if(!empty($userId))
            {
                $this->load->library('email');
                $config['mailtype'] = 'html';
                $this->email->initialize($config);
                $msg = "Hi there, <br />";
                $msg .= "This is an automated email to notify that the report submission time is nearby. Please don't forget to send the reports before the submission time exceeds.<br /><br />";
                $msg .="Thanks!<br />Heifier International";
                if(!in_array($use->ID,$userId))
                {

                    $this->email->from('info@heifierinterantional.org','Heifer International');
                    $this->email->to($use->email);
                    $this->email->subject('Report Submission Time Nearby');
                    $this->email->message($msg);
                    $this->email->send();
                    $this->email->clear();
                }
            }
            endforeach;
    }

    function sendDatePassedNotification()
    {
        $reports = $this->common_model->getTableData('reports')->result();
        $reportSettings = $this->common_model->getTableData('report_settings',array('title'=>"REPORT SETTINGS"))->row();
        if($reportSettings->report_submission_time=="quarterly")
        {
            $start_time = date('Y-m-d',strtotime($reportSettings->quarterly_start));
            $start_time = new DateTime($start_time);
            $current_time = date('Y-m-d');
            $current_time = new DateTime($current_time);
            $interval = date_diff($start_time, $current_time);
            $months = $interval->format('%m');
        }
        else if($reportSettings->report_submission_time=="monthly")
        {

        }

    }

    function datePassedForReportSubmission()
    {
        $reportSettings = $this->common_model->getTableData('report_settings',array('title'=>"REPORT SETTINGS"))->row();
        if($reportSettings->report_submission_time=="monthly")
        {
            $month = date('n');
            $day = date('d');
            if($day >5){
                echo "test";
                $month = $month - 1;
                $reportsData = $this->common_model->getTableData('reports',array('month'=>$month))->result();
            }
            else
            {
                $reportsData = array();
            }
            var_dump($reportsData);die;
        }
        else if($reportSettings->report_submission_time=="quarterly")
        {
            $start_time = date('Y-m-d',strtotime($reportSettings->quarterly_start));
            $start_time = new DateTime($start_time);
            $current_time = date('Y-m-d');
            $current_time = new DateTime($current_time);
            $interval = date_diff($start_time, $current_time);
            $months = $interval->format('%m');
            if($months % 3==0)
            {
                $m = date('n');
                $m = $m - 1;
                $reportsData = $this->common_model->getTableData('reports',array('month'=>$m))->result();
            }
            else
            {
                $reportsData = array();
            }
        }
        if(!empty($reportsData))
        {
            $userId = array();
            foreach($reportsData as $rd):
                $userId[] = $rd->user_id;
            endforeach;
            $usersDetail = $this->common_model->getTableData('users',array('accessLevel'=>"Project Holder",'active'=>'1'))->result();
            $userEmail = array();
            foreach($usersDetail as $ud):
                if(!in_array($ud->ID,$userId))
                    $userEmail[] = $ud->email;
            endforeach;
            if(!empty($userEmail)):
                $this->sendEmailForDatePassed($userEmail);
                endif;
        }
    }

    function sendEmailForDatePassed($userEmail = array())
    {
        $programOfficer = $this->getTopLevelUser($userEmail);
        $this->load->library('email');
        $config['mailtype'] = "html";
        $this->email->initialize($config);
        $msg = "Hi there, <br /><br />";
        $msg .= "This is an automated email to remaind that you have not sent the reports of the previous month. Please login to the site to submit your report immediately.<br /><br />";
        $msg .="Thanks!<br />Heifier International";
        //sending email for the project holders
        foreach($userEmail as $email):
            $this->email->from('noreply@heifierinternational.org','Heifier International');
            $this->email->to($email);
            $this->email->subject('Report Submission Time exceeds');
            $this->email->message($msg);
            $this->email->send();
            $this->email->clear();
        endforeach;

        //sending email for the program manager
        $msg = "Hi there, <br /><br />";
        $msg .= "This is an automated email to remaind that some of the project holder under you have not submitted their reports of the previous month.<br /><br />";
        $msg .="Thanks!<br />Heifier International";
        foreach($programOfficer as $email):
            $this->email->from('noreply@heifierinternational.org','Heifier International');
            $this->email->to($email);
            $this->email->subject('Report Submission Time exceeds');
            $this->email->message($msg);
            $this->email->send();
            $this->email->clear();
        endforeach;

    }

    function getTopLevelUser($userEmail = array())
    {
        if(!empty($userEmail)):
            $topUserEmail = array();
            foreach($userEmail as $email):
                $userId = $this->common_model->getTableData('users',array('email'=>$email))->row()->ID;
                $topUserId = $this->common_model->getTableData('user_hierarchy',array('user_id'=>$userId))->row();
                $topUserDetail = $this->common_model->getTableData('users',array('ID'=>$topUserId->parent_user_id))->row();
                if(!in_array($topUserDetail->email,$topUserEmail))
                    $topUserEmail[] = $topUserDetail->email;
            endforeach;
            return $topUserEmail;
        endif;
    }
}