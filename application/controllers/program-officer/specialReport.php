<?php
    class SpecialReport extends CI_Controller
    {
        function __construct()
        {
            parent::__construct();
            if(!$this->session->userdata('logged_in'))
                redirect('loginpage');
            $this->load->model('common_model');
            $this->load->model('special_report_model','special');
            $this->load->library('misc_lib');
            $this->load->library('email_notification');
        }

        function index()
        {
            $data['title'] = "Special Reports";
            $data['specialReport'] = $this->special->getSpecialReport();
            $reportId = array();
            foreach($data['specialReport'] as $b):
                $reportId[] = $b->id;
            endforeach;
            $data['logs'] = $this->special->getSpecialReportLogs($reportId)->result();
            $this->load->view('program-officer/specialReport',$data);
        }
    }