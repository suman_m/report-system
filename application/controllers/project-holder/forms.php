<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 7/15/14
 * Time: 2:08 PM
 */

Class Forms extends CI_Controller{

    function __construct()
    {
        parent::__construct();
        if(!$this->session->userdata('logged_in'))
            redirect('loginpage');
        $this->load->model('form_model');

    }
    public function index()
    {
        if($this->input->post('submitBtn'))
        {

            $this->load->helper('download');
            $file = $this->input->post('act');
            $this->form_model->formsformatalllog();
            $name =$file;
            if(file_exists(BASEPATH.'../policies/'.$file)):
            $response = file_get_contents(BASEPATH.'../formformat/'.$file); // Read the file's contents - cannot use relative path. It will try to call the file from same directory (controller) as this file. In order for it get the contents from the root folder put a . in front of the relative path//
                else:
                redirect('project-holder/forms');
                    endif;
            force_download($name, $response);
            redirect("/");
        }
        $id = $this->session->userdata('logedin_id');
        $data['form'] = $this->form_model->getfrom($id);
        $this->load->view('project-holder/formsandformat',$data);
    }
    public function count()
    {

        $data = $this->form_model->counteralert();
        if($data>0)
        {
            echo '1';
        }
        else
        {
            echo "0";
        }
        exit;
    }

    public function history()
    {
        echo "To DO";
    }
}