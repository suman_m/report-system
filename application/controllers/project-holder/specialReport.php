<?php
    class SpecialReport extends CI_Controller
    {
        function __construct()
        {
            parent::__construct();
            if(!$this->session->userdata('logged_in'))
                redirect('loginpage');
            $this->load->model('common_model');
            $this->load->model('special_report_model','special');
            $this->load->library('email_notification');
            $this->load->library('misc_lib');
        }

        function index()
        {
            $data['title'] = "Manage Special Report";
            $data['specialData'] = $this->common_model->getTableData('special_report',array('user_id'=>$this->session->userdata('logedin_id')))->result();
            $data['logs'] = $this->special->getLogs();
            $this->load->view('project-holder/specialReport',$data);
        }

        function submitReport()
        {
            $data['title'] = "Submit Special Report";
            $data['specialReport'] = $this->special->formsFormatData();
            if($this->input->post('submit')):
                $insertData['title'] = $this->input->post('reportTitle');
                $insertData['special_notes'] = $this->input->post('special_notes');
                $insertData['form_id'] = $this->input->post('form_id');
                $available = $this->common_model->getTableData('special_report',array('form_id'=>$insertData['form_id'],'user_id'=>$this->session->userdata('logedin_id')))->row();
                if(!empty($available)):
                    $this->session->set_flashdata('error','You have already submitted the special report.');
                redirect('project-holder/specialReport/submitReport');
                    endif;
                $config['upload_path'] = BASEPATH.'../specialReport/';
                $config['allowed_types'] = "pdf|doc|docx|xls|xlsx";
                $config['max_size'] = 0;
                $this->load->library('upload',$config);
                if($this->upload->do_upload('specialReport'))
                {
                    $file = $this->upload->data();
                    $fileName = $file['file_name'];

                }
                else
                {
                    echo $this->upload->display_errors();
                }
                $insertData['files'] = $fileName;
                $insertData['user_id'] = $this->session->userdata('logedin_id');
            $id = $this->common_model->insertData('special_report',$insertData);

                $logsMessage['user_id'] = $this->session->userdata('logedin_id');
                $logsMessage['special_report_id'] = $id;
                $logsMessage['logs'] = "Special Report Submitted";
                $logsMessage['access_level'] = $this->session->userdata('access_level');
                $this->common_model->insertData('special_report_logs',$logsMessage);

                //sending the email notification
                $emailList = $this->email_notification->getEmailForHolder($this->session->userdata('logedin_id'));
                $userData = $this->common_model->getTableData('users',array('id'=>$this->session->userdata('logedin_id')))->row();
                $this->load->library('email');
                $msg = "Hi there, <br />";
                $msg .= $userData->name.'('.$userData->accessLevel.') has submitted the special report.<br />';
                $msg .= 'Please <a href="'.site_url('loginpage').'">Login</a> here to view the backup data <br /><br />';
                $msg .="Thanks!<br />Heifier International";
                $config['mailtype'] = "html";
                $this->email->initialize($config);
                foreach($emailList as $email):
                    $this->email->from('adiksudip@gmail.com','Heifer International');
                    $this->email->to($email);
                    $this->email->subject('Special Report Submitted');
                    $this->email->message($msg);
                    $this->email->send();
                    $this->email->clear();
                endforeach;
            $this->session->set_flashdata('success','Report Submitted Successfully.');
            redirect('project-holder/specialReport/');
            endif;
            $this->load->view('project-holder/submitSpecialReport',$data);
        }
    }