<?php
    class Reports extends CI_Controller
    {
        function __construct()
        {
            parent::__construct();
            if(!$this->session->userdata('logged_in'))
                redirect('loginpage');
            $this->load->model('common_model');
            $this->load->model('reports_model');
            $this->load->library('misc_lib');
            $this->load->library('email_notification');
        }

        function index()
        {
            $data['title'] = "Report Management";
            $data['reportType'] = $this->common_model->getTableData('report_category')->result();
            $data['reports'] = $this->common_model->getTableData('reports',array('user_id'=>$this->session->userdata('logedin_id')))->result();
            $data['reportStatus'] = $this->common_model->getTableData('report_status')->result();
            $data['reportNotification'] = $this->misc_lib->getReportNotificationStatus();
            $data['reportLogs'] = $this->reports_model->getReportLogs();
            $report_id = '';
            foreach($data['reports'] as $reports):
                $report_id .= $reports->id.',';
                endforeach;
            $report_id = trim($report_id,',');
            //$data['reportLogs'] = $this->reports_model->getReportLogs($report_id);
            $this->load->view('project-holder/reports',$data);
        }

        public function slugify($text)
        {
            $text = preg_replace('~[^\\pL\d]+~u', '-', $text);
            $text = trim($text, '-');
            $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);
            $text = strtolower($text);
            $text = preg_replace('~[^-\w]+~', '', $text);
            if (empty($text))
            {
                return 'n-a';
            }
            return $text;
        }

        function submitReport()
        {
            if($this->input->post('submit'))
            {
                $data['title'] = $this->input->post('reportTitle');
                $data['special_notes'] = $this->input->post('special_notes');
                $data['user_id'] = $this->session->userdata('logedin_id');
                $data['month'] = $this->input->post('month');
                $data['project_id'] = $this->input->post('project');

                $available = $this->common_model->getTableData('reports',array('month'=>$data['month'],'project_id'=>$data['project_id']))->row();
                if(!empty($available))
                {
                    $this->session->set_flashdata('error','You have already submitted the report of the project. You can make the changes by editing the reports.');
                    redirect('project-holder/reports/');
                }
                $config['upload_path'] = BASEPATH.'../reports/';
                $config['allowed_types'] = "pdf|doc|docx|xls|xlsx";
                $config['max_size'] = 0;
                $this->load->library('upload',$config);
                $reportType = $this->common_model->getTableData('report_category',array('status'=>"1"))->result();
                //$value = json_decode($reportType->report_category);
                foreach($reportType as $v):

                    if($this->upload->do_upload($this->misc_lib->slugify($v->title).'-'.$v->id))
                    {
                        $file = $this->upload->data();
                        $file_name [$v->id] = $file['file_name'];
                    }
                    else
                        echo $this->upload->display_errors();
                    endforeach;
                $data['files'] = json_encode($file_name);
                $id = $this->common_model->insertData('reports',$data);
                $logs['report_id'] = $id;
                $logs['user_id'] = $this->session->userdata('logedin_id');
                $logs['logs'] = "REPORT SUBMITTED";
                $this->common_model->insertData('reports_logs',$logs);
                $this->session->set_flashdata('success','Data Saved Successfully.');
                $this->sentNotificationEmail('submit',$data['project_id'],$data['month']);
                redirect('project-holder/reports');
            }
            $data['title'] = "Submit Report";
            $data['reportType'] = $this->common_model->getTableData('report_category',array('status'=>"1"))->result();
            $data['projects'] = $this->common_model->getTableData('project_holder_detail',array('user_id'=>$this->session->userdata('logedin_id')))->result();
            $this->load->view('project-holder/uploadReports',$data);
        }

        function editReport($id)
        {

            if($this->input->post('submit'))
            {
                $data['title'] = $this->input->post('reportTitle');
                $data['special_notes'] = $this->input->post('special_notes');
                $data['month'] = $this->input->post('month');
                $data['project_id'] = $this->input->post('project');
                $config['upload_path'] = BASEPATH.'../reports/';
                $config['allowed_types'] = "pdf|doc|docx|xls|xlsx";
                $config['max_size'] = 0;
                $this->load->library('upload',$config);
                $reportType = $this->common_model->getTableData('report_category',array('status'=>"1"))->result();
                //$value = json_decode($reportType->report_category);
                foreach($reportType as $v):
                    if(isset($_FILES[$this->misc_lib->slugify($v->title).'-'.$v->id]['name']) && !empty($_FILES[$this->misc_lib->slugify($v->title).'-'.$v->id]['name']))
                    {
                        if($this->upload->do_upload($this->misc_lib->slugify($v->title).'-'.$v->id))
                        {
                            $file = $this->upload->data();
                            $file_name [$v->id] = $file['file_name'];
                        }
                        $available = $this->common_model->getTableData('report_status',array('report_id'=>$id,'report_type_id'=>$v->id))->row();
                        if(!empty($available))
                        {
                            $this->common_model->deleteTableData('report_status',array('id'=>$available->id));
                        }

                    }
                    else
                    {
                        $file_name[$v->id] = $this->input->post($this->misc_lib->slugify($v->title).'-'.$v->id.'-old');
                    }
                endforeach;
                $data['files'] = json_encode($file_name);
                $this->common_model->updateTableData('reports',$id,$data);
                $logs['report_id'] = $id;
                $logs['user_id'] = $this->session->userData('logedin_id');
                $logs['logs'] = "REPORT MODIFIED";
                $this->common_model->insertData('reports_logs',$logs);
                $this->session->set_flashdata('success','Data Saved Successfully.');
                $this->sentNotificationEmail('edit',$data['project_id'],$data['month']);
                redirect('project-holder/reports');
            }
             $data['title'] = "Edit Reports";
            $data['reportType'] = $this->common_model->getTableData('report_category',array('status'=>"1"))->result();
            $data['value'] = $this->common_model->getTableData('reports',array('id'=>$id))->row();
            $data['projects'] = $this->common_model->getTableData('project_holder_detail',array('user_id'=>$this->session->userdata('logedin_id')))->result();
            $this->load->view('project-holder/modifyReports',$data);
        }

        function sentNotificationEmail($type,$id,$month)
        {
            $projectData = $this->common_model->getTableData('projects',array('id'=>$id))->row();
            $userData = $this->common_model->getTableData('users',array('ID'=>$this->session->userdata('logedin_id')))->row();
            $projectOfficer = $this->common_model->getTableData('users',array('ID'=>$projectData->project_officer))->row();
            $projectManager = $this->common_model->getTableData('users',array('ID'=>$projectData->project_manager))->row();
            $email[] = $projectManager->email;
            $email[] = $projectOfficer->email;
            $msg = "Hi there, <br />";
            if($type =="submit")
            {
                $sub = "Report Submitted";
                $msg .= $userData->name.' has submitted the report of '.$projectData->name.' for the month of'.date('F', strtotime('01-' . $month . '-' . date('Y')));

            }
            else if($type=="edit")
            {
                $sub = "Report Modified";
                $msg .= $userData->name.' has modified the report of '.$projectData->name.' for the month of'.date('F', strtotime('01-' . $month . '-' . date('Y')));
            }
            $msg .= 'Please <a href="'.site_url('loginPage').'">Login</a> here to view the report <br /><br />';
            $msg .="Thanks!<br />Heifier International";

            $this->load->library('email');
            $config['mailtype'] = "html";
            $this->email->initialize($config);
            $this->email->subject($sub);
            $this->email->from('noreply@heifer.com','Heifer International');
            foreach($email as $e):
                $this->email->to($e);
                $this->email->send();
                $this->email->clear();
            endforeach;
        }
    }