<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 7/20/14
 * Time: 10:57 AM
 */
Class Policy extends CI_Controller{

    function __construct()
    {
        parent::__construct();
        if(!$this->session->userdata('logged_in'))
            redirect('loginpage');
        $this->load->model('policy_model');

    }
    public function index()
    {
        if($this->input->post('submitBtn'))
        {

            $this->load->helper('download');
            $file = $this->input->post('act');
            $this->policy_model->policyalllog();
            $name =$file;
            if(file_exists(BASEPATH.'../policies/'.$file)):
                $response = file_get_contents(BASEPATH.'../policies/'.$file); // Read the file's contents - cannot use relative path. It will try to call the file from same directory (controller) as this file. In order for it get the contents from the root folder put a . in front of the relative path//
                force_download($name, $response);
            else:
                redirect('project-holder/policy');
            endif;

        }
        $id = $this->session->userdata('logedin_id');
        $data['policy'] = $this->policy_model->getpolicy($id);
        $this->load->view('project-holder/policy',$data);
    }
    public function count()
    {

        $data = $this->policy_model->counteralert();
        if($data>0)
        {
            echo '1';
        }
        else
        {
            echo "0";
        }
        exit;
    }
}