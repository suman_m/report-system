<?php
class Backup extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('common_model');
        $this->load->model('backup_model');
        $this->load->library('misc_lib');
        $this->load->library('email_notification');
        if(!$this->session->userdata('logged_in'))
            redirect('loginpage');
    }

    function index()
    {
        $data['title'] = "Manage Backup Submission";
        $data['backupData'] = $this->common_model->getTableData('backup',array('user_id'=>$this->session->userdata('logedin_id')))->result();
        $data['backupType'] = $this->common_model->getTableData('report_settings',array('title'=>"BACKUP SETTINGS"))->row();
        $data['backupLogs'] = $this->backup_model->getLogs();
        $this->load->view('project-holder/backup',$data);
    }

    function submitBackup()
    {
        $data['title'] = "Submit Backup";
        $data['backupType'] = $this->common_model->getTableData('report_settings',array('title'=>'BACKUP SETTINGS'))->row();

        if($this->input->post('submit'))
        {
            $insertData['title'] = $this->input->post('backupTitle');
            $insertData['special_notes'] = $this->input->post('special_notes');
            $insertData['month'] = $this->input->post('month');
            $available = $this->common_model->getTableData('backup',array('month'=>$insertData['month']))->row();
            if(!empty($available))
            {
                $this->session->set_flashdata('error','You have already submitted the report of the project. You can make the changes by editing the reports.');
                redirect('project-holder/reports/');
            }
            $config['upload_path'] = BASEPATH.'../backup/';
            $config['allowed_types'] = "pdf|doc|docx|xls|xlsx";
            $config['max_size'] = 0;
            $this->load->library('upload',$config);
            $type = json_decode($data['backupType']->content);
            $fileName = array();
            foreach($type as $key=>$t):
                if($this->upload->do_upload($key))
                {
                    $file = $this->upload->data();
                    $fileName[$key] = $file['file_name'];

                }
                else
                {
                    echo $this->upload->display_errors();
                }
                endforeach;
            $insertData['files'] = json_encode($fileName);
            $insertData['user_id'] = $this->session->userdata('logedin_id');
            $id = $this->common_model->insertData('backup',$insertData);
            $logMessage['user_id'] = $this->session->userdata('logedin_id');
            $logMessage['backup_id'] = $id;
            $logMessage['logs'] = "Backup Submitted";
            $logMessage['access_level'] = $this->session->userdata('access_level');
            $this->common_model->insertData('backup_logs',$logMessage);
            $this->sentNotificationEmail('submit');
            redirect('project-holder/backup/');

        }
        $this->load->view('project-holder/submitBackup',$data);
    }

    function editBackup($id)
    {
        $data['title'] = "Submit Backup";
        $data['backupType'] = $this->common_model->getTableData('report_settings',array('title'=>'BACKUP SETTINGS'))->row();
        $data['value'] = $this->common_model->getTableData('backup',array('id'=>$id))->row();
        if($this->input->post('submit')):
            $insertData['title'] = $this->input->post('backupTitle');
            $insertData['special_notes'] = $this->input->post('special_notes');
            $insertData['month'] = $this->input->post('month');
            $config['upload_path'] = BASEPATH.'../backup/';
            $config['allowed_types'] = "pdf|doc|docx|xls|xlsx";
            $config['max_size'] = 0;
            $this->load->library('upload',$config);
            $type = json_decode($data['backupType']->content);
            $fileName = array();
            foreach($type as $key=>$t):
                if($this->upload->do_upload($key))
                {
                    $file = $this->upload->data();
                    $fileName[$key] = $file['file_name'];

                }
                else
                {
                    echo $this->upload->display_errors();
                }
            endforeach;
            $insertData['files'] = json_encode($fileName);
            $insertData['user_id'] = $this->session->userdata('logedin_id');
            if($data['value']->status==2)
                $insertData['status'] = 0;
            $this->common_model->updateTableData('backup',$id,$insertData);
            $logMessage['user_id'] = $this->session->userdata('logedin_id');
            $logMessage['backup_id'] = $id;
            $logMessage['logs'] = "Backup Modified";
            $logMessage['access_level'] = $this->session->userdata('access_level');
            $this->common_model->insertData('backup_logs',$logMessage);
            $this->sentNotificationEmail('edit');
            redirect('project-holder/backup/');
        endif;
        $this->load->view('project-holder/editBackup',$data);
    }

    function sentNotificationEmail($type)
    {
        $emailList = $this->email_notification->getEmailForHolder($this->session->userdata('logedin_id'));
        $msg = $this->email_notification->getBackupMessage($type);
        $this->load->library('email');
        $config['mailtype'] = "html";
        $this->email->initialize($config);
        foreach($emailList as $email):
            $this->email->from('adiksudip@gmail.com','Heifer International');
            $this->email->to($email);
            $this->email->subject('Report Submitted');
            $this->email->message($msg);
            $this->email->send();
            $this->email->clear();
        endforeach;
    }
}