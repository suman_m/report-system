<?php
    class Backup extends CI_Controller
    {
        function __construct()
        {
            parent::__construct();
            if(!$this->session->userdata('logged_in'))
                redirect('loginpage');
            $this->load->model('common_model');
            $this->load->model('backup_model');
            $this->load->library('misc_lib');
        }

        function index()
        {
            $data['title'] = "Backup";
            $data['value'] = $this->common_model->getTableData('report_settings',array('title'=>"BACKUP SETTINGS"))->row();
            $data['backupData'] = $this->backup_model->getData();
            $backupId = array();
            foreach($data['backupData'] as $b):
                $backupId[] = $b->id;
                endforeach;
            $data['backupLogs'] = $this->backup_model->getBackupLog($backupId)->result();
            $this->load->view('program-manager/backup',$data);
        }
    }