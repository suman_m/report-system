<?php

/**
 * Class Others
 */
class Others extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('others_model');
        $this->load->model('common_model');
    }

    function index()
    {
        $data['title'] = "Other Documents";
        $data['content'] = $this->others_model->getOthersDocument();
        $this->load->view('program-manager/others',$data);
    }
    function download_other($id)
    {
        $insertData['others_id'] = $id;
        $insertData['user_id'] = $this->session->userdata('logedin_id');
        $insertData['logs']= "Others Report Downloaded";
        $insertData['access_level'] = "Program Manager";
        $this->common_model->insertData('others_log',$insertData);

        $data = $this->common_model->getTableData('others',array('id'=>$id))->row();
        $this->load->helper('download');
        $content = file_get_contents(BASEPATH.'../others/'.$data->file);
        $name = $data->file;
        force_download($name,$content);
    }
}