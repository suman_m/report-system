<?php
    class Reports extends CI_Controller
    {
        function __construct()
        {
            parent::__construct();
            if(!$this->session->userdata('logged_in'))
                redirect('loginpage');
            $this->load->model('common_model');
            $this->load->model('reports_model');
            $this->load->library('misc_lib');
        }

        function index()
        {
            $data['title'] = "Reports";
            $data['reports'] = $this->reports_model->getReportDataManager();
            $data['reportType'] = $this->common_model->getTableData('report_category',array('status'=>1))->result();
            $data['reportStatus'] = $this->common_model->getTableData('report_status')->result();

            $reportId = array();
            foreach($data['reports'] as $re):
                $reportId[] = $re->id;
                endforeach;
            //$reportId = trim($reportId,',');
            $data['reportLogs'] = $this->reports_model->getReportLogsManager($reportId)->result();
            $this->load->view('program-manager/reports',$data);
        }

        function downloadReport($report_id,$report_type_id)
        {
            $report = $this->common_model->getTableData('reports',array('id'=>$report_id))->row();
            $report_content = json_decode($report->files);
            foreach($report_content as $key=> $content):
                if($key==$report_type_id)
                {
                    $insertData['report_id'] = $report_id;
                    $insertData['report_type_id'] = $report_type_id;
                    $insertData['user_id'] = $this->session->userdata('logedin_id');
                    $insertData['logs'] = "REPORT DOWNLOADED";
                    $this->common_model->insertData('reports_logs',$insertData);
                    $this->load->helper('download');
                    $data = file_get_contents(BASEPATH.'../reports/'.$content);
                    $name = $content;
                    force_download($name,$data);

                }
            endforeach;
        }
    }