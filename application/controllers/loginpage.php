<?php

class loginpage extends CI_Controller {

    function __construct(){
        parent::__construct();
    }

    public function index(){
        if($this->session->userdata('logged_in') == TRUE){
            redirect("myAccount");
        }
        $this->load->view('loginpage_view');
    }

    public function login_check(){
        $email = $this->input->post("logEmail");
        $pass = $this->input->post("logPass");
        if(!empty($email) && !empty($pass)){
            $pass = md5($pass);
            $this->load->model("loginpage_model");
            $log = $this->loginpage_model->get_logged_user($email,$pass);
            if(count($log) > 0){
                $logged_user = array("logedin_id" => $log[0]->ID, "email" => $log[0]->email, "logged_in" => true, "access_level" => $log[0]->accessLevel, "sub_level"=>$log[0]->sub_level);
                if($log[0]->accessLevel != "Super User"){
                    if($log[0]->active == 0){
                        $this->session->set_userdata(array("logging_error" => true, "logged_in" => false, 'alertMsg' => 'You are placed as an inactive user, please contact Head Office.'));
                        redirect("loginpage");
                    }elseif($log[0]->active == -1){
                        $this->session->set_userdata(array("logging_error" => true, "logged_in" => false, 'alertMsg' => 'Your credential has been deleted, please contact Head Office.'));
                        redirect("loginpage", $data);
                    }
                }
                $this->session->set_userdata($logged_user);
                redirect("myAccount");
            }else{
                $this->session->set_userdata(array("logging_error" => true, "logged_in" => false, 'alertMsg' => 'Email or password is incorrect.'));
                redirect("loginpage");
            }
        }
    }

    public function logout()
    {
        $this->session->sess_destroy();
        redirect('loginpage');
    }
}
?>