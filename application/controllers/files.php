<?php

/**
 * Class Files
 * php class to remove the the file from the folder
 */
class Files extends CI_Controller
    {
        function __construct()
        {
            parent::__construct();
            $this->load->model('common_model');
        }

        function index()
        {
            $currentMonth = date('n');
            $lastTwoMonth = $currentMonth - 2;
            $reports = $this->common_model->getTableData('reports',array('month <'=>$lastTwoMonth))->result();
            foreach($reports as $re):
                $files = json_decode($re->files);
                foreach($files as $file):
                    @unlink(BASEPATH.'../reports/'.$file);
                    endforeach;
                endforeach;
        }

        function removeBackup()
        {
            $currentMonth = date('n');
            $lastTwoMonth = $currentMonth - 2;
            $reports = $this->common_model->getTableData('backup',array('month <'=>$lastTwoMonth))->result();
            foreach($reports as $re):
                $files = json_decode($re->files);
                foreach($files as $file):
                    echo $file;
                    @unlink(BASEPATH.'../backup/'.$file);
                endforeach;
            endforeach;
        }
    }