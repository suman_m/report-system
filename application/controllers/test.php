<?php
class Test extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('common_model');
    }

    function index()
    {
        $reportSettings = $this->common_model->getTableData('report_settings',array('title'=>"REPORT SETTINGS"))->row();
        if($reportSettings->report_submission_time=="monthly"):
            if($reportSettings->submission_end=="next-month"):
                $m = date('n');
                $m = $m + 1;
                $curDate = date('Y-m-d',strtotime(date('Y').'-'.$m.'-05'));
                $lastSubmissionDate = date('Y-m-d',strtotime($curDate));

                $currentDate = date('Y-m-d');
                if(strtotime($currentDate)<=strtotime($lastSubmissionDate))
                {
                    $currentDate = new DateTime($currentDate);
                    $lastSubmissionDate = new DateTime($lastSubmissionDate);
                    $interval = $currentDate->diff($lastSubmissionDate);
                    $daysInterval = $interval->days;
                    if($daysInterval<=$reportSettings->email_frequency)
                    {
                        $this->reportSubmissionNotification();
                    }
                }
                endif;
            endif;
    }

    function reportSubmissionNotification()
    {
        $day = date('d');
        $m = date('n');

        if($day<= 5)
        {
            $m = $m - 2;
        }
        else
            $m = $m - 1;


        $projectHolderData = $this->common_model->getTableData('users',array('accessLevel'=>"Project Holder"))->result();
        $projectData = $this->common_model->getTableData('projects')->result();
        $uid = array();
        $email = array();
        foreach($projectHolderData as $pd):
            foreach($projectData as $p):
                if($pd->ID==$p->assign_to_project_holder)
                {
                    $reportSubmissionUser = $this->common_model->getTableData('reports',array('month'=>$m,'project_id'=>$p->id,'user_id'=>$pd->ID))->row();
                    if(empty($reportSubmissionUser))
                    {
                        $email[$p->id] = $pd->email;
                    }
                }
                endforeach;
            endforeach;


        if(!empty($email))
        {
            $this->load->library('email');
            $config['mailtype'] = 'html';
            $this->email->initialize($config);
            foreach($email as $key=>$e):
                $projectName = $this->common_model->getTableData('projects',array('id'=>$key))->row();
                $msg = "Hi there, <br />";
                $msg .= "This is an automated email to notify that the report submission time is nearby. Please don't forget to send the reports before the submission time exceeds.<br /><br />";
                $msg .="Thanks!<br />Heifier International";
                $this->email->from('info@heifierinterantional.org','Heifer International');
                $this->email->to($e);
                $this->email->subject('Report Submission Time Nearby');
                $this->email->message($msg);
                $this->email->send();
                $this->email->clear();
                endforeach;
        }
    }

    function datePassedNotification()
    {
        $day = date('d');
        $m = date('n');
        $m = $m - 1;

        if($day >5):
        //$reportSubmissionUser = $this->common_model->getTableData('reports',array('month'=>$m))->result();
        $projectHolderData = $this->common_model->getTableData('users',array('accessLevel'=>"Project Holder"))->result();
        $projectData = $this->common_model->getTableData('projects')->result();
        $uid = array();
        /*foreach($reportSubmissionUser as $rs):
            $uid[] = $rs->user_id;
            endforeach; */
        $email = array();
        foreach($projectHolderData as $pd):
            foreach($projectData as $p):
                if($pd->ID==$p->assign_to_project_holder)
                {
                    $reportSubmissionUser = $this->common_model->getTableData('reports',array('month'=>$m,'project_id'=>$p->id,'user_id'=>$pd->ID))->row();
                    if(empty($reportSubmissionUser))
                    {
                        $email[$p->id] = $pd->email;
                    }
                }
            endforeach;
        endforeach;

            if(!empty($email))
            {
                $this->load->library('email');
                $config['mailtype'] = 'html';
                $this->email->initialize($config);
                foreach($email as $key=>$e):
                    $projectName = $this->common_model->getTableData('projects',array('id'=>$key))->row();
                    //message to the project holder
                    $msg = "Hi there, <br />";
                    $msg .= "This is an automated email to notify that you have not submitted the report of the ".$projectName->name.". Please send the reports ASAP.<br /><br />";
                    $msg .="Thanks!<br />Heifier Internationa Nepall";
                    $this->email->from('info@heifierinterantional.org','Heifer International');
                    $this->email->to($e);
                    $this->email->subject('Report Submission Time Exceeds');
                    $this->email->message($msg);
                    $this->email->send();
                    $this->email->clear();

                    $projectHolderDetail = $this->common_model->getTableData('users',array('ID'=>$projectName->assign_to_project_holder))->row();

                    // message to the program officer
                    $programOfficer = $this->common_model->getTableData('users',array('ID'=>$projectName->program_officer))->row();
                    $msg = "Hi there, <br /><br />";
                    $msg .= 'This is an automated email to notify you that a project holder '.$projectHolderDetail->name.' has not submitted the reports of the '.$projectName->name;
                    $msg .="Thanks!<br />Heifier International Nepal";
                    $this->email->from('info@heifierinterantional.org','Heifer International');
                    $this->email->to($programOfficer->email);
                    $this->email->subject('Report Submission Time Exceeds');
                    $this->email->message($msg);
                    $this->email->send();
                    $this->email->clear();
                    //message to the program manager

                    $programManager = $this->common_model->getTableData('users',array('ID'=>$projectName->program_officer))->row();
                    $msg = "Hi there, <br /><br />";
                    $msg .= 'This is an automated email to notify you that a project holder '.$projectHolderDetail->name.' has not submitted the reports of the '.$projectName->name;
                    $msg .="Thanks!<br />Heifier International Nepal";
                    $this->email->from('info@heifierinterantional.org','Heifer International');
                    $this->email->to($programManager->email);
                    $this->email->subject('Report Submission Time Exceeds');
                    $this->email->message($msg);
                    $this->email->send();
                    $this->email->clear();
                endforeach;
            }
            endif;
    }
}