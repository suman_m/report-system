<?php

class MyAccount extends CI_Controller {

    function __construct(){
        parent::__construct();
        $this->load->model("myAccount_model");
        if(!$this->session->userdata('logged_in'))
            redirect('loginpage');
    }

    public function index(){
        $result["data"] = $user = $this->myAccount_model->get_user_details();
        $result["regn"] = $this->myAccount_model->get_region($user[0]->region);
        $result["edit"] = false;
        if($this->session->userdata('access_level')=="Project Holder")
        {
            $result['report_notification'] = $this->myAccount_model->submissionReportNotification();
            $result['backup_notification'] = $this->myAccount_model->submissionBackupNotification();
            $result['report_ends'] = $this->myAccount_model->datePassedReportNotification();
            $result['backup_ends'] = $this->myAccount_model->datePassedBackupNotification();
            $result['forms'] = $this->myAccount_model->getNewFormData();
            $result['policies'] = $this->myAccount_model->getNewPoliciesData();
        }
        $this->load->view('myAccount_view', $result);
    }

    public function changePassword(){
        $result["edit"] = true;
        $this->load->view('myAccount_view', $result);
    }

    public function savePassword(){
        $oldpass = md5($this->input->post("oldPass"));
        $newpass = md5($this->input->post("newPass"));
        $flag = $this->myAccount_model->updatePassword($oldpass,$newpass);
        $data["data"] = $user = $this->myAccount_model->get_user_details();
        $data["regn"] = $this->myAccount_model->get_region($user[0]->region);
        if($flag)
            $data["success"] = 1;
        else
            $data["success"] = 0;
        $data["edit"] = false;
        $this->load->view('myAccount_view', $data);
    }
}
?>