<?php

class myAccount_model extends CI_Model {

    function __construct(){
        parent::__construct();
    }

    public function get_user_details(){
        $result = $this->db->get_where('users', array('ID' => $this->session->userdata("logedin_id")))->result();
        return $result;
    }

    public function get_region($reg_id){
        return $result = $this->db->get_where('regions', array('id' => $reg_id))->result();
    }

    public function updatePassword($old,$new){
        $res = $this->db->get_where('users', array('ID' => $this->session->userdata("logedin_id"), 'password' => $old));
        if($res->num_rows > 0){
            $this->db->where('ID', $this->session->userdata("logedin_id"));
            $update = $this->db->update('users', array('password' => $new));
            return $update;
        }else{
            return false;
        }
    }

    public function getReportNotifications()
    {
        $data = $this->common_model->getTableData('report_settings',array('title'=>"REPORT SETTINGS"))->row();
        if($data->report_submission_time=="quarterly")
        {
            $start_time = date('Y-m-d',strtotime($data->quarterly_start));
            $start_time = new DateTime($start_time);
            //$current_time = date('Y-m-d');
            $current_time = "2014-08-23";
            $current_time = new DateTime($current_time);
            $interval = date_diff($start_time, $current_time);
            $months = $interval->format('%m');
            if($months % 2==0)
            {
                if($data->submission_end=="last-day")
                {
                    $lastSubmissionDate =  date('Y-m-t');
                }
                else if($data->submission_end="last-friday")
                {
                    $month = date('F');
                    $year = date('Y');
                    $lastSubmissionDate =  date('Y-m-d',strtotime('last friday of '.$month.' '.$year));
                }
            }
        }
        else if($data->report_submission_time=="monthly")
        {
            if($data->submission_end=="last-day")
            {
                $lastSubmissionDate =  date('Y-m-t');
            }
            else if($data->submission_end="last-friday")
            {
                $month = date('F');
                $year = date('Y');
                $lastSubmissionDate =  date('Y-m-d',strtotime('last friday of '.$month.' '.$year));
            }
        }

        if(isset($lastSubmissionDate))
        {
            $submission_ends = $lastSubmissionDate;
        }
        else
        {
            $submission_ends = '';
        }
        return $submission_ends;
    }

    function getBackupNotifications()
    {
        $data = $this->common_model->getTableData('report_settings',array('title'=>"BACKUP SETTINGS"))->row();
        if($data->report_submission_time=="quarterly")
        {
            $start_time = date('Y-m-d',strtotime($data->quarterly_start));
            $start_time = new DateTime($start_time);
            //$current_time = date('Y-m-d');
            $current_time = "2014-08-23";
            $current_time = new DateTime($current_time);
            $interval = date_diff($start_time, $current_time);
            $months = $interval->format('%m');
            if($months % 2==0)
            {
                if($data->submission_end=="last-day")
                {
                    $lastSubmissionDate =  date('Y-m-t');
                }
                else if($data->submission_end="last-friday")
                {
                    $month = date('F');
                    $year = date('Y');
                    $lastSubmissionDate =  date('Y-m-d',strtotime('last friday of '.$month.' '.$year));
                }
            }
        }
        else if($data->report_submission_time=="monthly")
        {
            if($data->submission_end=="last-day")
            {
                $lastSubmissionDate =  date('Y-m-t');
            }
            else if($data->submission_end="last-friday")
            {
                $month = date('F');
                $year = date('Y');
                $lastSubmissionDate =  date('Y-m-d',strtotime('last friday of '.$month.' '.$year));
            }
        }

        if(isset($lastSubmissionDate))
        {
            $submission_ends = $lastSubmissionDate;
        }
        else
        {
            $submission_ends = '';
        }
        return $submission_ends;
    }

    function datePassedNotificationReports()
    {
        $reportSettings = $this->common_model->getTableData('report_settings',array('title'=>"REPORT SETTINGS"))->row();
        if($reportSettings->report_submission_time=="monthly")
        {
            $month = date('n');
            $month = $month - 1;
            $m = $month;
            $reportsData = $this->common_model->getTableData('reports',array('month'=>$month))->result();
        }
        else if($reportSettings->report_submission_time=="quarterly")
        {
            $start_time = date('Y-m-d',strtotime($reportSettings->quarterly_start));
            $start_time = new DateTime($start_time);
            //$current_time = date('Y-m-d');
            $current_time = "2014-08-23";
            $current_time = new DateTime($current_time);
            $interval = date_diff($start_time, $current_time);
            $months = $interval->format('%m');

            if($months % 3==0)
            {
                $m = date('n');
                $m = $m - 1;
                $reportsData = $this->common_model->getTableData('reports',array('month'=>$m))->result();
            }
            else
            {
                $reportsData = array();
            }
        }

        if(!empty($reportsData))
        {
            $id = $this->session->userdata('logedin_id');
            $userId = array();
            foreach($reportsData as $rd):
                    $userId[] = $rd->user_id;
                endforeach;

            if(!in_array($id,$userId))
            {
                $date = date('F',strtotime('2014-'.$m.'-01'));
            }
            else
            {
                $date = '';
            }
        }
        else
        {
            $date = date('F',strtotime('2014-'.$m.'-01'));
        }
        return $date;
    }

    function datePassedNotificationBackup()
    {
        $reportSettings = $this->common_model->getTableData('report_settings',array('title'=>"BACKUP SETTINGS"))->row();
        if($reportSettings->report_submission_time=="monthly")
        {
            $month = date('n');
            $month = $month - 1;
            $m = $month;
            $reportsData = $this->common_model->getTableData('backup',array('month'=>$month))->result();
        }
        else if($reportSettings->report_submission_time=="quarterly")
        {
            $start_time = date('Y-m-d',strtotime($reportSettings->quarterly_start));
            $start_time = new DateTime($start_time);
            //$current_time = date('Y-m-d');
            $current_time = "2014-08-23";
            $current_time = new DateTime($current_time);
            $interval = date_diff($start_time, $current_time);
            $months = $interval->format('%m');

            if($months % 3==0)
            {
                $m = date('n');
                $m = $m - 1;
                $reportsData = $this->common_model->getTableData('backup',array('month'=>$m))->result();
            }
            else
            {
                $reportsData = array();
            }
        }


        if(!empty($reportsData))
        {
            $id = $this->session->userdata('logedin_id');
            $userId = array();
            foreach($reportsData as $rd):
                $userId[] = $rd->user_id;
            endforeach;

            if(!in_array($id,$userId))
            {
                $date = date('F',strtotime('2014-'.$m.'-01'));
            }
            else
            {
                $date = '';
            }
        }
        else
        {
            $date = date('F',strtotime('2014-'.$m.'-01'));
        }
        return $date;
    }

    function getNewFormData()
    {
        $id = $this->session->userdata('logedin_id');
        $form_receiver = $this->db->get_where('form_receiver',array('receiver_id'=>$id))->result();
        $flag = 0;
        if(!empty($form_receiver))
        {
            $formId = array();
            foreach($form_receiver as $fr):
                $available = $this->common_model->getTableData('forms_format_log',array('form_id'=>$fr->form_id,'user_id'=>$id))->row();
                if(empty($available))
                {
                    $flag = 1;
                    break;
                }
                endforeach;
        }
        return $flag;
    }

    function getNewPoliciesData()
    {
        $id = $this->session->userdata('logedin_id');
        $policyReceiver = $this->db->get_where('policy_receiver',array('receiver_id'=>$id))->result();
        $flag = 0;

        if(!empty($policyReceiver))
        {
            foreach($policyReceiver as $pr):
                $available = $this->db->get_where('policy_log',array('policy_id'=>$pr->id,'user_id'=>$id))->row();
                if(empty($available))
                {
                    $flag = 1;
                    break;
                }
                endforeach;
        }
        return $flag;
    }

    function datePassedReportNotification()
    {
        $date = date('d');
        $m = date('n');
        $d = '';

        if($date >5)
        {
            $id = $this->session->userdata('logedin_id');
            $m = $m - 1;
            $projectHolderData = $this->common_model->getTableData('users',array('ID'=>$id))->row();
            $projectData = $this->common_model->getTableData('projects',array('assign_to_project_holder'=>$id))->result();
            foreach($projectData as $pd):
                $available = $this->common_model->getTableData('reports',array('project_id'=>$pd->id,'month'=>$m))->row();
                if(empty($available))
                {
                    $d = date('F',strtotime('2014-'.$m.'-01'));
                    break;
                }
                endforeach;
        }
        return $d;
    }

    function datePassedBackupNotification()
    {
        $date = date('d');
        $m = date('n');
        $d = '';

        if($date >5)
        {
            $id = $this->session->userdata('logedin_id');
            $m = $m - 1;
           $available = $this->common_model->getTableData('backup',array('user_id'=>$id,'month'=>$m))->row();
            if(empty($available))
            {
                $d = date('F',strtotime('2014-'.$m.'-01'));
            }
        }
        return $d;
    }

    function submissionReportNotification()
    {
        $date = date('d');
        $m = date('n');
        if($date > 5)
        {
            $m = $m + 1;
        }
        $d = date('F',strtotime('2014-'.$m.'-01'));
        return $d;
    }

    function submissionBackupNotification()
    {
        $date = date('d');
        $m = date('n');
        if($date > 5)
        {
            $m = $m + 1;
        }
        $d = date('F',strtotime('2014-'.$m.'-01'));
        return $d;
    }
}
?>