<?php
    class Special_report_model extends CI_Model
    {
        function getLogs()
        {
            $logs = "SELECT * FROM special_report as s INNER JOIN special_report_logs as l ON s.id=l.special_report_id WHERE s.user_id=".$this->session->userdata('logedin_id');
            $value = $this->db->query($logs);
            return $value->result();
        }

        function getSpecialReport()
        {
            $userId = $this->session->userdata('logedin_id');
            $userData = $this->db->get_where('users',array('ID'=>$userId))->row();
            $userList = $this->db->get_where('users',array('accessLevel'=>"Project Holder",'region'=>$userData->region))->result();
            $userArray = array();
            foreach($userList as $user):
                $userArray[] = $user->ID;
            endforeach;
            //$userArray = trim($userArray,',');
            if(!empty($userArray))
            {
                $this->db->where_in('user_id',$userArray);
                $reports = $this->db->get('special_report')->result();

            }
            else
            {
                $reports = array();
            }
            return $reports;
        }

        function getSpecialReportLogs($reportId)
        {
            $this->db->where_in('special_report_id',$reportId);
            return  $this->db->get('special_report_logs');
        }

        function formsFormatData()
        {
           $query = "SELECT * FROM forms_format WHERE valid_date >= ".date('Y-m-d')." AND id IN (SELECT form_id FROM form_receiver WHERE receiver_id =".$this->session->userdata('logedin_id')." )";
            $data = $this->db->query($query);
            return $data->result();
        }

        function getSearchData($data)
        {
            $form = $data['form'];
            $region = $data['regions'];

            if($form!=0):
                $formformat = $this->common_model->getTableData('forms_format',array('id'=>$form))->result();
                $formreceiver = $this->common_model->getTableData('form_receiver',array('form_id'=>$form))->result();
            else:
                $formformat = $this->common_model->getTableData('forms_format',array('valid_date >'=>date('Y-m-d')))->result();
                if(empty($formformat)):
                    $data = array();
                    return $data;
                    endif;
                $fid = array();
                foreach($formformat as $f):
                    $fid[] = $f->id;
                    endforeach;
                if(!empty($fid)):
                    $this->db->where_in('form_id',$fid);
                $formreceiver = $this->db->get('form_receiver')->result();
                    endif;
            endif;

            if($region !=0)
            {
                $userId = $this->common_model->getTableData('users',array('region'=>$region,'accessLevel'=>"Project Holder"))->result();
                $uid = array();
                foreach($userId as $u):
                    foreach($formreceiver as $fr):
                        if($fr->receiver_id==$u->ID)
                            $uid[$fr->id] = $u->ID;
                    endforeach;
                 endforeach;
            }
            else
            {
                $uid = array();
                foreach($formreceiver as $fr):
                    $uid[$fr->id] = $fr->receiver_id;
                    endforeach;
            }
            $formId = array();
            foreach($formformat as $ff):
                $formId[] = $ff->id;
                endforeach;
           $this->db->where_in('form_id',$formId);
            $submissionList = $this->db->get('special_report')->result();

             $list = array();
                foreach($submissionList as $sl):
                    foreach($uid as $key=>$ui):
                        $form_id = $this->common_model->getTableData('form_receiver',array('id'=>$key))->row()->form_id;
                        if($sl->form_id==$form_id && $ui==$sl->user_id)
                            $list[] = $sl;
                        endforeach;
                endforeach;
                return $list;
        }


    }