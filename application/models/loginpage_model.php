<?php

class LoginPage_model extends CI_Model {

    function __construct(){
        parent::__construct();
    }

    public function get_logged_user($email,$pass){
        $result = $this->db->get_where('users', array('email' => $email, 'password' => $pass))->result();
        return $result;
    }
}
?>