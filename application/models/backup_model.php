<?php

class Backup_model extends CI_Model
{
    function getLogs()
    {
        $logs = "SELECT * FROM backup as b INNER JOIN backup_logs as l ON b.id=l.backup_id WHERE b.user_id=".$this->session->userdata('logedin_id');
        $value = $this->db->query($logs);
        return $value->result();
    }

    function getData()
    {
        $userId = $this->session->userdata('logedin_id');
        $userData = $this->db->get_where('users',array('ID'=>$userId))->row();
        $userList = $this->db->get_where('users',array('accessLevel'=>"Project Holder",'region'=>$userData->region))->result();
        $userArray = array();
        foreach($userList as $user):
            $userArray[] = $user->ID;
        endforeach;
        //$userArray = trim($userArray,',');
        if(!empty($userArray))
        {
            $this->db->where_in('user_id',$userArray);
            $reports = $this->db->get('backup')->result();

        }
        else
        {
            $reports = array();
        }
        return $reports;
    }

    function getBackupLog($backupId)
    {
        $this->db->where_in('backup_id',$backupId);
        return  $this->db->get('backup_logs');
    }

    function getSearchResult($search)
    {
        $region = $search['region'];
        $month = $search['month'];

        if($region !=0)
        {
            $users = $this->db->get_where('users',array('region'=>$region,'accessLevel'=>"Project Holder"))->result();
            if(!empty($users))
            {
                $userId = array();
                foreach($users as $use):
                    $userId[] = $use->ID;
                endforeach;
                if(!empty($userId))
                {
                    $this->db->where_in('user_id',$userId);
                }
                else
                {
                    $this->db->where('user_id',0);
                }
            }
            else
            {
                $this->db->where('user_id',0);
            }
        }
        if($month !=0)
        {
            $this->db->where('month',$month);
        }
        return $this->db->get('backup')->result();
    }
    function getDownloadData($month, $region)
    {
        if($region !=0)
        {
            $users = $this->db->get_where('users',array('accessLevel'=>"Project Holder",'region'=>$region))->result();
            $userId = array();
            foreach($users as $u):
                $userId[] = $u->ID;
            endforeach;

        }

        if($month !=0)
        {
            $this->db->where('month',$month);
        }
        if(!empty($userId)):
            $this->db->where_in('user_id',$userId);
        endif;

        $data = $this->db->get('backup');
        return $data->result();
    }
}