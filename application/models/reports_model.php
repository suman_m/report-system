<?php
class Reports_model extends CI_Model
{
    function getReportStatus()
    {
        $this->db->where('logs','REPORT ACCEPTED');
        $this->db->or_where('logs','REPORT REJECTED');
        return $this->db->get('reports_logs');
    }

    function getReportLogs()
    {
        //$reports = $this->db->get_where('reports',array('user_id'=>$this->session->userdata('loggedin_id')))->result();
        $logs = "SELECT * FROM reports as r INNER JOIN reports_logs as l ON r.id=l.report_id WHERE r.user_id=".$this->session->userdata('logedin_id');
        $value = $this->db->query($logs);
        return $value->result();
    }

    function getReportDataManager()
    {
        $userId = $this->session->userdata('logedin_id');
        $userData = $this->db->get_where('users',array('ID'=>$userId))->row();
        $userList = $this->db->get_where('users',array('accessLevel'=>"Project Holder",'region'=>$userData->region))->result();
        $userArray = array();
        foreach($userList as $user):
            $userArray[] = $user->ID;
            endforeach;
        //$userArray = trim($userArray,',');
        if(!empty($userArray))
        {
            $this->db->where_in('user_id',$userArray);
            $reports = $this->db->get('reports')->result();

        }
        else
        {
            $reports = array();
        }
        return $reports;
    }

    function getReportLogsManager($reportId)
    {
        $this->db->where_in('report_id',$reportId);
        return  $this->db->get('reports_logs');
    }

    function getSearchResult($search = array())
    {
        $startDate = $search['dateStart'];
        $endDate = $search['dateEnd'];
        $region = $search['region'];
        $month = $search['month'];
        $project = $search['project'];

        if($region !=0)
        {
            $users = $this->db->get_where('users',array('region'=>$region,'accessLevel'=>"Project Holder"))->result();
            if(!empty($users))
            {
                $userId = array();
                foreach($users as $use):
                    $userId[] = $use->ID;
                endforeach;
                if(!empty($userId))
                {
                    $this->db->where_in('user_id',$userId);
                }
                else
                {
                    $this->db->where('user_id',0);
                }
            }
            else
            {
                $this->db->where('user_id',0);
            }
        }
        if($startDate !='')
        {
            $startDate = date('Y-m-d h:i:s',strtotime($search['dateStart']));
            $this->db->where('date >=', $startDate);
        }
        if($endDate !='')
        {
            $endDate = date('Y-m-d h:i:s',strtotime($search['dateEnd']));
            $this->db->where('date <=', $endDate);
        }

        if($month !=0)
        {
            $this->db->where('month',$month);
        }

        if($project !=0)
        {
            $this->db->where('project_id',$project);
        }
        return $this->db->get('reports')->result();

    }

    function getDownloadData($month, $region)
    {
        if($region !=0)
        {
            $users = $this->db->get_where('users',array('accessLevel'=>"Project Holder",'region'=>$region))->result();
            $userId = array();
            foreach($users as $u):
                $userId[] = $u->ID;
                endforeach;

        }

        if($month !=0)
        {
            $this->db->where('month',$month);
        }
        if(!empty($userId)):
            $this->db->where_in('user_id',$userId);
        endif;

        $data = $this->db->get('reports');
        return $data->result();
    }
}