<?php

/**
 * Class Others_model
 */
class Others_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }
    public function getallpolicy()
    {
        $this->db->select('*');
        $this->db->from('policy');
        $this->db->order_by('id','DESC');
        $query =$this->db->get();
        $res = $query->result();
        return $res;

    }

    public function getAll()
    {
        $this->db->select('*');
        $this->db->from('users');
        $this->db->join('user_hierarchy', 'user_hierarchy.user_id = users.ID', 'left');
        $query = $this->db->get();
        $res = $query->result();
        return $res;

    }

    public function gethistory($id)
    {
        $this->db->select('*');
        $this->db->from('policy_log');
        $this->db->where('policy_log.policy_id',$id);
        $query =$this->db->get();
        $res = $query->result();
        return $res;
    }
    public function regions()
    {
        $this->db->select('*');
        $this->db->from('regions');
        $query = $this->db->get();
        $res = $query->result();
        return $res;
    }

    public function search()
    {
        $form_id = $this->uri->segment(4);
        $reg = $this->input->post('region');
        $from = $this->input->post('from');

        $to = $this->input->post('to');
        $this->db->select('*');
        $this->db->from('policy_log');
        if($reg!=null){
            $this->db->where('region_id',$reg);
            $this->db->where('policy_id',$form_id);
        }

        if($from!='')
        {
            $from = date('Y-m-d h:i:s',strtotime($from));
            $this->db->where('download_date >=',$from);
            $this->db->where('policy_id',$form_id);
        }

        if($to!='')
        {
            $to = date('Y-m-d h:i:s',strtotime($to));
            $this->db->where('download_date <=',$to);
            $this->db->where('policy_id',$form_id);
        }

        $query = $this->db->get();
        $res   = $query->result();
        return  $res;
    }

    public function sendform($file_name)
    {
        $data =array(
            "others_title"=>$this->input->post('otherTitle'),
            "others_description"=>$this->input->post('otherDescription'),
            "file"=>$file_name,
            "valid_date"=>$this->input->post('validDate')
        );
        $res = $this->db->insert('others',$data);
        if($res)
        {
            $op = $this->input->post('optional');
            if(isset($op))
            {
                $id = $this->db->insert_id();
                $insertData['others_id'] = $id;
                $insertData['user_id'] = $this->session->userdata('logedin_id');
                $insertData['logs'] = "Others Report Uploaded";
                $insertData['access_level'] = "Head Officer";
                $this->db->insert('others_log',$insertData);
                $this->session->set_userdata(array('id'=>$id));
                $sess_id = $this->session->userdata('id');
                foreach($this->input->post('optional') as $res):
                    $in['receiver_id']=$res;
                    $in['other_id']=$sess_id;
                    $result = $this->db->insert('other_receiver',$in);

                endforeach;
                $this->session->unset_userdata('id');

            }

        }
        return true;

    }

    public  function allemail($em)
    {
        $list = implode(",",$em);
        $this->db->select('users.email');
        $this->db->from('users');
        $this->db->where('users.ID IN ('.$list.')');
        $query = $this->db->get();
        $res = $query->result();
        return $res;
    }

    function getOthersDocument()
    {
        $query = 'SELECT o.id as ID, ore.id as recev, o.*,ore.* from others as o INNER JOIN other_receiver as ore ON o.id=ore.other_id WHERE receiver_id= '.$this->session->userdata('logedin_id');
        $query = $this->db->query($query);
        return $query->result();
    }
}