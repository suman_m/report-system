<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 7/20/14
 * Time: 10:43 AM
 */
Class Policy_model extends CI_Model{

    function __construct()
    {
        parent::__construct();
    }

    /********* HEAD OFFICE **********/
    public function getallpolicy()
    {
        $this->db->select('*');
        $this->db->from('policy');
        $this->db->order_by('id','DESC');
        $query =$this->db->get();
        $res = $query->result();
        return $res;

    }

    public function getAll()
    {
        $this->db->select('*');
        $this->db->from('users');
        $this->db->join('user_hierarchy', 'user_hierarchy.user_id = users.ID', 'left');
        $query = $this->db->get();
        $res = $query->result();
        return $res;

    }

    public function gethistory($id)
    {
        $this->db->select('*');
        $this->db->from('policy_log');
        $this->db->where('policy_log.policy_id',$id);
        $query =$this->db->get();
        $res = $query->result();
        return $res;
    }
    public function regions()
    {
        $this->db->select('*');
        $this->db->from('regions');
        $query = $this->db->get();
        $res = $query->result();
        return $res;
    }

    public function search()
    {
        $form_id = $this->uri->segment(4);
        $reg = $this->input->post('region');
        $from = $this->input->post('from');

        $to = $this->input->post('to');
        $this->db->select('*');
        $this->db->from('policy_log');
        if($reg!=null){
            $this->db->where('region_id',$reg);
            $this->db->where('policy_id',$form_id);
        }

        if($from!='')
        {
            $from = date('Y-m-d h:i:s',strtotime($from));
            $this->db->where('download_date >=',$from);
            $this->db->where('policy_id',$form_id);
        }

        if($to!='')
        {
            $to = date('Y-m-d h:i:s',strtotime($to));
            $this->db->where('download_date <=',$to);
            $this->db->where('policy_id',$form_id);
        }

        $query = $this->db->get();
        $res   = $query->result();
        return  $res;
    }

    public function sendform($file_name)
    {
        $data =array(
            "policy_name"=>$this->input->post('policytitle'),
            "description"=>$this->input->post('desc'),
            "category"=>$this->input->post('category'),
            "actual_policy"=>$file_name,
            "valid_date"=>$this->input->post('valdate')
        );
        $res = $this->db->insert('policy',$data);
        if($res)
        {
            $op = $this->input->post('optional');
            if(isset($op))
            {
                $id = $this->db->insert_id();
                $this->session->set_userdata(array('id'=>$id));
                $sess_id = $this->session->userdata('id');
                foreach($this->input->post('optional') as $res):
                    $in['receiver_id']=$res;
                    $in['policy_id']=$sess_id;
                    $result = $this->db->insert('policy_receiver',$in);

                endforeach;
                $this->session->unset_userdata('id');

            }

        }
        return true;

    }

    public  function allemail($em)
    {
        $list = implode(",",$em);
        $this->db->select('users.email');
        $this->db->from('users');
        $this->db->where('users.ID IN ('.$list.')');
        $query = $this->db->get();
        $res = $query->result();
        return $res;
    }

    /***********END HEAD OFFICE PART*******************/


    /********** Common function to see download file **************/

    public function getpolicy($id)
    {
        $this->db->select('f.id as formid ,rg.name as region_name,u.name as username,f.*,u.*,r.*,rg.*');
        $this->db->from('users as u');
        $this->db->join('policy_receiver AS r','r.receiver_id=u.ID');
        $this->db->join('policy as f',' f.id=r.policy_id');
        $this->db->join('regions AS rg','rg.id=u.region');
        $this->db->where('u.ID',$id);
        $this->db->order_by('f.id','DESC');
        $query = $this->db->get();
        $res = $query->result();
        return $res;

    }


    /********** End common function to see download file **************/


    /************* Record download history to the database************************/

    public function policyalllog()
    {
        $ip = $_SERVER['REMOTE_ADDR'];
        $data = array(
            "user_id"     =>$this->input->post('id'),
            "username"    =>$this->input->post('uname'),
            "access_level"=>$this->input->post('access_level'),
            "actual_file" =>$this->input->post('act'),
            "policy_name"=>$this->input->post('act_name'),
            "region_name"      =>$this->input->post('region'),
            "region_id"        =>$this->input->post('reg_id'),
            "policy_id"     =>$this->input->post('form_id'),
            "uploaded_date"=>$this->input->post('up_date'),
            "ip"          =>$ip
        );
        $this->db->insert('policy_log',$data);
        return true;
    }

    /************* Endrecord download history************************/


    /************ To view download history for program manager************************/

    public function policydownloadhistory($id)
    {
        $sessid = $this->session->userdata('logedin_id');
        $this->db->select('*');
        $this->db->from('policy_log');
        $this->db->where('region_id',$id);
        $this->db->where_not_in('user_id',$sessid);
        $query =$this->db->get();
        $res = $query->result();
        return $res;
    }


    /************ To view download history for program officer************************/

    public function policydownloadhistoryforofficer($id)
    {
        $sessid = $this->session->userdata('logedin_id');
        $this->db->select('*');
        $this->db->from('policy_log');
        $this->db->where('region_id',$id);
        $this->db->where_not_in('access_level','Program Manager');
        $this->db->where_not_in('user_id',$sessid);
        $query =$this->db->get();
        $res = $query->result();
        return $res;
    }

    /*************** For alert message**********************************/
    public  function counteralert()
    {
        $date = date('Y-m-d');
        $cur = strtotime('-1 days');
        $yes = date('Y-m-d h:i:s',$cur);
        $id = $this->session->userdata('logedin_id');
        $this->db->select('*');
        $this->db->from('policy_receiver');
        $this->db->where('receiver_id',$id);
        $this->db->where('date >=',$yes);
        $query = $this->db->get();
        $res = $query->num_rows();
        return $res;
        /*$num = $this->db->count_all_results('forms_format');
        return $num ;*/
    }
    
    public function getPolicyCategories()
    {
        $this->db->select('*');
        $this->db->from('policy_categories');
        $query = $this->db->get();
        $res = $query->result();
        return $res;
    }
    
    public function check_category_exist($reg){
        $res = $this->db->get_where('policy_categories', array('name' => $reg));
        return $res->num_rows;
    }

    public function add_category($reg){
        return $this->db->insert('policy_categories', array('name' => $reg));
    }

    public function update_category($reg, $id){
        return $this->db->update('policy_categories', array('name' => $reg), array('id' => $id));
    }

    public function delete_category($id){
        return $this->db->delete('policy_categories', array('id' => $id));
    }
}