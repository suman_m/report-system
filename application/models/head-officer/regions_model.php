<?php

class Regions_model extends CI_Model {

    function __construct(){
        parent::__construct();
    }

    public function get_regions(){
        return $this->db->get('regions')->result();
    }

    public function check_region_exist($reg){
        $res = $this->db->get_where('regions', array('name' => $reg));
        return $res->num_rows;
    }

    public function add_region($reg){
        return $this->db->insert('regions', array('name' => $reg));
    }

    public function update_region($reg, $id){
        return $this->db->update('regions', array('name' => $reg), array('id' => $id));
    }

    public function delete_region($id){
        return $this->db->delete('regions', array('id' => $id));
    }
}
?>