<?php

class Projects_model extends CI_Model {

    function __construct(){
        parent::__construct();
    }

    public function get_projects(){
        return $this->db->get('projects')->result();
    }

    public function get_single_project($id){
        return $this->db->get_where('projects', array('id' => $id))->result();
    }

    public function get_project_holders(){
        return $this->db->get_where('users', array('active !=' => '-1', 'accessLevel' => 'Project Holder'))->result();
    }

    public function check_project_exist($pro, $id = 0){
        $res = $this->db->get_where('projects', array('name' => $pro, 'id !=' => $id));
        return $res->num_rows;
    }

    public function add_project($pro){
        $logsMessage['logs'] = "Project Added";
        $this->db->insert('projects_logs',$logsMessage);
        return $this->db->insert('projects', $pro);
    }

    public function edit_project($pro, $id){
        $logsMessage['logs'] = "Project Edited";
        $this->db->insert('projects_logs',$logsMessage);
        return $this->db->update('projects', $pro, array('id' => $id));
    }

    public function assign_user($pid, $uid){
        $logsMessage['logs'] = "Project Assigned to ".$this->misc_lib->getUserName($uid);
        $this->db->insert('projects_logs',$logsMessage);
        return $this->db->update('projects', array('assign_to_project_holder' => $uid), array('id' => $pid));
    }

    public function unassign_user($uid){
        $logsMessage['logs'] = "Project has been withdrawn from  ".$this->misc_lib->getUserName($uid);
        $this->db->insert('projects_logs',$logsMessage);
        return $this->db->update('projects', array('assign_to_project_holder' => 0), array('assign_to_project_holder' => $uid));
    }
}
?>