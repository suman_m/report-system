<?php

class Users_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    public function get_single_user($id) {
        return $res = $this->db->get_where('users', array("ID" => $id))->result();
    }

    public function get_user_parent($id) {
        return $res = $this->db->get_where('user_hierarchy', array("user_id" => $id))->result();
    }

    public function get_users_list() {
        $this->db->order_by("active", "desc");
        $result = $this->db->get_where('users', array('active !=' => -1))->result();
        return $result;
    }

    public function get_users_list_by_access($acs) {
        $this->db->order_by("active", "desc");
        $result = $this->db->get_where('users', array('active !=' => -1, 'accessLevel' => $acs))->result();
        return $result;
    }

    public function get_inactive_users() {
        return $this->db->get_where('users', array('active' => 0))->result();
    }

    public function get_active_users() {
        return $this->db->get_where('users', array('active' => 1))->result();
    }

    public function get_program_manager($region) {
        return $this->db->get_where('users', array('accessLevel' => "Program Manager", 'region' => $region))->num_rows;
    }

    public function get_users_hierarchy() {
        $result = $this->db->get('user_hierarchy')->result();
        return $result;
    }

    public function get_regions() {
        return $res = $this->db->get('regions')->result();
    }

    public function get_accesslvl() {
        return $res = $this->db->get('access_level')->result();
    }

    public function get_filtered_users($reg, $acc) {
        if ($acc == "Project Holder")
            $acc = "Program Officer";
        elseif ($acc == "Program Officer")
            $acc = "Program Manager";
        if ($reg == '')
            return $res = $this->db->get_where('users', array("accessLevel" => $acc, 'active' => 1))->result();
        elseif ($acc == '')
            return $res = $this->db->get_where('users', array("region" => $reg, 'active' => 1))->result();
        else
            return $res = $this->db->get_where('users', array("accessLevel" => $acc, "region" => $reg, 'active' => 1))->result();
    }

    public function addUser($insertdata) {
        $res = $this->db->insert('users', $insertdata);
        return $lastinsertID = $this->db->insert_id();
    }

    public function editUser($insertdata, $id) {
        $this->db->where('ID', $id);
        return $this->db->update('users', $insertdata);
    }

    public function delete_user($id) {
        return $this->db->update('users', array('active' => -1), array('ID' => $id));
    }

    public function addUserParent($parentuser, $insertID) {
        return $res = $this->db->insert('user_hierarchy', array('user_id' => $insertID, 'parent_user_id' => $parentuser));
    }

    public function deleteUserParent($insertID) {
        return $this->db->delete('user_hierarchy', array('user_id' => $insertID));
    }

    public function addUserProject($project, $insertID) {
        return $res = $this->db->update('projects', array('assign_to_project_holder' => $insertID), array('id' => $project));
    }

    public function deleteUserProject($insertID) {
        return $res = $this->db->update('projects', array('assign_to_project_holder' => $insertID), array('id' => $project));
        return $this->db->delete('user_project_relationship', array('user_id' => $insertID));
    }

    public function has_subordinates($id) {
        return $this->db->get_where('user_hierarchy', array('parent_user_id' => $id))->num_rows;
    }

    function UpdateUserHierarchyParent($parent_id, $data) {
        $this->db->update('user_hierarchy', $data, array('parent_user_id' => $parent_id));
    }

    function UpdateUserHierarchyUser($user_id, $data) {
        $this->db->update('user_hierarchy', $data, array('user_id' => $user_id));
    }

    function getSubordianates($id) {
        $user = $this->db->select('*')
                ->from('user_hierarchy as uh')
                ->join('users as u', 'uh.user_id = u.id')
                ->where('uh.parent_user_id = '.$id);
        return $user->get()->result();
    }

}

?>